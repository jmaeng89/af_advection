EXE = ./afAdv
SDIR = ./src
ODIR = ./obj

# tecplot libraries
TECHOME = /usr/local/tecplot360ex
TECINCL = $(TECHOME)/include
UNAME_S := $(shell uname -s)
ifeq ($(UNAME_S),Darwin)
	TECIOLIB = $(TECHOME)/MacOS/libtecio.dylib
	TECIOEXTRALIB = -lstdc++
else
	TECIOLIB = $(TECHOME)/bin/libtecio.so
	FOUND_INSTALLED_LIBSTDCXX_S:=$(shell test -f $(TECHOME)/bin/libstdc++.so.6 && echo found || echo missing)
	ifeq ($(FOUND_INSTALLED_LIBSTDCXX_S),found)
		TECIOEXTRALIB = $(TECHOME)/bin/libstdc++.so.6
	else
		TECIOEXTRALIB = -lstdc++
	endif
endif
LINKLIBS = -lpthread

# set default variables
ifndef DBG_LEVEL
	DBG_LEVEL = run
endif
ifndef VERBOSE
	VERBOSE = clean
endif
ifndef MACHINE
	MACHINE = glg20
endif

FC = gfortran
BASEFLAGS = -fdefault-real-8 -cpp -falign-commons  \
			-ffpe-trap=invalid,zero,overflow,underflow -fcray-pointer

# add optimization/debug flags
ifeq ($(DBG_LEVEL),debug)
	DBGFLAGS = -O0 $(BASEFLAGS) -g3 -pedantic -Wall -Wextra\
		   -fbacktrace -fbounds-check -fcheck=all
else
	DBGFLAGS = -O3 $(BASEFLAGS) 
endif

# add precompiler directives
ifeq ($(VERBOSE),verb)
	VFLAGS = $(DBGFLAGS) -DVERBOSE
else
	VFLAGS = $(DBGFLAGS)
endif

FFLAGS = $(VFLAGS)

# Advection Modules ( order matters )
FMODS = $(ODIR)/solverVars.o $(ODIR)/physics.o \
		$(ODIR)/mathUtil.o $(ODIR)/refElemUtil.o \
		$(ODIR)/meshUtil.o $(ODIR)/analyticFunctions.o \
		$(ODIR)/iterativeFunctions.o $(ODIR)/reconstruction.o \
		$(ODIR)/boundaries.o $(ODIR)/update.o $(ODIR)/timeControl.o \
		$(ODIR)/inputOutput.o \
		$(ODIR)/tecplotVisualUtil.o \
		$(ODIR)/output_tecplot.o \
		$(ODIR)/startStop.o $(ODIR)/postProc.o


# Object files for main program
FOBJS = $(FMODS) \
	$(ODIR)/afAdv.o

# Object files for error calculation
EOBJS = $(FMODS) \
	$(ODIR)/postProc.o

# Object files for test program
TOBJS = $(FMODS) \
	$(ODIR)/afTest.o

# Make types
all : $(EXE) 

afErr: $(EOBJS) $(ODIR)/afError.o
	$(FC) $(FFLAGS) -o afError $^ $(TECIOLIB) $(TECIOEXTRALIB) $(LINKLIBS)
#	$(FC) $(FFLAGS) -o afError $^ $(LINKLIBS)

afTest: $(TOBJS) $(ODIR)/afTest.o 
	$(FC) $(FFLAGS) -o afTest $^ $(TECIOLIB) $(TECIOEXTRALIB) $(LINKLIBS)
#	$(FC) $(FFLAGS) -o afTest $^ $(LINKLIBS)

$(EXE) : $(FOBJS)
	$(FC) $(FFLAGS) -o $@ $^ -I$(TECINCL) $(TECIOLIB) $(TECIOEXTRALIB) $(LINKLIBS) 
#	$(FC) $(FFLAGS) -o $@ $^ -I$(LINKLIBS) 

$(ODIR)/%.o : $(SDIR)/%.f90
	$(FC) $(FFLAGS) -c -o $@ -I$(TECINCL) -J$(ODIR) $^
#	$(FC) $(FFLAGS) -c -o $@ -J$(ODIR) $^

# phony targets 
.PHONY : realclean clean debug verbose nyx distro

realclean :
	@-rm -vf afError
	@-rm -vf afTest
	@make clean 
clean :
	@-rm -vf $(EXE) $(ODIR)/*.o $(ODIR)/*.mod $(ODIR)/*genmod.f90 
debug :
	@make all DBG_LEVEL=debug
verbose :
	@make all DBG_LEVEL=debug VERBOSE=verb
