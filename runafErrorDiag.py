#-------------------------------------------------------------------------------
# @ purpose

#  Run fortran generated error executable
#
# @ history
#  8 October 2013 - Initial creation (Maeng)
#
# required libraries
import os, sys, numpy, subprocess

# local variables
caseDir = 'cases/linadv_tri/gauss_diag' # directory containing cases,drivers
numFiles = 5 # number of grid refinements

# for loop to run executables
for i in range(0, numFiles):
	print('%d',i)
	j =  ((2*4**(i)))
        caseNum = '%03d' % j
	caseName = caseDir+'/diag'+caseNum+'.drv'	# case name
	#j =  ((1*4**(i)))
	# run main executable
	#subprocess.call(['./afAdv',caseName])
	# run error executable
	subprocess.call(['./afError',caseName])
