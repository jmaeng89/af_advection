function[] = plotErrConvergenceSystem(baseDir,caseDir)
% PLOTERRCONVERGENCESYSTEM(BASEDIR,CASEDIR) returns the error convergence
%   result from a series of mesh refinements. 
%
%   J. Brad Maeng
%   10/1/2015 - created
%   3/31/2016 - revised, minor changes in variable names
%
    close all;

    % 2D linear advection on quadrilateral
    %baseDir = '../cases/linadv_quad/circRot';
    %baseDir = '../cases/linadv_quad/circRot2';
    %baseDir = '../cases/linadv_quad/sine';
    %baseDir = '../cases/linadv_quad/gauss';

    % 2D Burgers' equations on quadrilateral
    %baseDir = '../cases/burgers_quad/sine';
    %baseDir = '../cases/burgers_quad/gauss';

    % 2D pressureless Euler equations on quadrilateral
    %baseDir = '../cases/pless_quad/sine';
    %baseDir = '../cases/pless_quad/gauss';
    %baseDir = '../cases/pless_quad/pless_t1';
    %baseDir = '../cases/pless_quad/pless_t2';
    
    %% 2D linear advection on triangle
    baseDir = '../cases/linadv_tri/gauss_diag';
    %baseDir = '../cases/linadv_tri/gauss_unst';
    %baseDir = '../cases/linadv_tri/sine_unst';
    %baseDir = '../cases/linadv_tri/sine_diag';
    %baseDir = '../cases/linadv_tri/sine_diag_inv';
    %baseDir = '../cases/linadv_tri/gauss_diag_inv';

    %% 2D burgers equation on triangle
    %baseDir = '../cases/burgers_tri/gauss_diag';
    %baseDir = '../cases/burgers_tri/gauss_diag_inv';
    %baseDir = '../cases/burgers_tri/gauss_unst';

    %baseDir = '../cases/burgers_tri/sine_diag';
    %baseDir = '../cases/burgers_tri/sine_diag_inv';
    %baseDir = '../cases/burgers_tri/sine_unst';

    %% 2D pressureless Euler equations on triangle
    %baseDir = '../cases/pless_tri/gauss_diag_inv';
    %baseDir = '../cases/pless_tri/gauss_unst';

    %baseDir = '../cases/pless_tri/sine_unst';
    %baseDir = '../cases/pless_tri/sine_diag_inv';

    %baseDir = '../cases/pless_tri/sine_cos_unst';
    %baseDir = '../cases/pless_tri/sine_cos_diag_inv';

    %baseDir = '../cases/pless_tri/sine_uncoupled_unst';
    %baseDir = '../cases/pless_tri/sine_uncoupled_diag_inv';

    %baseDir = '../cases/pless_tri/pless_t1_diag_inv';
    %baseDir = '../cases/pless_tri/pless_t1_unst';

    %baseDir = '../cases/pless_tri/pless_t2_diag_inv';
    %baseDir = '../cases/pless_tri/pless_t2_unst';

    %baseDir = '../cases/pless_tri/pless_t1_test';

    %baseDir = '../cases/linadv_tri_limtest/sin_diag_inv';
    %baseDir = '../cases/linadv_tri_limtest/sin_diag';
    %baseDir = '../cases/linadv_tri_limtest/sin_unst';

    %baseDir = '../cases/linadv_tri/gauss_diag_abdiff';
    %baseDir = '../cases/burgers_tri/sine_unst';
    %baseDir = '../cases/pless_tri/svortex_unst';
    caseDir = {''};

    %% 1D problems
    %baseDir = '../cases/linadv_oned/gauss';
    %baseDir = '../cases/linadv_oned/sine';
    %baseDir = '../cases/burgers_oned/sine';
    %baseDir = '../cases/burgers_oned/gauss';
    %baseDir = '../cases/pless_oned/gauss';
    %baseDir = '../cases/pless_oned/sine';

    %baseDir = '../cases/burgers_tri';
    %baseDir = '../cases/pless_tri';
    %caseDir = {'sine_diag','sine_diag_inv','sine_unst'};
    %baseDir = '../cases';
    %caseDir = {'burgers_tri/sine_diag','burgers_tri/sine_diag_inv','burgers_tri/sine_unst','burgers_quad/sine'};
    %caseDir = {'linadv_tri/gauss_diag','linadv_tri/gauss_diag_inv','linadv_tri/gauss_unst','linadv_quad/gauss'};
    %caseDir = {'linadv_tri/gauss_diag_abdiff','linadv_tri/gauss_diag_inv_abdiff','linadv_tri/gauss_unst_abdiff','linadv_quad/gauss_abdiff'};
    %caseDir = {'gauss_diag','gauss_diag_inv','gauss_unst'};
    %caseDir = {'gauss_diag_abdiff'}; %,'gauss_diag_inv_abdiff','gauss_unst_abdiff'};
    %caseDir = {'pless_t1_diag_inv','pless_t1_unst'};

    % compare AF to DG
    baseDir = '../..';
    %caseDir = {'advection2d/cases/linadv_tri/gauss_diag', 'dg_advection2d/cases/gauss_diag_p2'};
    %caseDir = {'advection2d/cases/burgers_tri/sine_diag_inv','dg_advection2d/cases/burgers/sine_diagInv'};
    %caseDir = {'advection2d/cases/burgers_tri/sine_unst','dg_advection2d/cases/burgers/sine_unst'};

    % figure output flag
    figOutputOn = false;
    %figOutputOn = true;

    % table output flag
    tabOutputOn = false;
    %tabOutputOn = true;

    % conserved variable 
    consVar = false; % primitive 
    %consVar = true; % conserved variable

    % triangle rate indicator
    triRateOn = true;
    %triRateOn = false;
 
    % variables names
    if consVar
        pvNames = {'\rho', '\rho u', '\rho v'};
        %pvNames = {'\rho', 'u', 'v'};
    else
        pvNames = {'\rho', 'u', 'v'};
    end 
 
    % legend names
    legendNames = pvNames;
    legendNames = {'AF','DG1', 'DG2'};
    %legendNames = {'Str B', 'Unst'};
    
    [~,fileNum] = size(caseDir);
    
    % Allocate variables for error and spacing
    for i = 1:fileNum
        if isempty(caseDir{i})
            if consVar
                FN = [baseDir,'/','conserverrData','.dat'];     % complete file location     
            else
                FN = [baseDir,'/','errData','.dat'];    
            end
        else
            if consVar
                FN = [baseDir,'/',caseDir{i},'/','conserverrData','.dat'];     % complete file location
            else
                FN = [baseDir,'/',caseDir{i},'/','errData','.dat'];     
            end
        end 

        %[fh, fL1, fL2] = readErrData(FN);
        [fh, fDOF, fL1, fL2] = readErrData(FN);
        h{i} = fh;            % 1/dof^(-1/2)
        DOF{i} = fDOF;          % dof
        L1{i} = fL1;            % L1 norm of error
        L2{i} = fL2;            % L2 norm of error

    end

    [nEqns,totalLevs] = size(L1{1});
    nEqns = 1;
    % refinement levels
    refLevel = 5;
    % hard code
    %legendNames = legendNames(2:3);
     
    if tabOutputOn
        % open a file to store the convergence study data
        for i = 1:fileNum
            if consVar
                [fid] = fopen([baseDir,'/','conserverrConvergence',caseDir{i},'.dat'],'w+');
            else
                [fid] = fopen([baseDir,'/','errConvergence',caseDir{i},'.dat'],'w+');
            end
            for iEq = 1:nEqns
                for lev = 1:totalLevs
                    if ( lev == 1 )
                        fprintf(fid, '\n %s \n', caseDir{i});
                        fprintf(fid, 'Equation %5d, Variable %s\n', iEq, pvNames{iEq});
                        fprintf(fid, '%5s %s %9s %s %9s %s %9s %s %4s %s %9s %s %4s\n', ...
                                     'Level','&','DOF','&','h','&','L1Error','&','Order','&','L2Error','&','Order');
                        fprintf(fid, '%5d %s %4.2e %s %4.2e %s %4.2e %s %4s %s %4.2e %s %4s %s\n', ...
                                      lev,'&',DOF{i}(lev), '&', h{i}(lev),'&',L1{i}(iEq,lev),'&','','&',L2{i}(iEq,lev),'&','','\\');
                    elseif ( mod(lev,refLevel) == 1 && lev ~= 1)
                        fprintf(fid, '%5d %s %4.2e %s %4.2e %s %4.2e %s %4s %s %4.2e %s %4s %s\n',...
                                      lev,'&', DOF{i}(lev), '&',h{i}(lev),'&',L1{i}(iEq,lev),'&', ...
                                      '       ','&', ...
                                      L2{i}(iEq,lev), '&', ...
                                      '       ', '\\')
                    else
                        % latex tabular output
                        fprintf(fid, '%5d %s %4.2e %s %4.2e %s %4.2e %s %.2f %s %4.2e %s %.2f %s\n',...
                                      lev,'&',DOF{i}(lev),'&',h{i}(lev),'&',L1{i}(iEq,lev),'&', ...
                                      log(L1{i}(iEq,lev-1)/L1{i}(iEq,lev))/log(h{i}(lev-1)/h{i}(lev)),'&', ...
                                      L2{i}(iEq,lev),'&', ...
                                      log(L2{i}(iEq,lev-1)/L2{i}(iEq,lev))/log(h{i}(lev-1)/h{i}(lev)), '\\');
                    end
                end
            end
            fclose(fid);    
        end
    end
    
    for i = 1:fileNum
        for iEq = 1:nEqns
            for lev = 1:totalLevs             
               if ( lev == 1 )
                    fprintf('\n %s \n',caseDir{i});
                    fprintf('Equation %5d, Variable %s\n', iEq, pvNames{iEq});
                    fprintf('%5s  %12s  %12s   %12s  %7s  %12s  %7s\n', ...
                            'Level','DOF','h','L1Error','Order','L2Error','Order')
                    fprintf('%5s  %12s  %12s   %12s  %7s  %12s  %7s\n', ...
                            '-----', '-----','-----','------------','-------','------------','-------')
                    fprintf('%5d  %e  %e   %e   %5s   %e    %7s \n', ...
                            lev,DOF{i}(lev),h{i}(lev),L1{i}(iEq,lev),'',L2{i}(iEq,lev),'');
                elseif ( mod(lev,refLevel) == 1 && lev ~= 1)
                    fprintf('%5d  %e  %e   %e  %7s  %e  %7s\n',...
                            lev,DOF{i}(lev),h{i}(lev),L1{i}(iEq,lev), ...
                            '       ', ...
                            L2{i}(iEq,lev), ...
                            '       ')
                else
                    fprintf('%5d  %e  %e   %e  %7.4f  %e  %7.4f\n',...
                            lev,DOF{i}(lev),h{i}(lev),L1{i}(iEq,lev), ...
                            log(L1{i}(iEq,lev-1)/L1{i}(iEq,lev))/log(h{i}(lev-1)/h{i}(lev)), ...
                            L2{i}(iEq,lev), ... 
                            log(L2{i}(iEq,lev-1)/L2{i}(iEq,lev))/log(h{i}(lev-1)/h{i}(lev)))
                end
            end
        end
    end
    

    hRef = h{1};
    % plot solution convergence 
    style = ['bo-';'rs-';'k*-';'gd-';'m<-'];
    %style2 = ['bo-.';'rs-';'k<-';'gd-';'m<-'];
    style2 = ['bo-';'rs-';'k<-';'gd-';'m<-'];

    lWidth = 2;
    fSize = 18;

    % reference equation index for 3rd order line
    iEqRef = 2;
    offset2 = 10;
    offset = 0.2;

    %% L1 norm error
    %for i = 1:fileNum
    %    figure(1)
    %    %figure(i)
    %    for iEq = 1:nEqns
    %        loglog(h{i}(:),L1{i}(iEq,:),style(iEq,:),'linewidth',lWidth,'markersize',fSize)
    %        %loglog(h{i}(:),L1{i}(iEq,:),style2(i,:),'linewidth',lWidth,'markersize',fSize)
    %        hold on
    %        if ( iEq == nEqns && i == fileNum  )

    %            %rate = 2;
    %            %iFile = 1;
    %            %loglog(hRef(:),offset2*(hRef(:)).^rate/(hRef(1)^rate)*L1{iFile}(iEqRef,1),'k--','linewidth',lWidth)    % 3rd order
    %            %text(0.8*hRef(end),offset2*(hRef(end)).^rate/(hRef(1)^rate)*L1{iFile}(iEqRef,1),sprintf('%d',rate),'FontSize',fSize)

    %            rate = 3;
    %            iFile = fileNum;
    %            loglog(hRef(:),offset*(hRef(:)).^rate/(hRef(1)^rate)*L1{iFile}(iEqRef,1),'k--','linewidth',lWidth)    % 3rd order
    %            text(0.8*hRef(end),offset*(hRef(end)).^rate/(hRef(1)^rate)*L1{iFile}(iEqRef,1),sprintf('%d',rate),'FontSize',fSize)

    %            legend([legendNames],'location','best')

    %            hx = xlabel('DOF^-^1^/^2'); hy = ylabel('|\epsilon|_1');
    %            set(gca,'FontSize',fSize)
    %            set(hx,'FontSize',fSize)
    %            set(hy,'FontSize',fSize)
    %            
    %        end
    %    end
    %    
    %end

    %if triRateOn
    %    % triangle rate!
    %    offset2 = 10;
    %    iFile = 1;
    %    rate = 2;
    %    % triangle rate!
    %    tri_x = hRef(end:-1:end-1);
    %    tri_y = (hRef(end:-1:end-1)).^rate/(hRef(end-1)^rate)*L1{iFile}(iEqRef,end);
    %    %tri_y = interp1(hRef, L1{iFile}(iEqRef,:), tri_x);
    %    % down side up
    %    loglog(tri_x([1,1,2,1]), offset2*tri_y([1,2,2,1]),'k-'); %,'linewidth',lWidth)    % triangle  
    %    % right side up
    %    %loglog(tri_x([1,2,2,1]), offset2*tri_y([1,1,2,1]),'k--','linewidth',lWidth)    % triangle  
    %    %rate = log(L1{iFile}(iEqRef,end-1)/L1{iFile}(iEqRef,end))/log(h{iFile}(end-1)/h{iFile}(end));
    %    text(mean(tri_x([1,1,2,1])),mean(offset2*tri_y([1,2,2,1])),sprintf('%d', rate),'FontSize',fSize) % located above
    %    %text(mean(tri_x([1])),0.5*offset2*mean(tri_y([1])),sprintf('%3.2f', rate),'FontSize',fSize) % located below
    %    
    %    offset2 = 0.9;
    %    iFile = 2;
    %    rate = 3;
    %    % triangle rate!
    %    tri_x = hRef(end:-1:end-1);
    %    tri_y = (hRef(end:-1:end-1)).^rate/(hRef(end-1)^rate)*L1{iFile}(iEqRef,end);
    %    %tri_y = interp1(hRef, L1{iFile}(iEqRef,:), tri_x);
    %    %% down side up
    %    %loglog(tri_x([1,1,2,1]), offset2*tri_y([1,2,2,1]),'k--','linewidth',lWidth)    % triangle  
    %    % right side up
    %    loglog(tri_x([1,2,2,1]), offset2*tri_y([1,1,2,1]),'k-'); %,'linewidth',lWidth)    % triangle  
    %    %rate = log(L1{iFile}(iEqRef,end-1)/L1{iFile}(iEqRef,end))/log(h{iFile}(end-1)/h{iFile}(end));
    %    text(mean(tri_x([1,2,2,1])),mean(offset2*tri_y([1,1,2,1])),sprintf('%d', rate),'FontSize',fSize) % located above
    %    %text(mean(tri_x([1])),0.5*offset2*mean(tri_y([1])),sprintf('%3.2f', rate),'FontSize',fSize) % located below
    %end

    %if figOutputOn
    %    if consVar
    %        fname1 = [baseDir,'/','conserv_errorL1.eps'];
    %    else
    %        fname1 = [baseDir,'/','errorL1.eps'];
    %    end
    %    print('-depsc2', '-r300', fname1);
    %end


    %hold off;

    % L2 norm error
    for i = 1:fileNum
        figure(2)
        %figure(i+fileNum)        
        for iEq = 1:nEqns
            %figure(iEq)
            %loglog(h{i}(:),L2{i}(iEq,:),style(iEq,:),'linewidth',lWidth,'markersize',fSize)
            loglog(h{i}(:),L2{i}(iEq,:),style2(i,:),'linewidth',lWidth,'markersize',fSize)
            hold on
            if ( iEq == nEqns && i == fileNum )

                %rate = 2;
                %iFile = 1;
                %loglog(hRef(:),offset2*(hRef(:)).^rate/(hRef(1)^rate)*L2{iFile}(iEqRef,1),'k--','linewidth',lWidth)    % 3rd order
                %text(0.8*hRef(end),offset2*(hRef(end)).^rate/(hRef(1)^rate)*L2{iFile}(iEqRef,1),sprintf('%d',rate),'FontSize',fSize)

                rate = 3;
                iFile = fileNum;
                loglog(hRef(:),offset*(hRef(:)).^rate/(hRef(1)^rate)*L2{iFile}(iEqRef,1),'k--','linewidth',lWidth)    % 3rd order
                text(0.8*hRef(end),offset*(hRef(end)).^rate/(hRef(1)^rate)*L2{iFile}(iEqRef,1),sprintf('%d',rate),'FontSize',fSize)
                
                legend([legendNames],'location','best')

                hx = xlabel('DOF^-^1^/^2'); hy = ylabel('|\epsilon|_2');
                set(gca,'FontSize',fSize)
                set(hx,'FontSize',fSize)
                set(hy,'FontSize',fSize)
            end
        end
    end
    % triangle rate reference

    if triRateOn
        offset2 = 10;
        iFile = 1;
        rate = 2;
        % triangle rate!
        tri_x = hRef(end:-1:end-1);
        tri_y = (hRef(end:-1:end-1)).^rate/(hRef(end-1)^rate)*L2{iFile}(iEqRef,end);
        %tri_y = interp1(hRef, L2{iFile}(iEqRef,:), tri_x);
        % down side up
        %loglog(tri_x([1,1,2,1]), offset2*tri_y([1,2,2,1]),'k-'); %,'linewidth',lWidth)    % triangle  
        % right side up
        %loglog(tri_x([1,2,2,1]), offset2*tri_y([1,1,2,1]),'k--','linewidth',lWidth)    % triangle  
        %rate = log(L2{iFile}(iEqRef,end-1)/L2{iFile}(iEqRef,end))/log(h{iFile}(end-1)/h{iFile}(end));
        %text(mean(tri_x([1])),1.5*offset2*mean(tri_y([2])),sprintf('%d', rate),'FontSize',fSize) % located above
        %text(mean(tri_x([1,1,2,1])),mean(offset2*tri_y([1,2,2,1])),sprintf('%d', rate),'FontSize',fSize) % located above
        %text(mean(tri_x([1])),0.5*offset2*mean(tri_y([1])),sprintf('%3.2f', rate),'FontSize',fSize) % located below
        
        offset2 = 0.9;
        iFile = 2;
        rate = 3;
        % triangle rate!
        tri_x = hRef(end:-1:end-1);
        tri_y = (hRef(end:-1:end-1)).^rate/(hRef(end-1)^rate)*L2{iFile}(iEqRef,end);
        %tri_y = interp1(hRef, L2{iFile}(iEqRef,:), tri_x);
        %% down side up
        %loglog(tri_x([1,1,2,1]), offset2*tri_y([1,2,2,1]),'k--','linewidth',lWidth)    % triangle  
        % right side up
        %loglog(tri_x([1,2,2,1]), offset2*tri_y([1,1,2,1]),'k-') ; %,'linewidth',lWidth)    % triangle  
        rate = log(L2{iFile}(iEqRef,end-1)/L2{iFile}(iEqRef,end))/log(h{iFile}(end-1)/h{iFile}(end));
        %text(mean(tri_x([1,2,2,1])),mean(offset2*tri_y([1,1,2,1])),sprintf('%3.2f', rate),'FontSize',fSize) % located above
        %text(mean(tri_x([1])),0.5*offset2*mean(tri_y([1])),sprintf('%d', rate),'FontSize',fSize) % located below
        text(mean(tri_x([1])),1.5*offset2*mean(tri_y([2])),sprintf('%3.2f', rate),'FontSize',fSize) % located above
    end

    if figOutputOn
        if consVar
            fname2 = [baseDir,'/','conserv_errorL2.eps'];
        else
            fname2 = [baseDir,'/','errorL2.eps'];
        end
        print('-depsc2', '-r300', fname2);
    end

    hold off;


end

%function [h, L1, L2] = readErrData(file)
function [h, DOF, L1, L2] = readErrData(file)
% READERRDATA(FILE) returns the L1 and L2 norm errors for the solution 
%   contained in FILE

    A = load(file);

    [totalLevs, cols] = size(A);
    
    % determine the number of equations
    nEqns = (cols-3)/2;
    %nEqns = 4;
    
    % initialize output variables
    nCells = zeros(totalLevs,1);
    DOF = zeros(totalLevs,1);
    h = zeros(totalLevs,1); 
    L1 = zeros(nEqns,totalLevs);
    L2 = zeros(nEqns,totalLevs);
        
    for lev = 1:totalLevs
        h(lev) = A(lev,3);   % cell size, 1/sqrt(dof)
        nCells(lev) = A(lev,1);   % nCells
        DOF(lev) = A(lev,2);   % nCells + nEdges + nNodes
        %h(lev) = 1.0/sqrt(A(lev,2)-A(lev,1));   % cell size, 1/sqrt(dof)
        %h(lev) = 1.0/sqrt(3.0*A(lev,1));   % cell size, 1/sqrt(dof)
        for iEq = 1:nEqns
            L1(iEq,lev) = A(lev,3+iEq);
            L2(iEq,lev) = A(lev,3+iEq+nEqns);
        end
    end
            
end

