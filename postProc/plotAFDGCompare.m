function[] = plotAFDGCompare(baseDir,caseDir)
% PLOTERRCONVERGENCESYSTEM(BASEDIR,CASEDIR) returns the error convergence
%   result from a series of mesh refinements. 
%
%   J. Brad Maeng
%   10/1/2015 - created
%   3/31/2016 - revised, minor changes in variable names
%
    close all;

    % compare AF to DG
    baseDir = '../..';
    
    caseDir = {'../advection2d/cases/linadv_tri/gauss_diag', '../dg_advection2d/cases/linadv/gauss_diag_p1', '../dg_advection2d/cases/linadv/gauss_diag_p2'};
    caseDir = {'../advection2d/cases/linadv_tri/gauss_diag_inv', '../dg_advection2d/cases/linadv/gauss_diag_inv_p1', '../dg_advection2d/cases/linadv/gauss_diag_inv_p2'};
    caseDir = {'../advection2d/cases/linadv_tri/gauss_unst', '../dg_advection2d/cases/linadv/gauss_unst_p1', '../dg_advection2d/cases/linadv/gauss_unst_p2'};

    caseDir = {'../advection2d/cases/burgers_tri/sine_diag_inv', '../dg_advection2d/cases/burgers/sine_diag_inv_p1', '../dg_advection2d/cases/burgers/sine_diag_inv_p2'};
    caseDir = {'../advection2d/cases/burgers_tri/sine_diag', '../dg_advection2d/cases/burgers/sine_diag_p1', '../dg_advection2d/cases/burgers/sine_diag_p2'};
    caseDir = {'../advection2d/cases/burgers_tri/sine_unst', '../dg_advection2d/cases/burgers/sine_unst_p1', '../dg_advection2d/cases/burgers/sine_unst_p2'};

    caseDir = {'../advection2d/cases/pless_tri/pless_t2_unst', '../dg_advection2d/cases/pless/pless_t2_unst_p1', '../dg_advection2d/cases/pless/pless_t2_unst_p2'};
    caseDir = {'../advection2d/cases/pless_tri/pless_t2_diag_inv', '../dg_advection2d/cases/pless/pless_t2_diag_inv_p1', '../dg_advection2d/cases/pless/pless_t2_diag_inv_p2'};
    caseDir = {'../advection2d/cases/pless_tri/pless_t2_diag', '../dg_advection2d/cases/pless/pless_t2_diag_p1', '../dg_advection2d/cases/pless/pless_t2_diag_p2'}; 

    caseDir = {'../advection2d/cases/pless_tri/pless_t1_unst', '../dg_advection2d/cases/pless/pless_t1_unst_p1', '../dg_advection2d/cases/pless/pless_t1_unst_p2'};
    caseDir = {'../advection2d/cases/pless_tri/pless_t1_diag', '../dg_advection2d/cases/pless/pless_t1_diag_p1', '../dg_advection2d/cases/pless/pless_t1_diag_p2'}; 
    caseDir = {'../advection2d/cases/pless_tri/pless_t1_diag_inv', '../dg_advection2d/cases/pless/pless_t1_diag_inv_p1', '../dg_advection2d/cases/pless/pless_t1_diag_inv_p2'}; 

    caseDir = {'../afCode/advection2d/cases/pless/svortex', '../dgCode/dg_advection_euler_solver_2d/cases/pless/pvortex_p1', '../dgCode/dg_advection_euler_solver_2d/cases/pless/pvortex_p2'};
    caseDir = {'../afCode/advection2d/cases/burgers_tri/sine_unst', '../dgCode/dg_advection_euler_solver_2d/cases/burgers/sine_unst_p1', '../dgCode/dg_advection_euler_solver_2d/cases/burgers/sine_unst_p2'};

    % figure output flag
    figOutputOn = false;
    figOutputOn = true;

    % table output flag
    tabOutputOn = false;
    %tabOutputOn = true;

    % conserved variable 
    consVar = false; % primitive 
    %consVar = true; % conserved variable

    % triangle rate indicator
    %triRateOn = true;
    triRateOn = false;
    
    [~,fileNum] = size(caseDir);
    
    % Allocate variables for error and spacing
    for i = 1:fileNum
        if isempty(caseDir{i})
            if consVar
                FN = [baseDir,'/','conserverrData','.dat'];     % complete file location     
            else
                FN = [baseDir,'/','errData','.dat'];    
            end
            %FN2 = [baseDir,'/','time','.dat'];     % complete file location     
        else
            if consVar
                FN = [baseDir,'/',caseDir{i},'/','conserverrData','.dat'];     % complete file location
            else
                % TODO: different L2 norm error for AF and DG 
                %if ( i == 1 ) 
                %    FN = [baseDir,'/',caseDir{i},'/','conserverrData','.dat'];     % complete file location
                %else
                FN = [baseDir,'/',caseDir{i},'/','errData','.dat'];     
                %end
            end
            %FN2 = [baseDir,'/',caseDir{i},'/','time','.dat'];     % complete file location
        end 

        [fh, fDOF, fL1, fL2] = readErrData(FN);
        h{i} = fh;            % 1/dof^(-1/2)
        DOF{i} = fDOF;          % dof
        L1{i} = fL1;            % L1 norm of error
        L2{i} = fL2;            % L2 norm of error
                
        %%%%%% important
        WU{i} = DOF{i};
        if ( i == 1 )
            % AF work unit
            WU{i}(:) = DOF{i}(:).*(2/1);
        elseif ( i == 2 )
            %% DG work unit
            WU{i}(:) = DOF{i}(:).*(2/(1/3));
        elseif ( i == 3 )
            %% DG work unit
            WU{i}(:) = DOF{i}(:).*(3/(1/5));
        end
        %%%%%% important

        %[fnIter,ftotalCompTime] = readTimeData(FN2);
        %nIter{i} = fnIter;
        %totalTime{i} = ftotalCompTime;

    end

    % variables names
    if consVar
        pvNames = {'\rho', '\rho u', '\rho v'};
    else
        pvNames = {'\rho', 'u', 'v'};
    end 
 
    % legend names
    legendNames = pvNames;
    legendNames = {'AF','DG1','DG2'};

    [nEqns,totalLevs] = size(L1{1});
    iEqBegin = 2;
    iEqEnd = 2;
    nEqns = iEqEnd;
    % refinement levels
    refLevel = 5;
     
    if tabOutputOn
        % open a file to store the convergence study data
        %if consVar
            [fid] = fopen([baseDir,'/','conserverrConvergence_AF_DG_comp.dat'],'w+');
        %else
        %    [fid] = fopen([baseDir,'/','errConvergence_AF_DG_comp.dat'],'w+');
        %end

        for i = 1:fileNum
            %for iEq = 1:size(L2{i},1)
            for iEq = iEqBegin:iEqEnd
                %for lev = 1:totalLevs
                for lev = 1:size(L2{i},2)
                    if ( lev == 1 )
                        fprintf(fid, '\n %s \n', caseDir{i});
                        fprintf(fid, 'Equation %5d, Variable %s\n', iEq, pvNames{iEq});
                        fprintf(fid, '%5s  %7s  %12s  %8s  %4s\n', ...
                                     'Level', 'DOF', 'Work Unit','L2Error','Order');
                        fprintf(fid, '%5s  %7s  %12s  %8s  %4s\n', ...
                                     '-----','-----','----------','--------','-----');
                        fprintf(fid, '%5d %s %4.2e %s %4.2e %s %4.2e %s %4s %s\n', ...
                            lev,'&', DOF{i}(lev),'&',WU{i}(lev),'&',L2{i}(iEq,lev),'&','','\\');
                    elseif ( mod(lev,refLevel) == 1 && lev ~= 1)
                        fprintf(fid, '%5d %s %4.2e %s %4.2e %s %4.2e %s %4s %s\n',...
                            lev,'&', DOF{i}(lev),'&',WU{i}(lev),'&', ...
                            L2{i}(iEq,lev), '&', ...
                            '       ', '\\')
                    else
                        % latex tabular output
                        fprintf(fid, '%5d %s %4.2e %s %4.2e %s %4.2e %s %.2f %s\n',...
                                lev,'&',DOF{i}(lev),'&',WU{i}(lev),'&',L2{i}(iEq,lev),'&', ...
                                log(L2{i}(iEq,lev-1)/L2{i}(iEq,lev))/log(h{i}(lev-1)/h{i}(lev)), '\\');
                    end
                end
            end
        end
        fclose(fid);    

        %% open a file to store the convergence study data - wall time
        %[fid] = fopen([baseDir,'/','conserverrConvergence_AF_DG_comp_walltime.dat'],'w+');
        %for i = 1:fileNum
        %    for iEq = iEqBegin:iEqEnd
        %        for lev = 1:totalLevs
        %            if ( lev == 1 )
        %                fprintf(fid, '\n %s \n', caseDir{i});
        %                fprintf(fid, 'Equation %5d, Variable %s\n', iEq, pvNames{iEq});
        %                fprintf(fid, '%5s  %7s  %7s  %12s  %8s  %4s\n', ...
        %                             'Level','DOF','nIter','Wall time','L2Error','Order');
        %                fprintf(fid, '%5s  %7s  %7s  %12s  %8s  %4s\n', ...
        %                             '-----','-------','-------','------------','--------','----');
        %                fprintf(fid, '%5d %s %4.2e %s %7d %s %4.2e %s %4.2e %s %4s %s\n', ...
        %                    lev,'&', DOF{i}(lev),'&',nIter{i}(lev),'&',totalTime{i}(lev),'&',L2{i}(iEq,lev),'&','','\\');
        %            elseif ( mod(lev,refLevel) == 1 && lev ~= 1)
        %                fprintf(fid, '%5d %s %4.2e %s %7d %s %4.2e %s %4.2e %s %4s %s\n',...
        %                    lev,'&', DOF{i}(lev),'&',nIter{i}(lev),'&',totalTime{i}(lev),'&', ...
        %                    L2{i}(iEq,lev), '&', ...
        %                    '       ', '\\');
        %            else
        %                % latex tabular output
        %                fprintf(fid, '%5d %s %4.2e %s %7d %s %4.2e %s %4.2e %s %.2f %s\n',...
        %                        lev,'&',DOF{i}(lev),'&',nIter{i}(lev),'&',totalTime{i}(lev),'&', ...
        %                        L2{i}(iEq,lev),'&', ...
        %                        log(L2{i}(iEq,lev-1)/L2{i}(iEq,lev))/log(h{i}(lev-1)/h{i}(lev)), '\\');
        %            end
        %        end
        %    end
        %end
        %fclose(fid);    

    end
    
    for i = 1:fileNum
        %for iEq = 1:size(L2{i},1)
        for iEq = iEqBegin:iEqEnd
            %for lev = 1:totalLevs             
            for lev = 1:size(L2{i},2)             
               if ( lev == 1 )
                    fprintf('\n %s \n',caseDir{i});
                    fprintf('Equation %5d, Variable %s\n', iEq, pvNames{iEq});
                    fprintf('%5s  %12s  %12s   %12s   %12s  %7s\n','Level','DOF','h','Work Unit','L2Error','Order')
                    fprintf('%5s  %12s  %12s   %12s   %12s  %7s\n','-----', '-----','-----','------------','------------','-------')
                    fprintf('%5d  %e  %e   %e   %e    %7s \n',lev,DOF{i}(lev),h{i}(lev),WU{i}(lev),L2{i}(iEq,lev),'');
                elseif ( mod(lev,refLevel) == 1 && lev ~= 1)
                    fprintf('%5d  %e  %e   %e  %e  %7s\n',...
                        lev,DOF{i}(lev),h{i}(lev),WU{i}(lev), ...
                        L2{i}(iEq,lev), ...
                        '       ')
                else
                    fprintf('%5d  %e  %e   %e   %e  %7.4f\n',...
                            lev,DOF{i}(lev),h{i}(lev),WU{i}(lev), ...
                            L2{i}(iEq,lev), ... 
                            log(L2{i}(iEq,lev-1)/L2{i}(iEq,lev))/log(h{i}(lev-1)/h{i}(lev)))
                end
            end
        end
    end
    

    hRef = h{1};
    % plot solution convergence 
    style = ['bo-';'rs-';'k*-';'gd-';'m<-'];
    %style2 = ['bo-.';'rs-';'k<-';'gd-';'m<-'];
    style2 = ['bo-';'rs-';'k<-';'gd-';'m<-'];
    rateStyle2 = ['b--';'r--';'k--';'g--';'m--'];

    lWidth = 2;
    fSize = 24;
    mSize = 15;

    % reference equation index for 3rd order line
    offset2 = 10;
    offset = 0.2;

    
    % L2 norm error
    for iEq = iEqBegin:iEqEnd
        figure(iEq)
        for i = 1:fileNum
            loglog(h{i}(:),L2{i}(iEq,:),style2(i,:),'linewidth',lWidth,'markersize',mSize)
            hold on
        end
        legend([legendNames],'location','best')
        hx = xlabel('DOF^-^1^/^2'); hy = ylabel('|\epsilon|_2');
        set(gca,'FontSize',fSize)
        set(hx,'FontSize',fSize)
        set(hy,'FontSize',fSize)
        
        for i = 1:fileNum
            rate = log(L2{i}(iEq,end-1)/L2{i}(iEq,end))/log(h{i}(end-1)/h{i}(end));
            hRef = h{i}(end-1:end);
            dhRef = (h{i}(end-1)-h{i}(end));
            hRef2 = hRef;
            hRef2(3) = hRef(end)-(0.15*dhRef);
            %loglog(hRef, L2{i}(iEq,end-1:end)*(1.75),rateStyle2(i,:),'linewidth',lWidth)
            %text(hRef(end)-(0.1*dhRef),1.25*L2{i}(iEq,end),sprintf('%0.3g',rate),'FontSize',13,'HorizontalAlignment','right')
            offset = 1.75;

            if ( i == 1 )
                loglog(hRef2, (hRef2(:)).^rate/(hRef2(1)^rate)*0.6*L2{i}(iEq,end-1),rateStyle2(i,:),'linewidth',lWidth);
                text(hRef2(end),(hRef2(end)).^rate/(hRef2(1)^rate)*0.5*L2{i}(iEq,end-1),sprintf('%0.3g',rate),'FontSize',.8*fSize,'HorizontalAlignment','left')
            else
                loglog(hRef2, (hRef2(:)).^rate/(hRef2(1)^rate)*offset*L2{i}(iEq,end-1),rateStyle2(i,:),'linewidth',lWidth);
                text(hRef2(end),(hRef2(end)).^rate/(hRef2(1)^rate)*offset*L2{i}(iEq,end-1),sprintf('%0.3g',rate),'FontSize',.8*fSize,'HorizontalAlignment','right')
            end
        end 
        hold off;

        % change the size of figure
        figWidth = 24;
        figHeight = 20;
        set(gcf, 'paperunits', 'centimeters')
        set(gcf, 'paperpositionmode', 'manual')
        set(gcf, 'papersize', [figWidth figHeight])
        set(gcf, 'paperposition', [0 0 figWidth figHeight])
        set(gcf, 'renderer', 'painters')
        %set(findall(gcf, '-property', 'FontSize'), 'FontSize', fSize)
        %set(findall(gcf, '-property', 'FontName'), 'FontName', 'Times New Roman')


        if figOutputOn
            if consVar
                fname2 = [baseDir,'/','conserv_errorL2_AF_DG_comp',num2str(iEq),'.eps'];
            else
                fname2 = [baseDir,'/','errorL2_AF_DG_comp',num2str(iEq),'.eps'];
            end
            print('-depsc2', '-r300', fname2);
        end

    end


    for iEq = iEqBegin:iEqEnd
        figure(iEq+10)
        for i = 1:fileNum
            loglog(WU{i}(:),L2{i}(iEq,:),style2(i,:),'linewidth',lWidth,'markersize',mSize)
            hold on
            %if ( iEq == nEqns && i == fileNum )

                %rate = 2;
                %iFile = 1;
                %loglog(hRef(:),offset2*(hRef(:)).^rate/(hRef(1)^rate)*L2{iFile}(iEqRef,1),'k--','linewidth',lWidth)    % 3rd order
                %text(0.8*hRef(end),offset2*(hRef(end)).^rate/(hRef(1)^rate)*L2{iFile}(iEqRef,1),sprintf('%d',rate),'FontSize',fSize)

                %rate = 3;
                %iFile = fileNum;
                %loglog(hRef(:),offset*(hRef(:)).^rate/(hRef(1)^rate)*L2{iFile}(iEqRef,1),'k--','linewidth',lWidth)    % 3rd order
                %text(0.8*hRef(end),offset*(hRef(end)).^rate/(hRef(1)^rate)*L2{iFile}(iEqRef,1),sprintf('%d',rate),'FontSize',fSize)
                
                %legend([legendNames],'location','best')
                legend([legendNames],'location','SouthWest')

                hx = xlabel('Work Unit'); hy = ylabel('|\epsilon|_2');
                set(gca,'FontSize',fSize)
                set(hx,'FontSize',fSize)
                set(hy,'FontSize',fSize)
            %end
        end
        %if figOutputOn
        %    if consVar
        %        fname3 = [baseDir,'/','conserv_errorL2_WorkUnit_AF_DG_comp',num2str(iEq),'.eps'];
        %    else
        %        fname3 = [baseDir,'/','errorL2_WorkUnit_AF_DG_comp.eps'];
        %    end
        %    print('-depsc2', '-r300', fname3);
        %end
        hold off;

    end

    
    %for iEq = iEqBegin:iEqEnd
    %    figure(iEq+20)
    %    for i = 1:fileNum
    %        loglog(totalTime{i}(:),L2{i}(iEq,:),style2(i,:),'linewidth',lWidth,'markersize',fSize)
    %        hold on
    %        %if ( iEq == nEqns && i == fileNum )

    %            %rate = 2;
    %            %iFile = 1;
    %            %loglog(hRef(:),offset2*(hRef(:)).^rate/(hRef(1)^rate)*L2{iFile}(iEqRef,1),'k--','linewidth',lWidth)    % 3rd order
    %            %text(0.8*hRef(end),offset2*(hRef(end)).^rate/(hRef(1)^rate)*L2{iFile}(iEqRef,1),sprintf('%d',rate),'FontSize',fSize)

    %            %rate = 3;
    %            %iFile = fileNum;
    %            %loglog(hRef(:),offset*(hRef(:)).^rate/(hRef(1)^rate)*L2{iFile}(iEqRef,1),'k--','linewidth',lWidth)    % 3rd order
    %            %text(0.8*hRef(end),offset*(hRef(end)).^rate/(hRef(1)^rate)*L2{iFile}(iEqRef,1),sprintf('%d',rate),'FontSize',fSize)
    %            
    %            %legend([legendNames],'location','best')
    %            legend([legendNames],'location','SouthWest')

    %            hx = xlabel('Wall clock time'); hy = ylabel('|\epsilon|_2');
    %            set(gca,'FontSize',fSize)
    %            set(hx,'FontSize',fSize)
    %            set(hy,'FontSize',fSize)
    %        %end
    %    end
    %    if figOutputOn
    %        fname3 = [baseDir,'/','conserv_errorL2_Walltime_AF_DG_comp',num2str(iEq),'.eps'];
    %        print('-depsc2', '-r300', fname3);
    %    end
    %    hold off;

    %end


end

function [h, DOF, L1, L2] = readErrData(file)
% READERRDATA(FILE) returns the L1 and L2 norm errors for the solution 
%   contained in FILE

    A = load(file);

    [totalLevs, cols] = size(A);
    
    % determine the number of equations
    nEqns = (cols-3)/2;
    %nEqns = 4;
    
    % initialize output variables
    nCells = zeros(totalLevs,1);
    DOF = zeros(totalLevs,1);
    h = zeros(totalLevs,1); 
    L1 = zeros(nEqns,totalLevs);
    L2 = zeros(nEqns,totalLevs);
        
    for lev = 1:totalLevs
        h(lev) = A(lev,3);   % cell size, 1/sqrt(dof)
        nCells(lev) = A(lev,1);   % nCells
        DOF(lev) = A(lev,2);   % nCells + nEdges + nNodes
        %h(lev) = 1.0/sqrt(A(lev,2)-A(lev,1));   % cell size, 1/sqrt(dof)
        %h(lev) = 1.0/sqrt(3.0*A(lev,1));   % cell size, 1/sqrt(dof)
        for iEq = 1:nEqns
            L1(iEq,lev) = A(lev,3+iEq);
            L2(iEq,lev) = A(lev,3+iEq+nEqns);
        end
    end
            
end

function [nIter,totalCompTime] = readTimeData(file)
% READTIMEdATA(FILE) reads computation time data

    A = load(file);

    [totalLevs, cols] = size(A);
    
    % initialize output variables
    nIter = zeros(totalLevs,1);
    totalCompTime = zeros(totalLevs,1);
        
    for lev = 1:totalLevs
        nIter(lev) = A(lev,1);   % total iteration
        totalCompTime(lev) = A(lev,2);   % total computation time
    end
            
end

%% L1 norm error
    %for i = 1:fileNum
    %    figure(1)
    %    %figure(i)
    %    for iEq = iEqBegin:iEqEnd
    %        %loglog(h{i}(:),L1{i}(iEq,:),style(iEq,:),'linewidth',lWidth,'markersize',fSize)
    %        loglog(h{i}(:),L1{i}(iEq,:),style2(i,:),'linewidth',lWidth,'markersize',fSize)
    %        hold on
    %        if ( iEq == nEqns && i == fileNum  )

    %            %rate = 2;
    %            %iFile = 1;
    %            %loglog(hRef(:),offset2*(hRef(:)).^rate/(hRef(1)^rate)*L1{iFile}(iEqRef,1),'k--','linewidth',lWidth)    % 3rd order
    %            %text(0.8*hRef(end),offset2*(hRef(end)).^rate/(hRef(1)^rate)*L1{iFile}(iEqRef,1),sprintf('%d',rate),'FontSize',fSize)

    %            rate = 3;
    %            iFile = fileNum;
    %            loglog(hRef(:),offset*(hRef(:)).^rate/(hRef(1)^rate)*L1{iFile}(iEqRef,1),'k--','linewidth',lWidth)    % 3rd order
    %            text(0.8*hRef(end),offset*(hRef(end)).^rate/(hRef(1)^rate)*L1{iFile}(iEqRef,1),sprintf('%d',rate),'FontSize',fSize)

    %            legend([legendNames],'location','best')

    %            hx = xlabel('DOF^-^1^/^2'); hy = ylabel('|\epsilon|_1');
    %            set(gca,'FontSize',fSize)
    %            set(hx,'FontSize',fSize)
    %            set(hy,'FontSize',fSize)
    %            
    %        end
    %    end
    %    
    %%end

    %if triRateOn
    %    % triangle rate!
    %    offset2 = 10;
    %    iFile = 1;
    %    rate = 2;
    %    % triangle rate!
    %    tri_x = hRef(end:-1:end-1);
    %    tri_y = (hRef(end:-1:end-1)).^rate/(hRef(end-1)^rate)*L1{iFile}(iEqRef,end);
    %    %tri_y = interp1(hRef, L1{iFile}(iEqRef,:), tri_x);
    %    % down side up
    %    loglog(tri_x([1,1,2,1]), offset2*tri_y([1,2,2,1]),'k-'); %,'linewidth',lWidth)    % triangle  
    %    % right side up
    %    %loglog(tri_x([1,2,2,1]), offset2*tri_y([1,1,2,1]),'k--','linewidth',lWidth)    % triangle  
    %    %rate = log(L1{iFile}(iEqRef,end-1)/L1{iFile}(iEqRef,end))/log(h{iFile}(end-1)/h{iFile}(end));
    %    text(mean(tri_x([1,1,2,1])),mean(offset2*tri_y([1,2,2,1])),sprintf('%d', rate),'FontSize',fSize) % located above
    %    %text(mean(tri_x([1])),0.5*offset2*mean(tri_y([1])),sprintf('%3.2f', rate),'FontSize',fSize) % located below
    %    
    %    offset2 = 0.9;
    %    iFile = 2;
    %    rate = 3;
    %    % triangle rate!
    %    tri_x = hRef(end:-1:end-1);
    %    tri_y = (hRef(end:-1:end-1)).^rate/(hRef(end-1)^rate)*L1{iFile}(iEqRef,end);
    %    %tri_y = interp1(hRef, L1{iFile}(iEqRef,:), tri_x);
    %    %% down side up
    %    %loglog(tri_x([1,1,2,1]), offset2*tri_y([1,2,2,1]),'k--','linewidth',lWidth)    % triangle  
    %    % right side up
    %    loglog(tri_x([1,2,2,1]), offset2*tri_y([1,1,2,1]),'k-'); %,'linewidth',lWidth)    % triangle  
    %    %rate = log(L1{iFile}(iEqRef,end-1)/L1{iFile}(iEqRef,end))/log(h{iFile}(end-1)/h{iFile}(end));
    %    text(mean(tri_x([1,2,2,1])),mean(offset2*tri_y([1,1,2,1])),sprintf('%d', rate),'FontSize',fSize) % located above
    %    %text(mean(tri_x([1])),0.5*offset2*mean(tri_y([1])),sprintf('%3.2f', rate),'FontSize',fSize) % located below
    %end

    %if figOutputOn
    %    if consVar
    %        fname1 = [baseDir,'/','conserv_errorL1.eps'];
    %    else
    %        fname1 = [baseDir,'/','errorL1.eps'];
    %    end
    %    print('-depsc2', '-r300', fname1);
    %end
    %hold off;

