function [inflowEdgeCoord, inflowNodeData] = plotInflowOutflow(dir,fname)
% Routine to open and read reconFile that is generated from AF solver and
% plot the inflow and outflow edge solutions. Used for steady state
% advection solutions
    close all;
    
    dir = '../cases/steady';
    fname = 'stream2SC';
    
    % open file
    fid = fopen([dir,'/',fname,'.dat'],'r');
    
    % assign constants and parameters
    data = fread(fid, [1,3], 'int');
    nDim = data(1); nEqns = data(2); nIter = data(3);
    data = fread(fid, [1,5], 'int');
    maxNodesPerCell = data(1); maxFacesPerCell = data(2); nNodes = data(3); 
    nEdges = data(4); nCells = data(5);
    
    % mesh information
    for iNode = 1:nNodes
        nodeCoord(:,iNode) = fread(fid,[nDim,1],'double');
    end
    for iEdge = 1:nEdges
        edgeCoord(:,iEdge) = fread(fid,[nDim,1],'double');
    end
    for iCell = 1:nCells
        data = fread(fid, [(maxNodesPerCell+maxFacesPerCell),1],'int'); % 3 is for maxFacesPerCell
        cellNodes(:,iCell) = data(1:maxNodesPerCell);
        cellFaces(:,iCell) = data(maxNodesPerCell+1:end);
    end
    
    
    % collect solution information
    iter = 0;
    while iter < 1000
        % select the data at iter == 1000, discard everything else
        data1 = fread(fid,[1,1], 'int');
        iter = data1(1);
        data1 = fread(fid,[1,1], 'double');
        t = data1(1); 
        
        for iCell = 1:nCells
            cellAvg(:,iCell) = fread(fid, [nEqns,1], 'double');
        end

        for iNode = 1:nNodes
            nodeData(:,iNode) = fread(fid, [nEqns,1], 'double');
        end

        for iEdge = 1:nEdges
            edgeData(:,iEdge) = fread(fid, [nEqns,1], 'double');
        end
        
    end

    % close file
    fclose(fid);

    
    % select inflow data
    inflowNode = 0;
    outflowNode = 0;
    for iNode = 1:nNodes
        if nodeCoord(2,iNode) == -5
            inflowNode = inflowNode + 1;
            inflowNodeCoord(:,inflowNode) = nodeCoord(:,iNode);
            inflowNodeData(:,inflowNode) = nodeData(:,iNode);
        elseif nodeCoord(2,iNode) == 5
            outflowNode = outflowNode + 1;
            outflowNodeCoord(:,outflowNode) = nodeCoord(:,iNode);
            outflowNodeData(:,outflowNode) = nodeData(:,iNode);
        end
    end
    inflowEdge = 0;
    outflowEdge = 0;
    for iEdge = 1:nEdges
        if edgeCoord(2,iEdge) == -5
            inflowEdge = inflowEdge + 1;
            inflowEdgeCoord(:,inflowEdge) = edgeCoord(:,iEdge);
            inflowEdgeData(:,inflowEdge) = edgeData(:,iEdge);
        elseif edgeCoord(2,iEdge) == 5
            outflowEdge = outflowEdge + 1;
            outflowEdgeCoord(:,outflowEdge) = edgeCoord(:,iEdge);
            outflowEdgeData(:,outflowEdge) = edgeData(:,iEdge);
        end
    end            

    fSize = 16;
    
    figure(1)
    h1 = plot(inflowNodeCoord(1,:),inflowNodeData(1,:), 'b.');
    hold on
    plot(inflowEdgeCoord(1,:),inflowEdgeData(1,:), 'bo', 'markersize', 8)
    h2 = plot(outflowNodeCoord(1,:),outflowNodeData(1,:), 'r.');
    plot(outflowEdgeCoord(1,:),outflowEdgeData(1,:), 'ro', 'markersize', 8)   
    xlabel('x', 'fontsize', fSize)
    ylabel('u', 'fontsize', fSize)
    legend([h1, h2], 'Inflow', 'Outflow')
    axis([-5 5 -0.2 1.2])
    fname2 = [dir,'/','inflowOutflow',fname,'.eps'];
    print('-depsc2', fname2);
    
%     figure(2)
%     plot(linspace(-5,5,20), cellAvg(1,1:2:40),'bo')
%     hold on
%     plot(linspace(-5,5,20), cellAvg(1,161:2:200),'ro')
    
end


