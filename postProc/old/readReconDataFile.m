function [] = readReconDataFile()
% Routine to open and read reconData_nCells.dat file to access reconData
% outputAux2 routine in fortran code generates the data file
% 10/9/2014
    
    dirRD = '../' ; %'../cases/alignedflow/RD/BDBNU/';
    dirChOrg = '../' ; %'../cases/alignedflow/ChOrg/BDBNU/';
    fnameRD = 'RD';
    fnameChOrg = 'ChOrg';    
    
    % open files
    reconDataRD = dataRead([dirRD,fnameRD,'.dat']);
    reconDataChOrg = dataRead([dirChOrg,fnameChOrg,'.dat']);    

    [nCells, col] = size(reconDataRD);

    [nCells, col] = size(reconDataChOrg);

% node and edge update order
    figure(10) 
    plot((reconDataRD(:,1)), (reconDataRD(:,2)),'o', 'markersize', 8)
    hold on
    plot((reconDataChOrg(:,1)), (reconDataChOrg(:,2)),'rs')

    figure(11)
    plot((reconDataRD(:,1)), (reconDataRD(:,2))-(reconDataChOrg(:,2)),'o', 'markersize', 8)


%     % compare edge and/or node data for a specific cell
%     for iCell = 1:nCells
%         for icol = 3:3
%             uCompare(iCell,icol) = reconDataRD(iCell,icol) - reconDataChOrg(iCell,icol);
%         end
%     end
%     figure(1)
%     plot(uCompare)
%     figure(2)
%     plot(reconDataRD,'o', 'markersize', 15)
%     hold on
%     plot(reconDataChOrg,'rs', 'markersize', 15)
% 




end

function [reconData] = dataRead(file)
% DATAREAD(FILE) reads the data witin FILE, skipping header

    fid = fopen(file);
    Data =textscan(fid, '%f %f', 'HeaderLines', 0);
    fclose(fid);
    reconData = cell2mat(Data);

end