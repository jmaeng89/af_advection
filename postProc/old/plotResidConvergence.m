function[] = plotResidConvergence(dir,fileNames)
% PLOTRESIDCONVERGENCE(DIR,FILENAMES) plots the residual convergence history 
%   for a series of solution files
%
    close all;
    
    dir = '../cases/steady';

    prefix = 'auxConvg';
    
    fileNames = {'Org','BDEquiDist','BDStream1', ...
        'BDStream2'};
    
    solType = 'G';  
    
    [~,fileNum] = size(fileNames);
    
    % Allocate variables for error and spacing
    for i = 1:fileNum
        FN = [dir,'/',prefix,fileNames{i},solType,'.dat'];     % complete file location
        [fIter, fT, fResid, fBubble] = readResidSol(FN);
        iT(i,:) = fIter;
        t(i,:) = fT; 
        uResid(i,:) = fResid;
        bubble(i,:) = fBubble; % bubble function 
    end
    

    style = ['b-';'r-';'k-';'g-';'m-'];
    lWidth = 2;
    fSize = 16;
    
    % u residual convergence history
    figure(1)
    for i = 1:fileNum
        semilogy(iT(i,1:800),uResid(i,1:800),style(i,:),'linewidth',lWidth,'markersize',fSize)
        hold on
    end
    legend([fileNames],'location','best')
    hx = xlabel('Iteration'); hy = ylabel(['|R|_2']);
    hold off
    set(gca,'FontSize',fSize)
    set(hx,'FontSize',fSize)
    set(hy,'FontSize',fSize)
%     fname1 = [dir,'/','residConvg',solType,'.eps'];
%     print('-depsc2', fname1);    

    % bubble function convergence history
    figure(2)
    for i = 1:fileNum
        semilogy(iT(i,1:800),bubble(i,1:800),style(i,:),'linewidth',lWidth,'markersize',fSize)
        hold on
    end
    legend([fileNames],'location','best')
    hx = xlabel('Iteration'); hy = ylabel(['|c7|_2']);
    hold off
    set(gca,'FontSize',fSize)
    set(hx,'FontSize',fSize)
    set(hy,'FontSize',fSize)
%     fname1 = [dir,'/','bubbleConvg',solType,'.eps'];
%     print('-depsc2', fname1);    
        
end

function [fIter, fT, fResid, fBubble] = readResidSol(file)
% SOLERR(FILE) returns the L_1 and L_2 error norm for the solution 
%   contained in FILE

    A = load(file);

    [totalLevs, cols] = size(A);
    
    % initialize output variables
    fIter = zeros(totalLevs,1); 
    fT = zeros(totalLevs,1);
    fResid = zeros(totalLevs,1);
    fBubble = zeros(totalLevs,1);

    for lev = 1:totalLevs
        fIter(lev) = A(lev,1); 
        fT(lev) = A(lev,2);
        fResid(lev) = A(lev,3);
        fBubble(lev) = A(lev,4);
    end

end
