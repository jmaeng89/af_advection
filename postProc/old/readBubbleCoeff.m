% function [] = readBubbleCoeff()
% 11/12/2014
% read bubble coefficients from 'bubbleCoeff.dat'

    % directory and the name of file
    dir = '../cases/equilateral/';
    fname = 'bubbleCoeff.dat';

    % file open
    fid = fopen([dir,fname], 'r');
    
    % read line by line to obtain information about the mesh
    for iSkip = 1:5
        % skip all header
        temp = str2num(fgetl(fid));
    end
    
    line = str2num(fgetl(fid));
    nCells = line;

    it = 1;
   
    while 1
        f = (fgetl(fid));
        if ~ischar(f), break, end
        % iteration number and time
        line = str2num(f);
        iTer(it) = line(1); t(it) = line(2);
        % centroid and bubble coefficient
        for iCell = 1:nCells
            line = str2num(fgetl(fid));
            xCent(:,iCell) = line(1:2);
            bubble(it,iCell) = line(3);
        end
        it = it + 1;
    end        
    fclose(fid);

%%
    deg0Cells1 = [81:2:100];
    deg0Cells2 = [82:2:100];
    dl = 1.6;   % face length of equilateral triangle
    figure(1)
    hold on
    for it = 1:100
        for iCell = deg0Cells1
            r = xCent(1,iCell); 
            plot(r, bubble(it,iCell), 'bo', 'markersize', 15)
        end
        for iCell = deg0Cells2
            r = xCent(1,iCell); 
            plot(r, bubble(it,iCell), 'rs')
        end
        M(it) = getframe;
    end
    
    movie(M)
    
    
    
    
% end