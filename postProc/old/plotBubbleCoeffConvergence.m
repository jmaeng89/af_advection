function plotBubbleCoeffConvergence(dir,fileNames)
% PLOTBUBBLECOEFCONVERGENCE(DIR,FILENAMES) plots the bubble coefficient 
%   convergence for a series of solution files
%
%   DIR is the directory path. FILENAMES contains names of data files 
%   in COLUMN structure format. 
    close all;
    
    dir = '../cases/lts';
    deg = {'deg440' 'deg445', 'deg450', 'deg455', 'deg460'};
    fileNames = {'BDBNU/bubbleCoeffHist200','BDBNU/bubbleCoeffHist800', ...
                 'BDBNU/bubbleCoeffHist3200','BDBNU/bubbleCoeffHist12800', ...
                 'BDBNU/bubbleCoeffHist51200'}; 
     
    [~,dirNum] = size(deg);
    [~,fileNum] = size(fileNames);
    
    % plot solution convergence 
    style = ['-bo';'-rs';'-k<';'-g*'; '-mv'; ...
             ':bh';':rd';':kp';':g>'; ':mx'; ':y+'];
    lWidth = 2;
    fSize = 16;    
    
    % DOF^-1/2
    h = [3.949763e-02; 2.007644e-02; 1.012169e-02; 5.081907e-03; 2.546244e-03];

    
    % open a file to store the convergence study data
    fid = fopen([dir,'/','bubbleCoeffHistBDBNU.dat'],'a');
    
    % Allocate variables for error and spacing
    for iDir = 1:dirNum
        
        for lev = 1:fileNum
            FN = [dir,'/',deg{iDir},'/',fileNames{lev},'.dat'];     % complete file location
            [fh, fL2] = dataRead(FN);
            time = fh;          % time
            L2 = fL2;           % L2 norm of bubble coeff
            c7Norm(iDir,lev) = fL2(end);   % converged c7norm list

            figure(1)
            semilogy(fh, fL2, style(lev,:), 'linewidth', lWidth, 'markersize',3);
            hold on

        end

        legend('0', '1', '2', '3', '4','location','best')
        hx = xlabel('Time(s)'); hy = ylabel(['|c_7|_2']);
        hold off
        set(gca,'FontSize',fSize)
        set(hx,'FontSize',fSize)
        set(hy,'FontSize',fSize)
        fname1 = [dir,'/','bubbleCoeffBDBNU',deg{iDir},'.eps'];
        print('-depsc2', fname1);

        figure(2)
        loglog(h, c7Norm(iDir,:), style(iDir,:), 'linewidth',lWidth,'markersize',fSize);
        hold on    
        if ( iDir == 1 ) 
            loglog(h,(h.^3/(h(1)^3))*c7Norm(iDir,1), 'k--')    % 3rd order
            loglog(h,(h.^1/(h(1)^1))*c7Norm(iDir,1), 'k:')    % 1st order
        end
        legend('c7', '3rd', '1st')
        fname2 = [dir,'/','bubbleCoeffConvBDBNU.eps'];
        print('-depsc2', fname2);
        
        for lev = 1:fileNum
            if ( lev == 1 )
                fprintf('\n %s \n',deg{iDir});
                fprintf('%5s  %12s   %12s  %7s\n','Level','h','c7L2Norm','Order')
                fprintf('%5s  %12s   %12s  %7s\n','-----','-----','------------','-------')
                fprintf('%5d  %e   %e   %5s \n',lev,h(lev),c7Norm(iDir,lev),'');
            else
                fprintf('%5d  %e   %e  %7.4f\n',...
                        lev,h(lev),c7Norm(iDir,lev),log(c7Norm(iDir,lev-1)/c7Norm(iDir,lev))/log(h(lev-1)/h(lev)))
            end

        end
        
        for lev = 1:fileNum
            if ( lev == 1 )
                fprintf(fid, '\n %s \n',deg{iDir});
                fprintf(fid, '%5s  %12s  %12s  %7s\n','Level','h','c7L2Norm','Order');
                fprintf(fid, '%5s  %12s   %12s  %7s\n','-----','-----','------------','-------');
                fprintf(fid, '%5d %s %e %s %e %s %7s %s\n',lev,'&',h(lev),'&',c7Norm(iDir,lev),'&','','\\');
            else
                fprintf(fid, '%5d %s %e %s %e %s %7.4f %s\n',...
                        lev,'&',h(lev),'&', c7Norm(iDir,lev),'&',log(c7Norm(iDir,lev-1)/c7Norm(iDir,lev))/log(h(lev-1)/h(lev)), '\\');
            end

        end        
        
    end

    fclose(fid)


    

%     dir = '../cases/RD/deg40/BNDBU';
%     fileNames = {'bubbleCoeffHist200','bubbleCoeffHist800','bubbleCoeffHist3200','bubbleCoeffHist12800', 'bubbleCoeffHist51200'}; 
%      
%     [~,fileNum] = size(fileNames);
%     h = [3.949763e-02, 2.007644e-02, 1.012169e-02, 5.081907e-03, 2.546244e-03];
%     
%     % Allocate variables for error and spacing
%     for i = 1:5
%         FN = [dir,'/',fileNames{i},'.dat'];     % complete file location
%         [fL2] = dataRead(FN);
%         L2(i) = fL2;      % L2 norm of error
%     end
%     
%     % open a file to store the convergence study data
%     fid = fopen([dir,'/','bubbleHist.dat'],'w');
%     
%     for lev = 1:5
%         if ( lev == 1 )
%             fprintf(fid, '%5s  %12s  %12s\n','Level','h','L2Error');
%             fprintf(fid, '%5s  %12s  %12s\n','-----','-----','------------');
%             fprintf(fid, '%5d %e %e \n', lev, h(lev), L2(lev));
%         else
%             fprintf(fid, '%5d %e %e \n', lev, h(lev), L2(lev));
%         end
% 
%     end
%     fclose(fid)
    


%     % Allocate variables for c7 norm and iterations
%     for i = 1:fileNum
%         FN = [dir,'/',fileNames{i},'.dat'];     % complete file location
%         
%         [iters, time, c7norm] = dataRead(FN);
% 
%         figure(1)
%         semilogy(time,c7norm,style(i,:),'linewidth',lWidth,'markersize',2);
%         hold on
%         
%     end
%     legend('n200', 'n800', 'n3200', 'n12800','location','best')
%     hx = xlabel('Time(s)'); hy = ylabel(['|c_7|_2']);
%     hold off
%     set(gca,'FontSize',fSize)
%     set(hx,'FontSize',fSize)
%     set(hy,'FontSize',fSize)
%     fname1 = [dir,'/','bubbleCoeffconvergence_L2.eps'];
%     print('-depsc2', fname1);
    

    
end

function [time, L2] = dataRead(file)
% DATAREAD(FILE) reads the data witin FILE, skipping header

    fid = fopen(file);
    Data = textscan(fid, '%f %f %f', 'HeaderLines', 8);
    fclose(fid);
    Data = cell2mat(Data);
    
    [totalLevs, cols] = size(Data);
    
    % initialize output variables
    time = zeros(totalLevs,1); 
    L2 = zeros(totalLevs,1);
    
    for lev = 1:totalLevs
        time(lev) = Data(lev,2);   % time
        L2(lev) = Data(lev,3);
    end
end


function [h, L2] = solErr(file)
% SOLERR(FILE) returns the L_1 and L_2 error norm for the solution 
%   contained in FILE

    fid = fopen(file);
    Data =textscan(fid, '%f %f %f', 'HeaderLines', 2);
    fclose(fid);
    Data = cell2mat(Data);
    
    [totalLevs, cols] = size(Data);
    
    % initialize output variables
    h = zeros(totalLevs,1); 
    L2 = zeros(totalLevs,1);
    
    for lev = 1:totalLevs
        h(lev) = Data(lev,2);   % cell size, 1/sqrt(dof)
        L2(lev) = Data(lev,3);
    end

end