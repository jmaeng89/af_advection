function [Int, angle] = unitCircleLocs()
% 10/30/2014
% Returns cell indicies corresponding to the advected locations, in order
% to evaluate errors associated with various angles.

close all
clear all

gridDir = '../grids/';
gridFile = 'diag032'
    
% file open
fid = fopen([gridDir,gridFile,'.grd'], 'r');

% read line by line to obtain information about the mesh
line1 = str2num(fgetl(fid));
line2 = str2num(fgetl(fid));

% assign line1 and line2 information to variables
nDim = line1(1); nZones = line1(2); nPatches = line1(3);
nNodes = line2(1); nFaces = line2(2); nCells = line2(3); 
maxNodesPerFace = line2(4); maxFacesPerCell = line2(5);

% read X = (x, y) coordinates for each node
for iNode = 1:nNodes

    X(:,iNode) = str2num(fgetl(fid));

end

cellFaces = zeros(maxFacesPerCell,nCells);
% Face information
for iFace = 1:nFaces

    tempLine = str2num(fgetl(fid));

    switch nDim

        case 1    
            nodesPerFace(1,iFace) = tempLine(1);        % number of nodes in face
            faceNodes(1,iFace) = tempLine(2);       % nodes consisting face
            faceCells(1:2,iFace) = tempLine(3:4);        % current cell, neighbor cell

        case 2   
            nodesPerFace(1,iFace) = tempLine(1);        % number of nodes in face
            faceNodes(1:2,iFace) = tempLine(2:3);       % nodes consisting face
            faceCells(1:2,iFace) = tempLine(4:5);       % current cell, neighbor cell

    end

end
fclose(fid);

% go around cellInfo and plot face of current cell
for iFace = 1:nFaces

    switch nDim

        case 1

            xEdge = [X(faceNodes(1,iFace))];
            yEdge = [X(faceNodes(1,iFace))];

        case 2

            lCell = faceCells(1,iFace);  % current cell number
            rCell = faceCells(2,iFace);  % neighbor cell number 
            xEdge = [X(1,faceNodes(1,iFace)), X(1,faceNodes(2,iFace))];
            yEdge = [X(2,faceNodes(1,iFace)), X(2,faceNodes(2,iFace))];

            % faces contained in a cell
            for iSide = 1:2
                jCell = faceCells(iSide,iFace);   
                if ( jCell > 0 )
                    if cellFaces(1,jCell) == 0
                        cellFaces(1,jCell) = iFace;
                    else
                        jFace = 2;
                        while ( abs(cellFaces(jFace,jCell)) > 0 )
                            jFace = jFace + 1;
                        end
                        cellFaces(jFace,jCell) = iFace;
                    end
                end
            end
    end

end


% assuming structured triangular cells
Nx = sqrt(nCells/2);    % number of cells in x direction
dx = (max(X(1,:)) - min(X(1,:)))/Nx    % dx
Dx = 4.25;  % total distance travelled, constant
NxTotal = (Dx)/(dx)    % total number of cells travelled, number of iterations*dt
T = Dx  % total time fixed, fixing norm(V) = 1
xInd = [NxTotal:-1:0];
yInd = sqrt(NxTotal^2-xInd.^2);

l = 1;
for ii = 1:length(yInd)
    if mod(yInd(ii),1) == 0 || abs(mod(yInd(ii),1)) < 1/3
        % index of cells that are close to integers
        intInd(1,l) = xInd(ii); 
        intInd(2,l) = yInd(ii);
        Ntotal(l) = (xInd(ii)^2 + yInd(ii)^2); % make sure this is close to NxTotal^2
        angle(l) = (atan(yInd(ii)/xInd(ii)));% angle from index
        l = l+1;
    end
end

% angle2 = zeros(1,length(angle));
Ntotal
rad2deg(angle);
angle2 = [angle, angle(2:end)+pi/2];
fprintf('angles\n')
for ind = 1:length(angle2)
    fprintf('%24.18f \n', angle2(ind))
end


figure()
hold on

% 
% select an interior point of a cell, centroid
% pointInt1 = [0.50/3,0.25/3];    % original cell
pointInt1Vals = [0.50/3,0.25/3, 0.25/3,0.50/3, -0.25/3,0.25/3, -0.5/3,0.5/3, ...
    -0.25/3,-0.5/3, -0.5/3,-0.25/3, 0.5/3,-0.5/3, 0.25/3,-0.25/3 ];

for iCell = 1:8
    pointInt1 = pointInt1Vals(2*(iCell-1)+1:2*(iCell-1)+2);
plot(pointInt1(1), pointInt1(2), 'bo')
for ind = 1:length(angle2);
    V = [cos(angle2(ind)), sin(angle2(ind))]; % advection speed according to angle
    fprintf('angle %24.18f :\n', angle2(ind))

    pointInt2 = pointInt1 + V*T;
    plot(pointInt2(1), pointInt2(2), 'ro')

    for iCell = 1:nCells
        % loop over cell nodes to find the cell that contains the point of
        % interest
        xFace = [X(1,faceNodes(1,cellFaces(1,iCell))), ...
                 X(1,faceNodes(1,cellFaces(2,iCell))), ...
                 X(1,faceNodes(1,cellFaces(3,iCell))), ...
                 X(1,faceNodes(2,cellFaces(1,iCell))), ...
                 X(1,faceNodes(2,cellFaces(2,iCell))), ...
                 X(1,faceNodes(2,cellFaces(3,iCell)))];
        yFace = [X(2,faceNodes(1,cellFaces(1,iCell))), ...
                 X(2,faceNodes(1,cellFaces(2,iCell))), ...
                 X(2,faceNodes(1,cellFaces(3,iCell))), ...
                 X(2,faceNodes(2,cellFaces(1,iCell))), ...
                 X(2,faceNodes(2,cellFaces(2,iCell))), ...
                 X(2,faceNodes(2,cellFaces(3,iCell)))];    

        if (pointInt1(1) > min(xFace) && pointInt1(1) < max(xFace)) && ...
                (pointInt1(2) > min(yFace) && pointInt1(2) < max(yFace))
                fprintf('Origin cells: %d \n',iCell)

                xEdge1 = [X(1,faceNodes(1,cellFaces(1,iCell))), X(1,faceNodes(2,cellFaces(1,iCell)))];
                yEdge1 = [X(2,faceNodes(1,cellFaces(1,iCell))), X(2,faceNodes(2,cellFaces(1,iCell)))];
                xEdge2 = [X(1,faceNodes(1,cellFaces(2,iCell))), X(1,faceNodes(2,cellFaces(2,iCell)))];
                yEdge2 = [X(2,faceNodes(1,cellFaces(2,iCell))), X(2,faceNodes(2,cellFaces(2,iCell)))];
                xEdge3 = [X(1,faceNodes(1,cellFaces(3,iCell))), X(1,faceNodes(2,cellFaces(3,iCell)))];
                yEdge3 = [X(2,faceNodes(1,cellFaces(3,iCell))), X(2,faceNodes(2,cellFaces(3,iCell)))];

                plot( xEdge1, yEdge1, 'b-');
                plot( xEdge2, yEdge2, 'b-');
                plot( xEdge3, yEdge3, 'b-');

                text( mean(xFace), mean(yFace), num2str(iCell), 'color', 'b' ) % cell number 
        end

        if (pointInt2(1) > min(xFace) && pointInt2(1) < max(xFace)) && ...
                (pointInt2(2) > min(yFace) && pointInt2(2) < max(yFace))
                fprintf('Advected cells: %d \n',iCell)

                xEdge1 = [X(1,faceNodes(1,cellFaces(1,iCell))), X(1,faceNodes(2,cellFaces(1,iCell)))];
                yEdge1 = [X(2,faceNodes(1,cellFaces(1,iCell))), X(2,faceNodes(2,cellFaces(1,iCell)))];
                xEdge2 = [X(1,faceNodes(1,cellFaces(2,iCell))), X(1,faceNodes(2,cellFaces(2,iCell)))];
                yEdge2 = [X(2,faceNodes(1,cellFaces(2,iCell))), X(2,faceNodes(2,cellFaces(2,iCell)))];
                xEdge3 = [X(1,faceNodes(1,cellFaces(3,iCell))), X(1,faceNodes(2,cellFaces(3,iCell)))];
                yEdge3 = [X(2,faceNodes(1,cellFaces(3,iCell))), X(2,faceNodes(2,cellFaces(3,iCell)))];

                plot( xEdge1, yEdge1, 'b-');
                plot( xEdge2, yEdge2, 'b-');
                plot( xEdge3, yEdge3, 'b-');

                text( mean(xFace), mean(yFace), num2str(iCell), 'color', 'b' ) % cell number 
        end

    end

end

end


end