function [] = readReconFile()
% Routine to open and read reconFile that is generated from AF solver
    
    dir = '../';
    fname = 'gaussian002';
    
    % open file
    fid = fopen([dir,fname,'.dat'],'r');
    
    % assign constants and parameters
    data = fread(fid, [1,3], 'int');
    nDim = data(1); nEqns = data(2); nIter = data(3);
    data = fread(fid, [1,5], 'int');
    maxNodesPerCell = data(1); maxFacesPerCell = data(2); nNodes = data(3); 
    nEdges = data(4); nCells = data(5);
    
    % mesh information
    for iNode = 1:nNodes
        nodeCoord(:,iNode) = fread(fid,[nDim,1],'double');
    end
    for iEdge = 1:nEdges
        edgeCoord(:,iEdge) = fread(fid,[nDim,1],'double');
    end
    for iCell = 1:nCells
        data = fread(fid, [(maxNodesPerCell+maxFacesPerCell),1],'int'); % 3 is for maxFacesPerCell
        cellNodes(:,iCell) = data(1:maxNodesPerCell);
        cellFaces(:,iCell) = data(maxNodesPerCell+1:end);
    end
    
    % solution information collect
    for iter = 1:1; %nIter
        data1 = fread(fid,[1,2], 'double');
        t(iter) = data1(2); 
        
        for iCell = 1:nCells
            cellAvg(:,iCell+((iter-1)*nCells)) = fread(fid, [nEqns,1], 'double');
        end
        
        for iNode = 1:nNodes
            nodeData(:,iNode+((iter-1)*nNodes)) = fread(fid, [nEqns,1], 'double');
        end
        
        for iEdge = 1:nEdges
            edgeData(:,iEdge+((iter-1)*nEdges)) = fread(fid, [nEqns,1], 'double');
        end
    
    end
    % close file
    fclose(fid);
  
    
    
    % plot grid
    gridDir = '../grids/';
    gridFile = 'diag008';
    gridDisp(gridDir,gridFile);
    
    % plot solution
    iter = 1;  % choose an iteration number to display
%     iter2 = 3;
    hold on
    % go around cells and plot edge and node data
    for iEq = 1:nEqns

        for iNode = 1:nNodes
%             if nodeCoord(2,iNode) >= - 0.75 && nodeCoord(2,iNode) <= -0.5
            plot3( nodeCoord(1,iNode),nodeCoord(2,iNode),nodeData(iEq,iNode+((iter-1)*(nNodes))),'o' );
%             plot3( nodeCoord(1,iNode),nodeCoord(2,iNode),nodeData(iEq,iNode+((iter2-1)*(nNodes))),'ro' );
%             plot( nodeCoord(1,iNode),nodeCoord(2,iNode),'o' );
%             end
        end

        for iEdge = 1:nEdges
%             if edgeCoord(2,iEdge) >= -0.75 && edgeCoord(2,iEdge) <= -0.5
            plot3( edgeCoord(1,iEdge),edgeCoord(2,iEdge),edgeData(iEq,iEdge+((iter-1)*(nEdges))),'.' );
%             plot3( edgeCoord(1,iEdge),edgeCoord(2,iEdge),edgeData(iEq,iEdge+((iter2-1)*(nEdges))),'r.' );
            
%             plot( edgeCoord(1,iEdge),edgeCoord(2,iEdge),'.' );
%             end
        end 

    end


end
