!-------------------------------------------------------------------------------
! @purpose 
!  Provides visualization functions
!
! @author
!  J. Brad Maeng
!
! @history
!  5/3/2016 - Initial creation
!  10/20/2017 - ASCII output created
!
module output_tecplot

    use solverVars
    use meshUtil 
    use inputOutput, only: fileLength, filePath, &
                           solFileTmp, solFile, solFileName
    use reconstruction
    use update

    implicit none

    ! TecPlot parameter
    integer, parameter :: vnSubElem = 2  ! number of sub-cells per side of standard triangle

    integer :: nVars,            & ! TecPlot number of variables, only set once
               tecFileUnit         ! local file unit

contains
!-------------------------------------------------------------------------------
! @purpose 
!   Open tecplot data file and write file header in ASCII format  
!   Also initialize mesh and connectivity 
!
! @author
!   J. Brad Maeng
!
! @history
!   10/20/2017 - Initial creation
!
subroutine initTecPlotFileAscii(fileName,fileUnit)

    implicit none

    ! Interface variables
    character(fileLength), intent(in), optional :: fileName
    
    integer, intent(in), optional :: fileUnit

    ! Local variables
    integer :: ierr,             & ! error code
               idir,             & ! direction index
               iNode,            & ! node index
               iElem,            & ! element index
               vElem,            & ! element index
               i, j,             &
               vnNodePerElem,    & ! number of visualization node per elem    
               vnElem              ! number of visualization elements

    character(80) :: title,     & ! data set title
                     file_type, & ! file type
                     elem_type, & ! FE element type
                     fname        ! file name including the path

    character(80) :: varfmt,    &
                     varfmt2

    character(400) :: variables   ! variables in data set

    integer, allocatable :: vNodeConnect(:,:),  & ! viz node connectivity
                            ij2node(:,:)          ! i,j to node index array

    real(FP), allocatable :: vNodeCoord(:,:),   & ! viz node coordinate
                             vNodeData(:,:)       ! reconstruction data at nodes

    real(FP) :: dXi,        & ! xi spacing
                xi(nDim)      ! reference coordinates

    real(FP) :: sol_time    ! solution time

    
    title = 'Solution Reconstruction'
    file_type = 'FULL'
    sol_time = 0.0_FP

    select case (nDim)
      case (2)
        ! variables
        variables = '"x", "y",' ! coordinates
        variables = trim(adjustl(variables)) // & 
                    ' "P1", "P2", "P3" ' !, "P1EX", "P2EX", "P3EX" '
        ! total number of variables
        nVars = nDim + 3 

        ! Finite element mesh variables
        dXi = 1.0_FP / real(vnSubElem)
        select case ( maxFacesPerCell )
          case ( 3 ) 
            ! triangular
            elem_type = 'FETRIANGLE'
            vnNodePerElem = (vnSubElem+1)*(vnSubElem+2)/2
            vnElem = vnSubElem**2
            allocate( ij2node(vnSubElem+1,vnSubElem+1) ) 
          case ( 4 ) 
            ! quadrilateral
            elem_type = 'FEQUADRILATERAL'
            vnNodePerElem = (vnSubElem+1)*(vnSubElem+1)
            vnElem = vnSubElem**2
            allocate( ij2node(vnSubElem+1,vnSubElem+1) ) 
        end select

    end select

    allocate( vNodeConnect(maxFacesPerCell,nCells*vnElem), &
              vNodeCoord(nDim,vnNodePerElem*nCells), &
              vNodeData(nVars-nDim,vnNodePerElem*nCells) )

    ! check if filename is given
    if ( .not. present(fileName) ) then
        fname = solFileName ! has path included in variable
        tecFileUnit = solFile 
    else
        ! if file name is provided, open new output
        fname = fileName 
        tecFileUnit = fileUnit 
    end if

    ! Open file
    open( unit = tecFileUnit, file = trim(adjustl(fname)), form = 'formatted', &
          iostat = ierr )
    if ( ierr /= 0 ) then
        write(*,*) 'ERROR: opening ', trim(adjustl(fname))
        stop
    end if

    ! Write file header
    write(tecFileUnit, '(3a)') 'TITLE = "', trim(adjustl(title)),'"'
    write(tecFileUnit, '(2a)') 'FILETYPE = ', trim(adjustl(file_type))
    write(tecFileUnit, '(2a)') 'VARIABLES = ', trim(adjustl(variables))

    ! Write mesh connectivity list sharing 
    write(tecFileUnit, '(a,i0,a,i0,2a)') 'ZONE T = "Viz Mesh", DATAPACKING=POINT, NODES=', &
                                vnNodePerElem*nCells, ', ELEMENTS=', &
                                vnElem*nCells, ', ZONETYPE=', trim(adjustl(elem_type))

    ! Create new node lists and connectivity before writing to file
    iNode = 0
    vElem = 0
    select case ( nDim )

      case (2)

        select case ( maxFacesPerCell ) 

          case ( 3 ) ! triangle
            do iElem = 1, nCells
                ! node coordinates
                ij2node = 0
                do j = 0, vnSubElem
                    do i = 0, vnSubElem-j
                        xi(1) = dXi*i
                        xi(2) = dXi*j
                        iNode = iNode+1
                        vNodeCoord(:,iNode) = ref2cart(nDim,iElem,xi(:))

                        ! store mapping from i,j to new node index
                        ij2node(i+1,j+1) = iNode
                    end do
                end do

                ! connectivity for lower triangles
                do j = 1, vnSubElem
                    do i = 1, vnSubElem-(j-1)
                        vElem = vElem + 1
                        vNodeConnect(1,vElem) = ij2node(i,j)
                        vNodeConnect(2,vElem) = ij2node(i+1,j)
                        vNodeConnect(3,vElem) = ij2node(i,j+1)
                    end do
                end do

                ! connectivity for upper triangles
                do j = 1, vnSubElem-1
                    do i = 2, vnSubElem-(j-1) ! -1 left in for readability
                        vElem = vElem + 1
                        vNodeConnect(1,vElem) = ij2node(i,j)
                        vNodeConnect(2,vElem) = ij2node(i,j+1)
                        vNodeConnect(3,vElem) = ij2node(i-1,j+1)
                    end do
                end do
            end do

          case ( 4 ) ! quadrilateral
            do iElem = 1, nCells
                ! node coordinates
                ij2node = 0
                do j = 0, vnSubElem
                    do i = 0, vnSubElem
                        xi(1) = 1.0_FP*(dXi*i)
                        xi(2) = 1.0_FP*(dXi*j)
                        iNode = iNode+1
                        vNodeCoord(:,iNode) = ref2cart(nDim,iElem,xi(:))

                        ! store mapping from i,j to new node index
                        ij2node(i+1,j+1) = iNode
                    end do
                end do

                ! connectivity for quadrilateral sub elements
                do j = 1, vnSubElem
                    do i = 1, vnSubElem
                        vElem = vElem + 1
                        vNodeConnect(1,vElem) = ij2node(i,j)
                        vNodeConnect(2,vElem) = ij2node(i+1,j)
                        vNodeConnect(3,vElem) = ij2node(i+1,j+1)
                        vNodeConnect(4,vElem) = ij2node(i,j+1)
                    end do
                end do
            end do

        end select
    end select

    ! write visualization node coordinates
    vNodeData(:,:) = 0.0_FP
    write(varfmt,'(a,i0,a)') '(',nVars,'e24.15e2)'
    do iNode = 1, vnNodePerElem*nCells
        write(tecFileUnit,varfmt) vNodeCoord(:,iNode), vNodeData(:,iNode) 
    end do

    ! write visualization node connectivity information
    write(varfmt2,'(a)') '('
    do iNode = 1, maxFacesPerCell
        if (iNode == maxFacesPerCell) then
            write(varfmt2,'(a,a)') trim(adjustl(varfmt2)),'i0' 
        else
            write(varfmt2,'(a,a)') trim(adjustl(varfmt2)),'i0,1x,' 
        end if
    end do
    write(varfmt2,'(a,a)') trim(adjustl(varfmt2)),')'
    do iElem = 1, vnElem*nCells
        write(tecFileUnit,varfmt2) vNodeConnect(:,iElem) 
    end do

    deallocate( vNodeConnect, vNodeCoord, vNodeData, ij2node )

end subroutine initTecPlotFileAscii
!-------------------------------------------------------------------------------
! @purpose 
!   Write zone data file in ASCII format  
!   Share the mesh coordinate and connectivity with zone 1 
!
! @author
!   J. Brad Maeng
!
! @history
!   10/20/2017 - Initial creation
!
subroutine outputTecPlotSolutionAscii(iter,t,fileUnit)
    
    implicit none

    ! Interface variables
    integer, intent(in) :: iter           ! zone number

    real(FP), intent(in) :: t   ! solution time

    integer, intent(in), optional :: fileUnit

    ! Local variables
    integer :: ierr,            & ! error code
               iNode,           & ! node index
               iElem,           & ! global cell index
               iEq,             & ! equation index
               i, j,            & ! ref. coord indices
               vnNodePerElem,   & ! viz nodes per cell
               vnElem             ! number of visualization elements

    character(80) :: title,     & ! data set title
                     file_type, & ! file type
                     elem_type, & ! FE element type
                     fname,     & ! file name including the path
                     varfmt,    &      
                     varShareList

    real(FP) :: dXi,                & ! reference coordinate increment
                xi(nDim),           & ! reference coordinate of node
                xCart(nDim),        & ! phycial coordinate
                radialPos,          &
                theta,              &
                qTemp(nEqns),       & ! temporary variable
                qTemp2(nEqns)         

    real(FP) :: sol_time ! solution time

    real(FP), allocatable :: vNodeData(:,:) ! reconstruction data at nodes

    ! check if fileUnit is given
    if ( .not. present(fileUnit) ) then
        tecFileUnit = solFile 
    else
        ! if fileUnit is provided, open new output
        tecFileUnit = fileUnit 
    end if

    sol_time = t

    select case (nDim)
    
      case (2)

        ! Finite element mesh variables
        dXi = 1.0_FP / real(vnSubElem)
        select case ( maxFacesPerCell )
          case ( 3 ) 
            ! triangular
            elem_type = 'FETRIANGLE'
            vnNodePerElem = (vnSubElem+1)*(vnSubElem+2)/2
            vnElem = vnSubElem**2
          case ( 4 ) 
            ! quadrilateral
            elem_type = 'FEQUADRILATERAL'
            vnNodePerElem = (vnSubElem+1)*(vnSubElem+1)
            vnElem = vnSubElem**2
        end select
        ! x and y coordinate share
        varShareList = '[1,2]'
            
    end select

    allocate( vNodeData(nVars-nDim,vnNodePerElem*nCells) )

    ! use latest values
    call updateReconData()

    ! fill in array of viz data
    vNodeData(:,:) = 0.0_FP
    select case (nDim)

      case (2)

       iNode = 0
       select case ( maxFacesPerCell )

         case ( 3 ) ! triangle
           do iElem = 1, nCells

               do j = 0, vnSubElem
                   do i = 0, vnSubElem-j
                       iNode = iNode + 1
                       xi(1) = i*dXi
                       xi(2) = j*dXi
                       ! physical coordinate related
                       xCart(:) = ref2cart(nDim,iElem,xi)
                       radialPos = sqrt(xCart(1)**2.0_FP + xCart(2)**2.0_FP)
                       if ( abs(xCart(2)) <= eps .and. abs(xCart(1)) <= eps ) then
                           theta = 0.0_FP
                       else
                           theta = atan2(xCart(2),xCart(1))
                       end if

                       ! Interpolation solution
                       qTemp(:) = reconState(nDim,nEqns,iElem,xi)
                       vNodeData(1,iNode) = qTemp(1)
                       vNodeData(2,iNode) = qTemp(2)
                       vNodeData(3,iNode) = qTemp(3)

                       !select case ( eqFlag )
                       !  case ( LINADV_EQ )
                       !    qTemp2(:) = evalFunction(ndim,nEqns,xCart,solTime)
                       !  case ( BURGERS_EQ )
                       !    qTemp2(:) = burgSol(ndim,nEqns,qTemp,xCart,solTime)
                       !  case ( PLESSEULER_EQ )
                       !    qTemp2(:) = plessEulerSol(ndim,nEqns,qTemp,xCart,solTime)
                       !end select
                       !vNodeData(4,iNode) = qTemp2(1)
                       !vNodeData(5,iNode) = qTemp2(2)
                       !vNodeData(6,iNode) = qTemp2(3)
                       
                   end do
               end do
           end do

         case ( 4 ) ! quadrilateral
           do iElem = 1, nCells
               do j = 0, vnSubElem
                   do i = 0, vnSubElem
                       iNode = iNode + 1
                       xi(1) = dXi*i
                       xi(2) = dXi*j
                       ! physical coordinate related
                       xCart(:) = ref2cart(nDim,iElem,xi)
                       radialPos = sqrt(xCart(1)**2.0_FP + xCart(2)**2.0_FP)
                       if ( abs(xCart(2)) <= eps .and. abs(xCart(1)) <= eps ) then
                           theta = 0.0_FP
                       else
                           theta = atan2(xCart(2),xCart(1))
                       end if

                       ! Interpolation solution
                       qTemp(:) = reconState(nDim,nEqns,iElem,xi)
                       vNodeData(1,iNode) = qTemp(1)
                       vNodeData(2,iNode) = qTemp(2)
                       vNodeData(3,iNode) = qTemp(3)

                       !select case ( eqFlag )
                       !  case ( LINADV_EQ )
                       !    qTemp2(:) = evalFunction(ndim,nEqns,xCart,solTime)
                       !  case ( BURGERS_EQ )
                       !    qTemp2(:) = burgSol(ndim,nEqns,qTemp,xCart,solTime)
                       !  case ( PLESSEULER_EQ )
                       !    qTemp2(:) = plessEulerSol(ndim,nEqns,qTemp,xCart,solTime)
                       !end select
                       !vNodeData(4,iNode) = qTemp2(1)
                       !vNodeData(5,iNode) = qTemp2(2)
                       !vNodeData(6,iNode) = qTemp2(3)

                   end do
               end do
           end do

       end select
    end select

    ! Write solution zone header 
    write(varfmt,'(i0)') iter
    write(tecFileUnit, '(3a,i0,a,i0,5a,e24.15e2)') 'ZONE T = "', &
            trim(adjustl(varfmt)),'", DATAPACKING=POINT, NODES=', &
            vnNodePerElem*nCells, ', ELEMENTS=', &
            vnElem*nCells, ', ZONETYPE=', trim(adjustl(elem_type)), &
            ', VARSHARELIST=(',trim(adjustl(varShareList)), &
            '=1), CONNECTIVITYSHAREZONE=1, STRANDID=0, SOLUTIONTIME=', &
            sol_time

    ! write visualization node coordinates
    write(varfmt,'(a,i0,a)') '(',nVars-nDim,'e24.15e2)'
    do iNode = 1, vnNodePerElem*nCells
        write(tecFileUnit,varfmt) vNodeData(:,iNode) 
    end do

    deallocate( vNodeData )

end subroutine outputTecPlotSolutionAscii
!-------------------------------------------------------------------------------
! @purpose 
!   Close tecplot data file in ASCII format  
!
! @author
!   J. Brad Maeng
!
! @history
!   10/20/2017 - Initial creation
!
subroutine closeTecPlotFileAscii(fileUnit)

    implicit none

    ! Interface variable
    integer, intent(in), optional :: fileUnit

    ! Local variable
    integer :: ierr               ! error code

    ! check if fileUnit is given
    if ( .not. present(fileUnit) ) then
        tecFileUnit = solFile 
    else
        ! if fileUnit is provided, open new output
        tecFileUnit = fileUnit 
    end if

    ! Close file
    close( unit = tecFileUnit, iostat = ierr )
    if ( ierr /= 0 ) then
        write(*,*) 'ERROR: closing file'
        stop
    end if

end subroutine closeTecPlotFileAscii
!-------------------------------------------------------------------------------
end module output_tecplot
!-------------------------------------------------------------------------------
