!-------------------------------------------------------------------------------
!> @purpose 
!>  Provides input and output functions
!>
!> @author
!>  Timothy A. Eymann
!>
!> @history
!>  10 April 2015 - Added additional visualization routines (Maeng)
!>  20 September 2016 - Integrate all advective systems (Maeng)
!>
module inputOutput

    implicit none

    integer, parameter :: fileLength = 200 !< length of a file string

    character(fileLength) :: filePath,        & !< base path for files
                             drvFileName,     & !< driver file name
                             solFileName,     & !< solution file name
                             reconFileName,   & !< combined mesh/sol file name
                             inputFileName,   & !< combined mesh/sol file name = input
                             meshFileName,    & !< mesh file
                             bcFileName,      & !< boundary condition file
                             histFileName,    & !< history file
                             residFileName,   & !< residual history file name
                             auxFileName,     & !< auxiliary data file 
                             auxFileName2,    & !< auxiliary data file 
                             dumpReconName,   & !< reconstruction dump file name
                             dumpSolName,     & !< solution dump file name
                             dumpPrompt,      & !< data dump file prompt
                             shutDownPrompt     !< simulation shut down prompt

    character(fileLength) :: reconFileName_copy,  & !< reconstruction file name copy
                             solFileName_copy       !< solution file name copy

    integer, parameter :: drvFile = 10,        & !< input file unit
                          solFile = 20,        & !< solution output file unit
                          solFileTmp = 24,     & !< temp. solution output file unit
                          meshFile = 30,       & !< mesh file unit
                          reconFile = 40,      & !< reconstruction file unit
                          inputFile = 1100,    & !< solution recon. input file unit
                          bcFile = 50,         & !< initialization file
                          histFile = 60,       & !< history file unit
                          residFile = 90,      & !< residual history file unit
                          auxFile = 70,        & !< aux file unit
                          auxFile2 = 80,       & !< aux file unit
                          dumpReconFile = 124, & !< reconstruction dump file unit
                          dumpPromptFile = 121,& !< solution dump file prompt unit
                          shutDownPromptFile = 135 !< simulation shut down prompt unit

    logical :: solInput = .false.      !< solution recon. input file flag. Default = false

    integer :: solOutputFreq        !< frequency of solution output

contains
!----------------------------------------------------------------------
!> @purpose 
!>  Gather driver (input) file from command line; make sure it exists
!>
!> @author
!>  Timothy A. Eymann
!>
!> @history
!>  7 July 2010 - Initial Creation
!>
subroutine getDriverFile

    use solverVars
    use meshUtil
    implicit none

    ! Local variables
    logical :: isFile   !< flag specifying file existence

    integer :: nArgs    !< number of command line arguments
    
    character(fileLength)   :: executable   !< name of program

    call getarg(0,executable)
    
    ! Check for proper command line arguments
    nArgs = iargc()
    if ( nArgs .lt. 1 ) then  
        write(*,*)
        write(*,10) 'ERROR: Too few arguments.'
        write(*,*) 
        write(*,10) 'Usage:'
        write(*,20) trim(adjustl(executable)),' driverFile.drv'
        write(*,*)
        stop
    else
        call getarg(1, drvFileName)
       
        ! Make sure file exists
        inquire( file = trim(adjustl(drvFileName)), exist = isFile )
        if ( .not. isFile ) then
            write(*,*)
            write(*,'(3A)') "ERROR: Driver file '",                        &
                              trim(adjustl(drvFileName)), "' does not exist."
            write(*,*)
            stop
        end if
    end if

 10 format ( 1X, A )
 20 format ( 3X, 2A )

end subroutine
!-------------------------------------------------------------------------------
!> @purpose 
!>  Parse input file 
!>
!> @author
!>  Timothy A. Eymann
!>
!> @history
!>  7 July 2010 - Initial creation
!>  7 March 2012 - Adapted from burgSolv version
!>  27 April 2016 - Replaced dtIn, nIter in time parameter with cfl, tf
!>
subroutine parseDriverFile

    use solverVars
    use meshUtil, only: nDim
    use update, only: eqFlag, LINADV_EQ, BURGERS_EQ, PLESSEULER_EQ

    implicit none

    ! Local variables
    character(1)          :: header   !< Buffer for comments/whitespace in file
    character(fileLength) :: tmpFile  !< Place holder for file name 
    integer               :: line,  & !< Counter to skip lines in file
                             iErr     !< error status
    
    open( unit = drvFile, file = trim(adjustl(drvFileName)), &
          form = 'formatted' )
 
    ! File names
    read(drvFile,'(A1)',advance='yes') (header, line = 1,4)
    read(drvFile,'(A100)') tmpFile; call stripInputString(tmpFile,filePath)

    read(drvFile,'(A100)') tmpFile; call stripInputString(tmpFile,solFileName)
    solFileName_copy = solFileName
    solFileName = trim(adjustl(filePath))//'/'//trim(adjustl(solFileName))

    ! set data dump file name
    dumpPrompt = trim(adjustl(filePath))//'/dump'
    ! set simulation shut down prompt name 
    shutDownPrompt = trim(adjustl(filePath))//'/shutdown'
  
    read(drvFile,'(A100)') tmpFile; call stripInputString(tmpFile,reconFileName)
    reconFileName_copy = reconFileName
    ! reconstruction file name
    reconFileName = trim(adjustl(filePath))//'/'//trim(adjustl(reconFileName))

    ! set residual file name 
    auxFileName = trim(adjustl(filePath))//'/aux'//trim(adjustl(reconFileName_copy))
    residFileName = trim(adjustl(filePath))//'/resid'//trim(adjustl(reconFileName_copy))
    
    ! assume mesh and bc file are specified by full path
    read(drvFile,'(A100)') tmpFile; call stripInputString(tmpFile,meshFileName)
    meshFileName = trim(adjustl(meshFileName))
    
    read(drvFile,'(A100)') tmpFile; call stripInputString(tmpFile,bcFileName)
    bcFileName = trim(adjustl(bcFileName))

    histFileName = trim(adjustl(filePath))//'/'//'history.dat'

    ! Solution parameters
    read(drvFile,'(A1)',advance='yes') (header, line = 1,4)
    read(drvFile,*) govEqn
    ! parse mesh once governing equation is known (also allocates variables)
    call readMeshFile
    select case ( trim(adjustl(govEqn)) )
        case ( 'linadv' )
            eqFlag = LINADV_EQ

        case ( 'plesseul' )
            eqFlag = PLESSEULER_EQ

        case ( 'burgers' )
            eqFlag = BURGERS_EQ
        
        case default
            write(*,'(3a)') 'ERROR: governing equation "', &
                            trim(adjustl(govEqn)),'" undefined.'
            stop
    end select
    read(drvFile,*) initSolnType
    
    ! Time-step parameters
    read(drvFile,'(A1)',advance='yes') (header, line = 1,4)
    read(drvFile,*) tFinal
    read(drvFile,*) cfl
 
    ! Output parameters
    read(drvFile,'(A1)',advance='yes') (header, line = 1,4)
    read(drvFile,*) solOutputFreq
    if ( solOutputFreq == 0 ) solOutputFreq = 1

    ! Solution input file parameters -------------------------------------------------
    ! TODO: I guess I can also take mesh information as well?
    ! For continuing simulation from an existing reconstruction file
    ! read specified file. File name MUST include path as well 
    read(drvFile,'(A1)',advance='yes',iostat=iErr) (header, line = 1,4)
    if ( iErr < 0 ) then
        ! end of file 
        GO TO 108
    end if

    read(drvFile,'(A100)',iostat=iErr) tmpFile; call stripInputString(tmpFile,inputFileName)
    if ( iErr > 0 ) then
        ! error reading file 
        write(*,*) 
        write(*,*) 'ERROR: reading input file name'
        write(*,*)
        stop
    else if ( iErr < 0 ) then
        ! end of file 
        GO TO 108
    else
        call readInputFile(inputFile,inputFileName,solInput)
        ! TODO: tFinal in this case is not reflective of tSim /= 0 
    end if

108 close(drvFile)
    ! Solution input file parameters -------------------------------------------------

    !close(drvFile)

end subroutine parseDriverFile
!-------------------------------------------------------------------------------
!> @purpose 
!>  Parse boundary condition file
!>
!> @author
!>  Timothy A. Eymann
!>
!> @history
!>  27 March 2012 - Initial creation
!>
subroutine parseBCfile

    use solverVars, only: inLength, iLeft, iRight, initSolnType
    use meshUtil, only: nFaces, faceCells, cellNodes, cellFaces, &
                        maxNodesPerCell, maxFacesPerCell, maxNodesPerFace, &
                        faceNodes, nPatches
    use boundaryConditions
    use meshUtil, only: edgeCoord
    implicit none

    ! Local variables
    character(1) :: header !< Buffer for headers in file
    character(inLength) :: patchName !< patch name
    character(fileLength) :: idList !< list of patch ids
    
    integer :: line,    & !< counter for line skipping
               iErr,    & !< error code
               i,       & !< loop index
               id0,     & !< starting index for id string
               idF,     & !< ending index for id string
               iFace,   & !< face index
               iNode,   & !< node index
               bcID       !< boundary condition id

    logical :: isFile !< file exist flag

    ! Make sure file exists
    inquire( file = trim(adjustl(bcFileName)), exist = isFile )
    if ( .not. isFile ) then
        write(*,*)
        write(*,'(3A)') "ERROR: Boundary file '",                        &
                          trim(adjustl(bcFileName)), "' does not exist."
        write(*,*)
        stop
    end if
    
    open(unit=bcFile,file=trim(adjustl(bcFileName)),form='formatted')
    read(bcFile,'(a)',advance='yes') (header, line = 1,4)

    iErr = 0
    i = 0
    pNodePair(:,:) = 0
    pEdgePair = 0
    do while ( ( iErr == 0 ) )
        !read(bcFile,'(<fileLength>a)',iostat=iErr) idList(:)
        read(bcFile,'(*(a))',iostat=iErr) idList(:)
        read(bcFile,*,iostat=iErr) header
        read(bcFile,*,iostat=iErr) patchName

        !write(*,*) patchName, bcID
        ! Tasks done once end of the file is detected
        if ( iErr /= 0 ) then
            
            ! mark edges and nodes connected to boundary with bc type
            do iFace = 1, nFaces
                if ( ( faceCells(iRight,iFace) < 0 ) .and. &
                     ( edgeBC(iFace) /= periodicID ) ) then
                    ! store patch id
                    edgePatch(iFace) = abs(faceCells(iRight,iFace))

                    do i = 1, nPatches
                        if ( patchID(i) == abs(faceCells(iRight,iFace)) ) then
                            bcID = patchType(i)
                        end if
                    end do
                    edgeBC(iFace) = bcID
                    do iNode = 1, maxNodesPerFace
                        nodeBC(faceNodes(iNode,iFace)) = bcID
                    end do
                end if
            end do
            
            call testPeriodicAssignment
            return
        end if
 
        ! parse patch id list (full line)
        id0 = 1
        idF = 1
        do while ( ( idF < fileLength ) .and. ( iErr == 0 ) )
           
            idF = index(idList,' ')

            if ( idF > 1 ) then
                i = i + 1
                read(idList(1:idF),*,iostat=iErr) patchID(i)

                select case ( trim(adjustl(patchName)) )
                    case ('periodic') 
                        patchType(i) = periodicID

                    case ('outflow')
                        patchType(i) = outflowID
                    
                    case ('wall')
                        patchType(i) = wallID

                    case ('specFunc')
                        patchType(i) = specFuncID

                    case ('internal')
                        patchType(i) = internalID

                    case ('initSoln')
                        patchType(i) = initSolnID

                    case default
                        write(*,'(3a)') 'ERROR: ',trim(adjustl(patchName)),&
                                        ' unknown patch type.'
                        stop
                end select
            else
                exit
            end if

            ! remove last patch number from list
            id0 = idF+1
            idList = idList(id0:fileLength-id0+1)

        end do

        ! find "neighbor" across domain for periodic pair 
        if ( patchType(i) == periodicID  ) then
            ! assumes only two patches on the line
            call setPeriodicNeighbor(patchID(i-1:i))
        elseif ( patchType(i) == specFuncID ) then
            read(bcFile,*) bcFunction
        elseif ( patchType(i) == initSolnID ) then
            ! set bcFunction to initial solution type, convenient
            bcFunction = initSolnType
        end if

        read(bcFile,*,iostat=iErr) header
   end do

end subroutine parseBCfile
!-------------------------------------------------------------------------------
!> @purpose 
!>  Strip off commented text from string input
!>
!> @author
!>  Timothy A. Eymann (adapted from routine by David McDaniel)
!>
!> @history
!>  21 May 2010 - Initial creation
!>
subroutine stripInputString(strin,strout)

    implicit none

    ! Local variables
    character(*)  :: strin, strout
    integer       :: i

    i = index(strin,'#')
    if ( i .gt. 0 ) then
        strout = trim(strin(1:i-1))
    else
        strout = trim(strin)
    end if

end subroutine
!----------------------------------------------------------------------
!> @purpose 
!>  Check if data dump is required. Checks for data dump file prompt 
!>   in the driver file directory called 'dump' without the apostrophes.
!>   Outputs binary file containing solution at the particular iteration
!>
!> @author
!>  J. Brad Maeng
!>
!> @history
!>  28 March 2016 - Initial Creation
!>
subroutine shutDownInquiry(switch)

    !use solverVars
    !use meshUtil

    implicit none

    ! Interface variables
    logical, intent(out) :: switch

    ! Local variables
    logical :: isFile   !< flag specifying file existence

    integer :: iNode,   & !< node index
               iCell,   & !< cell index
               iEdge,   & !< edge index
               iErr       !< error code

    ! shut down switch
    switch = .false.

    ! Make sure file exists
    inquire( file = trim(adjustl(shutDownPrompt)), exist = isFile )
    if ( isFile ) then
        ! open-close and delete dump file
        open( unit = shutDownPromptFile, file = trim(adjustl(shutDownPrompt)), &
              status = 'OLD' )
        close ( unit = shutDownPromptFile, status = 'delete' )

        ! activate shut down switch
        switch = .true.
        write(*,*)
        write(*,'(1A)') 'Simulation shut down requested.'
        write(*,'(3A)') 'Driver file is "', trim(adjustl(drvFileName)), '" '
        write(*,*)
        
    end if

end subroutine shutDownInquiry
!----------------------------------------------------------------------
!> @purpose 
!>  Check if data dump is required. Checks for data dump file prompt 
!>   in the driver file directory called 'dump' without the apostrophes.
!>   Outputs binary file containing solution at the particular iteration
!>
!> @author
!>  J. Brad Maeng
!>
!> @history
!>  28 March 2016 - Initial Creation
!>  12 August 2016 - Implemented switch 
!>
subroutine dumpDataInquiry(iter,t,switch)

    use solverVars
    use meshUtil
    use update

    implicit none

    !include 'tecio.f90'

    ! Interface variables
    integer, intent(in) :: iter

    real(FP), intent(in) :: t 

    logical, intent(out) :: switch

    ! Local variables
    logical :: isFile   !< flag specifying file existence

    character(fileLength) :: varfmt,    & !< 
                             temp

    integer :: iNode,   & !< node index
               iCell,   & !< cell index
               iEdge,   & !< edge index
               iErr       !< error code

    ! dump data switch
    switch = .false.

    ! Make sure file exists
    inquire( file = trim(adjustl(dumpPrompt)), exist = isFile )
    if ( isFile ) then
        ! open-close and delete dump file
        open( unit = dumpPromptFile, file = trim(adjustl(dumpPrompt)), &
              status = 'OLD' )
        close ( unit = dumpPromptFile, status = 'delete' )

        ! activate dump data switch
        switch = .true.

        ! change dump file name according to iteration number
        reconFileName_copy = adjustr(reconFileName_copy)
        temp = reconFileName_copy(:len(reconFileName_copy)-4) ! trim '.dat' from the end of file name 
        write(varfmt,'(2a,i0,a)') trim(adjustl(temp)),'.dump', iter, '.dat'
        write(*,*)
        write(*,'(1A)') "Solution dump file found."
        write(*,'(2A)') "Driver file is ", trim(adjustl(drvFileName))
        write(*,*)
        write(*,'(3A)') "Solution dump file '", trim(adjustl(varfmt)), "' is created."
        write(*,*)

        dumpReconName = trim(adjustl(filePath))//'/'//trim(adjustl(varfmt))

        ! open dump file and write binary output data
        open( unit = dumpReconFile, file = trim(adjustl(dumpReconName)) , &
              form = 'unformatted', access = 'stream', status = 'REPLACE', &
              iostat = iErr )
        if ( iErr /= 0 ) then
            write(*,*) 'ERROR: opening ', trim(adjustl(dumpReconName))
            stop
        end if

        ! store binary data in the new dump file
        write(dumpReconFile) nDim, nEqns, nIter
        write(dumpReconFile) maxNodesPerCell, maxFacesPerCell, nNodes, nEdges, nCells   

        ! mesh information
        do iNode = 1,nNodes
            write(dumpReconFile) nodeCoord(:,iNode)
        end do

        do iEdge = 1,nEdges
            write(dumpReconFile) edgeCoord(:,iEdge)
        end do

        ! cell nodes and faces
        do iCell = 1, nCells
            write(dumpReconFile) cellNodes(:,iCell), cellFaces(:,iCell)
        end do

        ! indicate iteration and time for data
        write(dumpReconFile) iter, t 

        ! conserved cell averages
        do iCell = 1, nCells
            write(dumpReconFile) cellAvgN(:,iCell)
        end do

        ! primitive cell averages
        do iCell = 1, nCells
            write(dumpReconFile) primAvgN(:,iCell)
        end do

        ! node values
        do iNode = 1, nNodes
            write(dumpReconFile) nodeDataN(:,iNode)
        end do

        ! edge values
        do iEdge = 1, nEdges
            write(dumpReconFile) edgeDataN(:,iEdge)
        end do

        close ( dumpReconFile )

        ! result visualization at iteration
        solFileName_copy = adjustr(solFileName_copy)
        temp = solFileName_copy(:len(solFileName_copy)-4) ! trim '.plt' from the end of file name 
        write(varfmt,'(2a,i0,a)') trim(adjustl(temp)),'.dump', iter, '.plt'
        write(*,'(3A)') 'TecPlot file "', trim(adjustl(varfmt)), '" is created.'
        write(*,*)

        ! create a new TecPlot file name 
        dumpSolName = trim(adjustl(filePath))//'/'//trim(adjustl(varfmt))
        
    end if

end subroutine dumpDataInquiry
!-------------------------------------------------------------------------------
!> @purpose 
!>  Read solution input file
!>
!> @author
!>  J. Brad Maeng
!>
!> @history
!>  11 April 2016 - Initial creation
!>
subroutine readInputFile(fileUnit,fileName,updated)
    
    use solverVars, only: tSim

    implicit none

    ! Interface variables
    integer, intent(in) :: fileUnit !< file unit number

    character(fileLength), intent(in) :: fileName !< file name

    logical, intent(out) :: updated

    ! Local variables
    integer :: maxIter,     & !< maximum iteration number
               iter,        & !< iteration counter
               iErr           !< error 

    ! open solution input file 
    open( unit = fileUnit, file = trim(adjustl(fileName)), form = 'unformatted', &
        access = 'stream', status = 'OLD', iostat = iErr , action = 'read' )
    if ( iErr /= 0 ) then
        write(*,*) 'ERROR: opening solution reconstruction input file'
        write(*,*) 'Instead, solution will be initialized from its initial condition.'
        updated = .false.
        return
    end if
    write(*,*)
    write(*,*) 'WARNING: Solution initialized from "', trim(adjustl(fileName)), '".'

    ! signal that solution is initialized from input file
    updated = .true.

    ! read the most recent reconstruction data file
    maxIter = getIterNum(fileUnit)
    rewind(fileUnit)
    call skipConnectivity(fileUnit)
    do iter = 1, maxIter-1  
        iErr = skipData(fileUnit)
    end do
    call readData(fileUnit,iter,tSim)
    write(*,'(a,e12.4e2)') '      Starting time: ', tSim

    ! TODO: put a dimension check so if a wrong input file is called, quit program.
    ! check nCells, nNodes, nEdges

    ! close the file
    close(fileUnit)

end subroutine readInputFile
!-------------------------------------------------------------------------------
!> @purpose 
!>  Read a AVUS/Cobalt formatted (*.grd) mesh from a file.  Allocates data
!>  structures associated with mesh variables
!>
!> @author
!>  Timothy A. Eymann
!>
!> @history
!>  7 March 2012 - Initial creation
!>
subroutine readMeshFile

    use meshUtil
    use boundaryConditions
    use solverVars
    use update
    implicit none

    ! Local variables
    integer :: iFace,       & !< face index
               iNode,       & !< node index
               iDir,        & !< coordinate direction
               iErr,        & !< error code
               nodesPerFace   !< number of nodes on face
    real(FP) :: nodeTempX,     &
                nodeTempY

    ! guess that file is formatted
    open( unit = meshFile, file=trim(adjustl(meshFileName)), form='formatted')
    read(meshFile,*,iostat=iErr) nDim, nZones, nPatches

    if ( iErr /= 0 ) then
        write(*,*) 'ERROR: mesh file must be formatted (ascii)'
        stop
    end if

    read(meshFile,*) nNodes, nFaces, nCells, maxNodesPerFace, maxFacesPerCell

    ! handle special case of 3d quad element
    if ( ( maxFacesPerCell > 4 ) .and. ( nDim > 2 ) ) then
        maxNodesPerCell = 8
    else
        ! 1 and 2D
        maxNodesPerCell = maxFacesPerCell
    end if

    ! calculate number of edges in mesh
    if ( nDim <= 2 ) then
        nEdges = nFaces
    end if

    ! initialize arrays
    call allocateMeshVars()

    ! store node coordinates
    !write(*,*) 'WARNING: mesh domain has been modified for fast vortex'
    do iNode = 1,nNodes
        read(meshFile,*) nodeCoord(:,iNode)

        !! flip x signs
        !! is was done to mitigate the mesh alignment issue for diagonal mesh
        !nodeCoord(1,iNode) = -nodeCoord(1,iNode)
        !nodeCoord(2,iNode) = nodeCoord(2,iNode)
        !! flip x signs

        do iDir = 1, nDim
            xMax(iDir) = max( xMax(iDir), nodeCoord(iDir,iNode) )
            xMin(iDir) = min( xMin(iDir), nodeCoord(iDir,iNode) )
        end do
    end do

    ! store face connectivity information
    do iFace = 1,nFaces
        read(meshFile,*) nodesPerFace, faceNodes(1:nodesPerFace,iFace), &
                         faceCells(:,iFace)
    end do

    close(meshFile)

end subroutine readMeshFile
!-------------------------------------------------------------------------------
!> @purpose 
!>  Return number of iterations in afSolver binary file
!>
!> @author
!>  Timothy A. Eymann
!>
!> @history
!>  26 July 2012 - Initial Creation
!>  11 April 2016 - Relocated to inputOutput.f90 from postProc.f90 (Maeng)
!>
function getIterNum(fileUnit)

    implicit none

    ! Interface variables
    integer, intent(in) :: fileUnit !< file unit for binary data

    ! Function variable
    integer :: getIterNum

    ! Local variables
    integer :: iErr !< error code

    iErr = 0
    getIterNum = 0
    
    rewind(fileUnit)

    call skipConnectivity(fileUnit)
    do while ( iErr == 0 )
        iErr = skipData(fileUnit)
        if ( iErr == 0 ) getIterNum = getIterNum + 1
    end do

end function getIterNum
!-------------------------------------------------------------------------------
!> @purpose 
!>  Skip connectivity data in binary file
!>
!> @author
!>  Timothy A. Eymann
!>
!> @history
!>  26 July 2012 - Initial Creation
!>  11 April 2016 - Relocated to inputOutput.f90 from postProc.f90 (Maeng)
!>
subroutine skipConnectivity(fileUnit)

    use meshUtil, only: nDim, nNodes, nEdges, nCells, maxNodesPerCell
    implicit none

    ! Interface variables
    integer, intent(in) :: fileUnit !< file unit for binary data

    ! Local variables
    integer :: i,       & !< counter
               iNode,   & !< node index
               iEdge,   & !< edge index
               iCell,   & !< cell index
               dumInt     !< dummy integer variable

    real :: dumReal !< dummy real variable

    read(fileUnit) (dumInt, i=1,8) 
    do iNode = 1, nNodes
        read(fileUnit) (dumReal, i=1,nDim)
    end do
    do iEdge = 1, nEdges
        read(fileUnit) (dumReal, i=1,nDim)
    end do
    do iCell = 1, nCells
        read(fileUnit) (dumInt, i=1,2*maxNodesPerCell)
    end do

end subroutine skipConnectivity
!-------------------------------------------------------------------------------
!> @purpose 
!>  Skip iteration data from an output file produced by afSolver
!>
!> @author
!>  Timothy A. Eymann
!>
!> @history
!>  19 April 2012 - Initial creation
!>  11 April 2016 - Relocated to inputOutput.f90 from postProc.f90 (Maeng)
!>
function skipData(fileUnit)

    use solverVars, only: nEqns
    use meshUtil, only: nCells, nEdges, nNodes
    implicit none

    ! Interface variables
    integer, intent(in) :: fileUnit !< file unit number

    ! Function variable
    integer :: skipData

    ! Local variables
    integer :: dumInt,  & !< dummy integer variable
               iCell,   & !< cell index
               iNode,   & !< node index
               iEdge,   & !< edge index
               iEq        !< equation index

    real :: dumReal !< dummy real variable

    read(fileUnit, iostat=skipData) dumInt, dumReal
    
    do iCell = 1, nCells
        read(fileUnit, iostat=skipData) (dumReal, iEq=1,nEqns) 
    end do

    do iCell = 1, nCells
        read(fileUnit, iostat=skipData) (dumReal, iEq=1,nEqns) 
    end do
    
    do iNode = 1, nNodes
        read(fileUnit, iostat=skipData) (dumReal, iEq=1,nEqns)
    end do
    
    do iEdge = 1, nEdges
        read(fileUnit, iostat=skipData) (dumReal, iEq=1,nEqns)
    end do

end function skipData
!-------------------------------------------------------------------------------
!> @purpose 
!>  Read data from an output file produced by afSolver
!>
!> @author
!>  Timothy A. Eymann
!>
!> @history
!>  19 April 2012 - Initial creation
!>  11 April 2016 - Relocated to inputOutput.f90 from postProc.f90 (Maeng)
!>
subroutine readData(fileUnit,solIter,solTime)

    use solverVars, only: FP, nEqns
    use update, only: primAvgN, cellAvgN, & 
                      nodeDataN, edgeDataN
    use meshUtil, only: nCells, nNodes, nEdges
    implicit none

    ! Interface variables
    integer, intent(in) :: fileUnit !< file unit number

    integer, intent(out), optional :: solIter !< solution iteration

    real(FP), intent(out), optional :: solTime 

    ! Local variables
    integer :: dumInt,  & !< dummy integer variable
               iCell,   & !< cell index
               iNode,   & !< node index
               iEdge,   & !< edge index
               iEq        !< equation index

    real :: dumReal !< dummy real variable

    if ( (present(solIter)) .and. (present(solTime)) ) then
        read(fileUnit) solIter, solTime
    else
        read(fileUnit) dumInt, dumReal
    end if

    do iCell = 1, nCells
        read(fileUnit) (cellAvgN(iEq,iCell), iEq=1,nEqns) 
    end do
    
    do iCell = 1, nCells
        read(fileUnit) (primAvgN(iEq,iCell), iEq=1,nEqns) 
    end do
    
    do iNode = 1, nNodes
        read(fileUnit) (nodeDataN(iEq,iNode), iEq=1,nEqns)
    end do
    
    do iEdge = 1, nEdges
        read(fileUnit) (edgeDataN(iEq,iEdge), iEq=1,nEqns)
    end do

end subroutine readData
!-------------------------------------------------------------------------------
!> @purpose 
!>  Allocate variables dimensioned with mesh parameters
!>
!> @author
!>  Timothy A. Eymann
!>
!> @history
!>  3 April 2012 - Initial creation
!>
subroutine allocateMeshVars

    use solverVars
    use meshUtil
    use boundaryConditions
    use update
    use reconstruction

    implicit none

    allocate( cellCentroid(nDim,nCells), &
              cellVolume(nCells), &
              cellDetJac(nCells), &
              cellInvJac(nDim,nDim,nCells), &
              cellJac(nDim,nDim,nCells), &
              cellAngles(maxNodesPerCell,nCells), &
              cellAvg(nEqns,nCells), &
              cellAvgN(nEqns,nCells), & 
              primAvg(nEqns,nCells), &
              primAvgN(nEqns,nCells), & 
              deltaAvg(nEqns,nCells), & 
              cellFaces(maxFacesPerCell,nCells), &
              cellNodes(maxNodesPerCell,nCells), & 
              faceNodes(maxNodesPerFace,nFaces), &
              faceArea(nFaces), &
              faceCells(2,nFaces), &
              faceNormal(nDim,nFaces), &
              edgeCoord(nDim,nEdges), &
              edgeData(nEqns,nEdges), &
              edgeDataH(nEqns,nEdges), &
              edgeDataN(nEqns,nEdges), &
              edgeCell(1,nEdges), &
              nodeCoord(nDim,nNodes), &
              nodeData(nEqns,nNodes), &
              nodeDataH(nEqns,nNodes), &
              nodeDataN(nEqns,nNodes),  &
              nodeCell(1,nNodes), &
              patchID(nPatches), &
              patchType(nPatches), &
              nodeBC(nNodes), &
              edgeBC(nEdges), &
              xMax(nDim), xMin(nDim), &
              reconData(nEqns,(nDim+2)*(nDim+3)/(2*(3-nDim)),nCells), &
              pNodePair(nDim,nNodes), pEdgePair(nEdges), &
              edgePatch(nEdges), &
              xiNode(nDim,maxNodesPerCell), xiEdge(nDim,maxFacesPerCell), &
              residMax(nEqns), residNorm(nEqns), primNorm(nEqns), conservNorm(nEqns) )

    cellCentroid(:,:) = 0.0_FP
    cellVolume(:) = 0.0_FP
    cellDetJac(:) = 0.0_FP
    cellInvJac(:,:,:) = 0.0_FP
    cellJac(:,:,:) = 0.0_FP
    cellAvg(:,:) = 0.0_FP
    cellAvgN(:,:) = cellAvg(:,:)
    primAvg(:,:) = 0.0_FP
    primAvgN(:,:) = primAvg(:,:)
    deltaAvg(:,:) = 0.0_FP
    cellFaces(:,:) = 0
    cellNodes(:,:) = 0
    cellAngles(:,:) = 0.0_FP
    faceNodes(:,:) = 0
    faceArea(:) = 0.0_FP
    faceCells(:,:) = 0
    faceNormal(:,:) = 0.0_FP
    nodeCoord(:,:) = 0.0_FP
    edgeData(:,:) = 0.0_FP
    edgeDataH(:,:) = 0.0_FP
    edgeDataN(:,:) = 0.0_FP
    edgeCoord(:,:) = 0.0_FP
    edgeCell(:,:) = 0
    nodeData(:,:) = 0.0_FP
    nodeDataH(:,:) = 0.0_FP
    nodeDataN(:,:) = 0.0_FP
    nodeCell(:,:) = 0
    patchID(:) = 0
    patchType(:) = internalID
    nodeBC(:) = 0
    edgeBC(:) = 0
    xMax(:) = -1.0e6_FP
    xMin(:) = 1.0e6_FP
    reconData(:,:,:) = 0.0_FP
    pNodePair(:,:) = 0
    pEdgePair(:) = 0
    edgePatch(:) = 0
    xiNode(:,:) = 0.0_FP
    xiEdge(:,:) = 0.0_FP
    residMax(:) = 0.0_FP
    residNorm(:) = 0.0_FP
    primNorm(:) = 0.0_FP
    conservNorm(:) = 0.0_FP

end subroutine allocateMeshVars
!-------------------------------------------------------------------------------
!> @purpose 
!>  Deallocate variables dimensioned with mesh parameters
!>
!> @author
!>  Timothy A. Eymann
!>
!> @history
!>  7 March 2012 - Initial creation
!>
subroutine deallocateMeshVars

    use solverVars
    use meshUtil
    use boundaryConditions
    use update
    use reconstruction

    implicit none

    deallocate( cellCentroid, cellVolume, cellDetJac, cellInvJac, cellJac, &
                cellAvg, cellAvgN, primAvg, primAvgN, deltaAvg, &
                edgeData, edgeDataN, edgeDataH, nodeData, nodeDataN, nodeDataH, &
                cellFaces, cellNodes, cellAngles, &
                faceNodes, faceArea, faceCells, faceNormal, &
                edgeCoord, edgeCell, &
                nodeCoord, nodeCell, &
                patchID, patchType, nodeBC, edgeBC, &
                xMax, xMin, reconData, &
                pNodePair, pEdgePair, edgePatch, xiNode, xiEdge )

end subroutine deallocateMeshVars
!-------------------------------------------------------------------------------
!> @purpose 
!>  Output binary file containing vertex, edge, and centroid data which can be
!>  used with external visualization routines
!>
!> @author
!>  Timothy A. Eymann
!>
!> @history
!>  12 March 2012 - Initial creation
!>  10 April 2015 - Added primitive variable cell average (Maeng)
!>
subroutine outputBinary(iter, t)

    use solverVars, only: FP, nEqns
    use meshUtil, only: nDim, nCells, nNodes, nEdges
    use update, only: cellAvgN, primAvgN, &
                      nodeDataN, edgeDataN
    implicit none
    
    ! Interface variables
    integer, intent(in) :: iter !< iteration number

    real(FP), intent(in) :: t !< current time

    ! Local variables
    integer :: iNode,   & !< node index
               iCell,   & !< cell index
               iEdge      !< edge index

    ! indicate iteration and time for data
    write(reconFile) iter, t 

    ! conserved cell averages
    do iCell = 1, nCells
        write(reconFile) cellAvgN(:,iCell)
    end do

    ! primitive cell averages
    do iCell = 1, nCells
        write(reconFile) primAvgN(:,iCell)
    end do

    ! node values
    do iNode = 1, nNodes
        write(reconFile) nodeDataN(:,iNode)
    end do

    ! edge values
    do iEdge = 1, nEdges
        write(reconFile) edgeDataN(:,iEdge)
    end do

end subroutine outputBinary
!-------------------------------------------------------------------------------
!> @purpose 
!>  Open files for writing 
!>
!> @author
!>  Timothy A. Eymann
!>
!> @history
!>  12 March 2012 - Initial creation
!>
subroutine openFiles

    use meshUtil
    use reconstruction
    use solverVars, only: nEqns, nIter

    implicit none

    ! Local variables
    integer :: i,           & !< counter
               iNode,       & !< node index
               iEdge,       & !< edge index
               iCell,       & !< cell index
               iErr,        & !< error code
               dateVal(8)     !< array of date and time values


    ! open reconstruction file and write header information
    open( unit = reconFile, file = trim(adjustl(reconFileName)), &
          form = 'unformatted', access = 'stream', status = 'REPLACE', &
          iostat = iErr)
    if ( iErr /= 0 ) then
        write(*,*) 'ERROR: opening ', trim(adjustl(reconFileName))
        stop
    end if

    write(reconFile) nDim, nEqns, nIter
    write(reconFile) maxNodesPerCell, maxFacesPerCell, nNodes, nEdges, nCells   

    ! mesh information
    do iNode = 1,nNodes
        write(reconFile) nodeCoord(:,iNode)
    end do

    do iEdge = 1,nEdges
        write(reconFile) edgeCoord(:,iEdge)
    end do

    ! cell nodes and faces
    do iCell = 1, nCells
        write(reconFile) cellNodes(:,iCell), cellFaces(:,iCell)
    end do

    ! set up history file and metadata
    open( unit = histFile, file = trim(adjustl(histFileName)), &
          form = 'formatted', position = 'append', iostat = iErr)
    if ( iErr /= 0 ) then
        write(*,*) 'ERROR: opening ', trim(adjustl(histFileName))
        stop
    end if
    call date_and_time( VALUES = dateVal(:) )
    write(histFile,'(a)',advance='no') '#'
    write(histFile,'(78a)',advance='no') ('-',i=1,78)
    write(histFile,'(a)') '#'
    write(histFile, '(a)') '#'
    write(histFile, '(a,i0,a,i0,a,i0,1x,i2.0,a,i0.2)' ) &
        '#     Job started: ',dateVal(2),'/',dateVal(3),'/',dateVal(1),&
         dateVal(5),':',dateVal(6)
    write(histFile, '(2a)') '#            Mesh: ',trim(adjustl(meshFileName))
    write(histFile, '(2a)') '#         BC File: ',trim(adjustl(bcFileName))
    write(histFile, '(a)') '#'

    ! open residual file
    !open ( unit = residFile, file=trim(adjustl(residFileName)), form='formatted' )

    !! open aux file
    !open ( unit = auxFile, file=trim(adjustl(auxFileName)), form='formatted' )

end subroutine openFiles
!-------------------------------------------------------------------------------
!> @purpose 
!>  Close open files
!>
!> @author
!>  Timothy A. Eymann
!>
!> @history
!>  12 March 2012 - Initial creation
!>
subroutine closeFiles

    implicit none

    ! Local variables
    integer :: iErr !< Error code

    close( reconFile )
    close( histFile )

    !close( residFile )
    !close( auxFile )

end subroutine closeFiles
!-------------------------------------------------------------------------------
!> @purpose 
!>  Output file containing auxiliary data. Contains residual norm over all
!>  cells
!>
!> @author
!>  J. Brad Maeng
!>
!> @history
!>  4 December 2013 - Initial creation
!>
subroutine outputResidual(iter,t)

    use solverVars, only: FP, nEqns
    use meshUtil, only: nCells, faceNormal, faceArea, cellVolume,   &
                        cellNodes, cellFaces
    use update, only: residMax, residNorm, primNorm, conservNorm, &
                      nodeDataN, edgeDataN
    implicit none

    ! Interface variables
    integer, intent(in) :: iter !< iteration number

    real(FP), intent(in) :: t !< simulation time

    ! Local variable
    character(fileLength) :: varfmt

    !! indicate iteration and time for data
    !write(varfmt,'(a,i0,a,i0,a)') '(i0,e12.4e2,',2*nEqns,'(e24.16e2))'
    !write(residFile,varfmt) iter, t, residNorm(:), residMax(:)
    
end subroutine outputResidual
!-------------------------------------------------------------------------------
!> @purpose 
!>  Output file containing auxiliary data. Contains residual norm over all
!>  cells
!>
!> @author
!>  J. Brad Maeng
!>
!> @history
!>  4 December 2013 - Initial creation
!>
subroutine outputAux(iter,t)

    use solverVars, only: FP, nEqns
    use meshUtil
    use mathUtil
    use reconstruction
    use analyticFunctions
    use physics
    use update

    implicit none

    ! Interface variables
    integer, intent(in) :: iter !< iteration number

    real(FP), intent(in) :: t !< simulation time

    ! Local variable
    integer :: iEdge
    
    character(fileLength) :: varfmt

    ! temporary
    integer :: iPoint,  & 
               lCell,   &
               iCell,   &
               iNode,   &
               gNode,   &
               gEdge,   &
               nodeL,   &  
               nodeR

    real(FP) :: qErr(nEqns),            &
                errSumX(nEqns),         &
                errSumY(nEqns),         &
                errSum(nDim,nEqns),     &
                area,                   & 
                dL(nDim),               & 
                xCart(nDim)               !< physical coordinate

    write(varfmt,'(a,i0,a,i0,a)') '(i0, ', 3 ,'e24.15e2)'

!    ! residual*volume error convergence
!    errSum(:,:) = 0.0_FP
!    errSumX(:) = 0.0_FP
!    errSumY(:) = 0.0_FP
!    area = 0.0_FP
!    do iCell = 1,nCells
!        errSumX(:) = errSumX(:) + &
!            cellVolume(iCell)*abs(residExactVol(:,iCell)-residNumVol(:,iCell))**1.0_FP
!        errSumY(:) = errSumY(:) + &
!            cellVolume(iCell)*(abs(residExactVol(:,iCell)-residNumVol(:,iCell)))**2.0_FP 
!        area = area + cellVolume(iCell) 
!    end do
!    errSum(1,:) = (errSumX/area)**(1.0_FP/1.0_FP)
!    errSum(2,:) = (errSumY/area)**(1.0_FP/2.0_FP)
!    open( unit = 1024, file = trim(adjustl(filePath))//'/residVolError.dat', form = 'formatted', &
!          access = 'append' )
!    write(varfmt,'(a,i0,a,i0,a)') '(i0,e24.16e2,',2*nEqns,'(e24.16e2))'
!    write(1024,varfmt) nCells, sqrt(totalVolume/real(nCells+nNodes+nEdges)), &
!                        errSum(1,:), errSum(2,:)
!    close( unit = 1024 )
!
!    ! residual error convergence
!    errSum(:,:) = 0.0_FP
!    errSumX(:) = 0.0_FP
!    errSumY(:) = 0.0_FP
!    area = 0.0_FP
!    do iCell = 1,nCells
!        errSumX(:) = errSumX(:) + &
!            cellVolume(iCell)*abs(residExact(:,iCell)-residNum(:,iCell))**1.0_FP
!        errSumY(:) = errSumY(:) + &
!            cellVolume(iCell)*(abs(residExact(:,iCell)-residNum(:,iCell)))**2.0_FP 
!        area = area + cellVolume(iCell) 
!    end do
!    errSum(1,:) = (errSumX/area)**(1.0_FP/1.0_FP)
!    errSum(2,:) = (errSumY/area)**(1.0_FP/2.0_FP)
!    open( unit = 1021, file = trim(adjustl(filePath))//'/residError.dat', form = 'formatted', &
!          access = 'append' )
!    write(varfmt,'(a,i0,a,i0,a)') '(i0,e24.16e2,',2*nEqns,'(e24.16e2))'
!    write(1021,varfmt) nCells, sqrt(totalVolume/real(nCells+nNodes+nEdges)), &
!                        errSum(1,:), errSum(2,:)
!    close( unit = 1021 )
!
!    ! average fluxes 
!    errSum(:,:) = 0.0_FP
!    errSumX(:) = 0.0_FP
!    errSumY(:) = 0.0_FP
!    area = 0.0_FP
!    do iEdge = 1,nEdges
!        lCell = faceCells(1,iEdge)
!        nodeL = faceNodes(2,iEdge)
!        nodeR = faceNodes(1,iEdge)
!        dL(:) = (nodeCoord(:,nodeR)-nodeCoord(:,nodeL))
! 
!        ! evaluate error in flux
!        !errSumX(:) = errSumX(:) + abs(fluxAvgExact(1,:,iEdge)-fluxAvg(1,:,iEdge))**1.0_FP 
!        !errSumY(:) = errSumY(:) + abs(fluxAvgExact(2,:,iEdge)-fluxAvg(2,:,iEdge))**1.0_FP
!        errSumX(:) = max(errSumX(:),abs(fluxAvgExact(1,:,iEdge)-fluxAvg(1,:,iEdge))) 
!        errSumY(:) = max(errSumY(:),abs(fluxAvgExact(2,:,iEdge)-fluxAvg(2,:,iEdge)))
!        area = area + 1.0_FP
!    end do 
!    !errSum(1,:) = (errSumX/area)**(1.0/1.0)
!    !errSum(2,:) = (errSumY/area)**(1.0/1.0)
!    errSum(1,:) = (errSumX)
!    errSum(2,:) = (errSumY)
!    
!    open( unit = 1011, file = trim(adjustl(filePath))//'/fluxError_avg.dat', form = 'formatted', &
!          access = 'append' )
!    write(varfmt,'(a,i0,a,i0,a)') '(i0,e24.16e2,',2*nEqns,'(e24.16e2))'
!    write(1011,varfmt) nCells, sqrt(totalVolume/real(nCells+nNodes+nEdges)), &
!                        errSum(1,:), errSum(2,:)
!
!    close( unit = 1011 ) 
!
!    ! exact point flux error evaluation    
!    ! TODO: maximum norm and the appropriate spacing/independent value for judging the error
!    ! a simple area isn't the way...
!    errSum(:,:) = 0.0_FP
!    errSumX(:) = 0.0_FP
!    errSumY(:) = 0.0_FP
!    area = 0.0_FP
!    do iEdge = 1,nEdges
!        !errSumX(:) = errSumX(:) + & 
!        !    abs(edgePtFluxExact(1,:,iEdge)-edgePtFlux(1,:,iEdge)) 
!        !errSumY(:) = errSumY(:) + & 
!        !    abs(edgePtFluxExact(2,:,iEdge)-edgePtFlux(2,:,iEdge))
!        errSumX(:) = max(errSumX(:),abs(edgePtFluxExact(1,:,iEdge)-edgePtFlux(1,:,iEdge))) 
!        errSumY(:) = max(errSumY(:),abs(edgePtFluxExact(2,:,iEdge)-edgePtFlux(2,:,iEdge)))
!        !area = area + 1.0_FP
!    end do
!    !errSum(1,:) = (errSumX/area)
!    !errSum(2,:) = (errSumY/area)
!    errSum(1,:) = (errSumX)
!    errSum(2,:) = (errSumY)
!    open( unit = 1010, file = trim(adjustl(filePath))//'/fluxError_edgept.dat', form = 'formatted', &
!          access = 'append' )
!    write(varfmt,'(a,i0,a,i0,a)') '(i0,e24.16e2,',2*nEqns,'(e24.16e2))'
!    write(1010,varfmt) nCells,sqrt(totalVolume/real(nCells+nNodes+nEdges)), &
!                        errSum(1,:), errSum(2,:)
!    close( 1010 )
!
!    errSum(:,:) = 0.0_FP
!    errSumX(:) = 0.0_FP
!    errSumY(:) = 0.0_FP
!    area = 0.0_FP
!    do iNode = 1,nNodes
!        !errSumX(:) = errSumX(:) + &
!        !    abs(nodePtFluxExact(1,:,gNode)-nodePtFlux(1,:,gNode)) 
!        !errSumY(:) = errSumY(:) + &
!        !    abs(nodePtFluxExact(2,:,gNode)-nodePtFlux(2,:,gNode))
!        errSumX(:) = max(errSumX(:),abs(nodePtFluxExact(1,:,iNode)-nodePtFlux(1,:,iNode)))
!        errSumY(:) = max(errSumY(:),abs(nodePtFluxExact(2,:,iNode)-nodePtFlux(2,:,iNode)))
!        area = area + 1.0_FP
!    end do
!    !errSum(1,:) = (errSumX/area)
!    !errSum(2,:) = (errSumY/area)
!    errSum(1,:) = (errSumX)
!    errSum(2,:) = (errSumY)
!    open( unit = 10101, file = trim(adjustl(filePath))//'/fluxError_nodept.dat', form = 'formatted', &
!          access = 'append' )
!    write(varfmt,'(a,i0,a,i0,a)') '(i0,e24.16e2,',2*nEqns,'(e24.16e2))'
!    write(10101,varfmt) nCells,sqrt(totalVolume/real(nCells+nNodes+nEdges)), &
!                        errSum(1,:), errSum(2,:)
!    close( 10101 )

end subroutine outputAux
!-------------------------------------------------------------------------------
end module inputOutput
!-------------------------------------------------------------------------------

