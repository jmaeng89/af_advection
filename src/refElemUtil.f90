!-------------------------------------------------------------------------------
!> @purpose 
!>  Reference element utilities.  
!>
!> @author
!>  J. Brad Maeng
!>  
!> @history
!>  23 September 2016 - Initial creation
!>
module refElemUtil

    use solverVars, only: FP
    implicit none

contains
!-------------------------------------------------------------------------------
!> @purpose 
!>  Line 3 node quadratic lagrange shape/test function 
!>
!> @author
!>  J. Brad Maeng
!>
!> @history
!>  27 September 2016 - Initial Creation
!>
function line_quadraticTestFunc(nDim,xi) result(phi)

    implicit none

    ! Interface variables
    integer, intent(in) :: nDim

    real(FP), intent(in) :: xi(nDim) ! reference coordinates

    ! Function variable
    real(FP) :: phi(nDim+2) 

    phi(1) = (1.0_FP-2.0_FP*xi(1))*(1.0_FP-xi(1)) 
    phi(2) = 4.0_FP*xi(1)*(1.0_FP-xi(1))
    phi(3) = xi(1)*(2.0_FP*xi(1)-1.0_FP)

end function line_quadraticTestFunc
!-------------------------------------------------------------------------------
!> @purpose 
!>  Line 3 node quadratic lagrange shape/test function 
!>
!> @author
!>  J. Brad Maeng
!>
!> @history
!>  27 September 2016 - Initial Creation
!>
function line_gradQuadraticTestFunc(nDim,xi) result(gPhi)

    implicit none

    ! Interface variables
    integer, intent(in) :: nDim

    real(FP), intent(in) :: xi(nDim) ! reference coordinates

    ! Function variable
    real(FP) :: gPhi(nDim,nDim+2) 

    gPhi(1,1) = -2.0_FP*(1.0_FP-xi(1))  &
                -1.0_FP*(1.0_FP-2.0_FP*xi(1))  
    gPhi(1,2) = 4.0_FP*(1.0_FP-xi(1))  &
                -4.0_FP*xi(1)
    gPhi(1,3) = (2.0_FP*xi(1)-1.0_FP) + &
                2.0_FP*xi(1)

end function line_gradQuadraticTestFunc
!-------------------------------------------------------------------------------
!> @purpose 
!>  Triangle 3 node linear lagrange shape/test function 
!>
!> @author
!>  J. Brad Maeng
!>
!> @history
!>  22 September 2016 - Initial Creation
!>
function tri_linearTestFunc(nDim,xi) result(phi)

    implicit none
    
    ! Interface variables
    integer, intent(in) :: nDim

    real(FP), intent(in) :: xi(nDim) ! reference coordinates

    ! Function variable
    real(FP) :: phi(nDim+1) 

    phi(1) = 1.0_FP - xi(1) - xi(2)
    phi(2) = xi(1)
    phi(3) = xi(2)

end function tri_linearTestFunc
!-------------------------------------------------------------------------------
!> @purpose 
!>  Triangle 3 node linear lagrange shape/test function gradients 
!>
!> @author
!>  J. Brad Maeng
!>
!> @history
!>  22 September 2016 - Initial Creation
!>
function tri_gradLinearTestFunc(nDim,xi) result(gPhi)

    implicit none
    
    ! Interface variables
    integer, intent(in) :: nDim

    real(FP), intent(in) :: xi(nDim) ! reference coordinates

    ! Function variable
    real(FP) :: gPhi(nDim,nDim+1) 

    ! dPhi/dxi
    gPhi(1,1) = -1.0_FP
    gPhi(1,2) = 1.0_FP
    gPhi(1,3) = 0.0_FP

    ! dPhi/deta
    gPhi(2,1) = -1.0_FP
    gPhi(2,2) = 0.0_FP
    gPhi(2,3) = 1.0_FP

end function tri_gradLinearTestFunc
!-------------------------------------------------------------------------------
!> @purpose 
!>  Triangle 6 node quadratic lagrange shape/test function 
!>
!> @author
!>  J. Brad Maeng
!>
!> @history
!>  22 September 2016 - Initial Creation
!>
function tri_quadraticTestFunc(nDim,xi) result(phi)

    implicit none

    ! Interface variables
    integer, intent(in) :: nDim

    real(FP), intent(in) :: xi(nDim) ! reference coordinates

    ! Function variable
    real(FP) :: phi(nDim*(nDim+1)) 

    ! Local variable
    real(FP) :: s1, s2, s3  ! triangle area coordinate

    s1 = 1.0_FP - (xi(1)+xi(2))
    s2 = xi(1)
    s3 = xi(2)

    phi(1) = s1*(2.0_FP*s1-1.0_FP)
    phi(2) = 4.0_FP*s2*s3
    phi(3) = s2*(2.0_FP*s2-1.0_FP)
    phi(4) = 4.0_FP*s3*s1
    phi(5) = s3*(2.0_FP*s3-1.0_FP)
    phi(6) = 4.0_FP*s1*s2
    
end function tri_quadraticTestFunc
!-------------------------------------------------------------------------------
!> @purpose 
!>  Triangle 6 node quadratic lagrange shape/test function gradients
!>
!> @author
!>  J. Brad Maeng
!>
!> @history
!>  22 September 2016 - Initial Creation
!>
function tri_gradQuadraticTestFunc(nDim,xi) result(gPhi)

    implicit none

    ! Interface variables
    integer, intent(in) :: nDim

    real(FP), intent(in) :: xi(nDim) ! reference coordinates

    ! Function variable
    real(FP) :: gPhi(nDim,nDim*(nDim+1)) 

    ! Local variable
    real(FP) :: s1, s2, s3  ! triangle area coordinate

    s1 = 1.0_FP - (xi(1)+xi(2))
    s2 = xi(1)
    s3 = xi(2)

    ! dPhi/dxi
    gPhi(1,1) = 1.0_FP-4.0_FP*s1
    gPhi(1,2) = 4.0_FP*s3
    gPhi(1,3) = 4.0_FP*s2-1.0_FP
    gPhi(1,4) = -4.0_FP*s3
    gPhi(1,5) = 0.0_FP
    gPhi(1,6) = 4.0_FP*(s1-s2)
    
    ! dPhi/deta
    gPhi(2,1) = 1.0_FP-4.0_FP*s1
    gPhi(2,2) = 4.0_FP*s2
    gPhi(2,3) = 0.0_FP
    gPhi(2,4) = 4.0_FP*(s1-s3)
    gPhi(2,5) = 4.0_FP*s3-1.0_FP
    gPhi(2,6) = -4.0_FP*s2

end function tri_gradQuadraticTestFunc
!-------------------------------------------------------------------------------
!> @purpose 
!>  Quadrilateral 4 node bilinear lagrange shape/test function 
!>
!> @author
!>  J. Brad Maeng
!>
!> @history
!>  22 September 2016 - Initial Creation
!>
function quadril_linearTestFunc(nDim,xi) result(phi)

    implicit none
    
    ! Interface variables
    integer, intent(in) :: nDim

    real(FP), intent(in) :: xi(nDim) ! reference coordinates

    ! Function variable
    real(FP) :: phi(2*nDim) 

    ! Local variables
    real(FP) :: s1, s2  ! reference coordinate

    ! change reference coord from [0,1] to [-1,1] to use in basis functions
    s1 = 2.0_FP*xi(1)-1.0_FP
    s2 = 2.0_FP*xi(2)-1.0_FP
    !s1 = xi(1)
    !s2 = xi(2)

    ! x,y in [-1,1]
    phi(1) = 0.25_FP*(1.0_FP-s1)*(1.0_FP-s2)
    phi(2) = 0.25_FP*(1.0_FP+s1)*(1.0_FP-s2)
    phi(3) = 0.25_FP*(1.0_FP+s1)*(1.0_FP+s2)
    phi(4) = 0.25_FP*(1.0_FP-s1)*(1.0_FP+s2)

end function quadril_linearTestFunc
!-------------------------------------------------------------------------------
!> @purpose 
!>  Quadrilateral 4 node bilinear lagrange shape/test function gradients
!>
!> @author
!>  J. Brad Maeng
!>
!> @history
!>  22 September 2016 - Initial Creation
!>
function quadril_gradLinearTestFunc(nDim,xi) result(gPhi)

    implicit none
    
    ! Interface variables
    integer, intent(in) :: nDim

    real(FP), intent(in) :: xi(nDim) ! reference coordinates

    ! Function variable
    real(FP) :: gPhi(nDim,2*nDim) 

    ! Local variables
    real(FP) :: s1, s2  ! reference coordinate

    ! change reference coord from [0,1] to [-1,1] to use in basis functions
    s1 = 2.0_FP*xi(1)-1.0_FP
    s2 = 2.0_FP*xi(2)-1.0_FP
    !s1 = xi(1)
    !s2 = xi(2)

    ! dPhi/dxi
    gPhi(1,1) = -0.25_FP*(1.0_FP-s2)
    gPhi(1,2) = 0.25_FP*(1.0_FP-s2)
    gPhi(1,3) = 0.25_FP*(1.0_FP+s2)
    gPhi(1,4) = -0.25_FP*(1.0_FP+s2)

    ! dPhi/eta
    gPhi(2,1) = -0.25_FP*(1.0_FP-s1)
    gPhi(2,2) = -0.25_FP*(1.0_FP+s1)
    gPhi(2,3) = 0.25_FP*(1.0_FP+s1)
    gPhi(2,4) = 0.25_FP*(1.0_FP-s1)

end function quadril_gradLinearTestFunc
!-------------------------------------------------------------------------------
!> @purpose 
!>  Quadrilateral 9 node biquadratic lagrange shape/test function 
!>
!> @author
!>  J. Brad Maeng
!>
!> @history
!>  22 September 2016 - Initial Creation
!>
!>----------------------------------------------
!> node indices are positive 
!> edge indices are negative
!>  -1 < xi(1) < 1
!>  -1 < xi(2) < 1
!>
!>        4 -- (-3) -- 3   
!>        |            |
!>       (-4)    9   (-2)
!>        |            |
!>        1 -- (-1) -- 2
!>
!>----------------------------------------------
function quadril_quadraticTestFunc(nDim,xi) result(phi)

    implicit none
    
    ! Interface variables
    integer, intent(in) :: nDim

    real(FP), intent(in) :: xi(nDim) ! reference coordinates

    ! Function variable
    real(FP) :: phi((nDim+1)*(nDim+1))

    ! Local variables
    real(FP) :: s1, s2  ! reference coordinate

    ! change reference coord from [0,1] to [-1,1] to use in basis functions
    s1 = 2.0_FP*xi(1)-1.0_FP
    s2 = 2.0_FP*xi(2)-1.0_FP
    !s1 = xi(1)
    !s2 = xi(2)

    ! nodes
    phi(1) = 0.25_FP*(s1-1.0_FP)*(s2-1.0_FP)*s1*s2
    phi(2) = 0.25_FP*(s1+1.0_FP)*(s2-1.0_FP)*s1*s2
    phi(3) = 0.25_FP*(s1+1.0_FP)*(s2+1.0_FP)*s1*s2
    phi(4) = 0.25_FP*(s1-1.0_FP)*(s2+1.0_FP)*s1*s2
    ! edges
    phi(5) = 0.5_FP*(1.0_FP-s1*s1)*(s2-1.0_FP)*s2
    phi(6) = 0.5_FP*(1.0_FP-s2*s2)*(s1+1.0_FP)*s1
    phi(7) = 0.5_FP*(1.0_FP-s1*s1)*(s2+1.0_FP)*s2
    phi(8) = 0.5_FP*(1.0_FP-s2*s2)*(s1-1.0_FP)*s1
    ! centroid
    phi(9) = (1.0_FP-s1*s1)*(1.0_FP-s2*s2)

end function quadril_quadraticTestFunc
!-------------------------------------------------------------------------------
!> @purpose 
!>  Quadrilateral 9 node biquadratic lagrange shape/test function gradients
!>
!> @author
!>  J. Brad Maeng
!>
!> @history
!>  22 September 2016 - Initial Creation
!>
function quadril_gradQuadraticTestFunc(nDim,xi) result(gPhi)

    implicit none
    
    ! Interface variables
    integer, intent(in) :: nDim

    real(FP), intent(in) :: xi(nDim) ! reference coordinates

    ! Function variable
    real(FP) :: gPhi(nDim,(nDim+1)*(nDim+1))

    ! Local variables
    real(FP) :: s1, s2  ! reference coordinate

    ! change reference coord from [0,1] to [-1,1] to use in basis functions
    s1 = 2.0_FP*xi(1)-1.0_FP
    s2 = 2.0_FP*xi(2)-1.0_FP
    !s1 = xi(1)
    !s2 = xi(2)

    ! dPhi/dxi
    gPhi(1,1) = 0.25_FP*(s2-1.0_FP)*s2*(2.0_FP*s1-1.0_FP)
    gPhi(1,2) = 0.25_FP*(s2-1.0_FP)*s2*(2.0_FP*s1+1.0_FP)
    gPhi(1,3) = 0.25_FP*(s2+1.0_FP)*s2*(2.0_FP*s1+1.0_FP)
    gPhi(1,4) = 0.25_FP*(s2+1.0_FP)*s2*(2.0_FP*s1-1.0_FP)
    gPhi(1,5) = -s1*s2*(s2-1.0_FP)
    gPhi(1,6) = 0.5_FP*(1.0_FP-s2*s2)*(2.0_FP*s1+1.0_FP)
    gPhi(1,7) = -s1*s2*(s2+1.0_FP)
    gPhi(1,8) = 0.5_FP*(1.0_FP-s2*s2)*(2.0_FP*s1-1.0_FP)
    gPhi(1,9) = (-2.0_FP*s1)*(1.0_FP-s2*s2) 

    ! dPhi/deta
    gPhi(2,1) = 0.25_FP*(s1-1.0_FP)*s1*(2.0_FP*s2-1.0_FP)
    gPhi(2,2) = 0.25_FP*(s1+1.0_FP)*s1*(2.0_FP*s2-1.0_FP)
    gPhi(2,3) = 0.25_FP*(s1+1.0_FP)*s1*(2.0_FP*s2+1.0_FP)
    gPhi(2,4) = 0.25_FP*(s1-1.0_FP)*s1*(2.0_FP*s2+1.0_FP)
    gPhi(2,5) = 0.5_FP*(1.0_FP-s1*s1)*(2.0_FP*s2-1.0_FP)
    gPhi(2,6) = -s1*s2*(s1+1.0_FP)
    gPhi(2,7) = 0.5_FP*(1.0_FP-s1*s1)*(2.0_FP*s2+1.0_FP)
    gPhi(2,8) = -s1*s2*(s1-1.0_FP)
    gPhi(2,9) = (1.0_FP-s1*s1)*(-2.0_FP*s2)

end function quadril_gradQuadraticTestFunc
!-------------------------------------------------------------------------------
end module refElemUtil
