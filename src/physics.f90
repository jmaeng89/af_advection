!-------------------------------------------------------------------------------
!> @purpose 
!>  Physics routines that are independent of solution method
!>
!> @author
!>  Timothy A. Eymann
!>
!> @history
!>  20 September 2016 - Integrate all advective systems (Maeng)
!>
module physics

    use solverVars, only: FP
    implicit none

    integer, parameter :: LINADV_FLUX = 1,      & !< flag for advection flux
                          PLESSEULER_FLUX = 2,  & !< flag for pressureless Euler flux
                          BURGERS_FLUX = 3        !< flag for Burgers' flux

    integer, parameter :: DENSITY = 1,  & !< density index
                          UVEL = 2,     & !< x-component of velocity
                          VVEL = 3        !< y-component of velocity
    
contains
!-------------------------------------------------------------------------------
!> @purpose 
!>  Convert a vector of primitive values to conservative values.  Assumes
!>  Euler equation
!>
!> @history
!>  14 May 2013 - Initial creation
!>  26 January 2015 - Modified for pressureless euler (Maeng)
!>  16 February 2015 - Added multidimension case (Maeng)
!>  9 October 2015 - Euler equations (Maeng)
!>
function prim2conserv(nDim,nEqns,q)

    implicit none

    ! Interface vairables
    integer, intent(in) :: nDim,    & !< problem dimension 
                           nEqns      !< number of fields in state vector
    
    real(FP), intent(in) :: q(nEqns) !< primitive state vector

    ! Function variable
    real(FP) :: prim2conserv(nEqns)

    ! Local variables
    real(FP) :: velMag !< square of velocity magnitude

    prim2conserv(:) = 0.0_FP
    ! pressureless euler equations
    select case ( nDim )
        
        case ( 1 )

            prim2conserv(1) = q(1)
            prim2conserv(2) = q(1)*q(2)

        case ( 2 )

            prim2conserv(1) = q(DENSITY)
            prim2conserv(2) = q(DENSITY)*q(UVEL)
            prim2conserv(3) = q(DENSITY)*q(VVEL)

    end select

end function prim2conserv
!-------------------------------------------------------------------------------
!> @purpose 
!>  Convert a vector of constervative values to primitive values.  Assumes
!>  Euler equation
!>
!> @history
!>  14 May 2013 - Initial creation
!>  26 January 2015 - Modified for pressureless euler (Maeng)
!>
function conserv2prim(nDim,nEqns,q)

    implicit none

    ! Interface vairables
    integer, intent(in) :: nDim,    & !< problem dimension
                           nEqns      !< number of fields in state vector
    
    real(FP), intent(in) :: q(nEqns) !< conservative state vector

    ! Function variable
    real(FP) :: conserv2prim(nEqns)

    conserv2prim(:) = 0.0_FP
    select case ( nDim )
        
        case ( 1 )

            conserv2prim(1) = q(1)
            conserv2prim(2) = q(2)/q(1)

        case ( 2 )

            ! pressureless Euler, isentropic Euler
            conserv2prim(1) = q(1)
            conserv2prim(2) = q(2)/q(1)
            conserv2prim(3) = q(3)/q(1)

    end select

end function conserv2prim
!-------------------------------------------------------------------------------
!> @purpose 
!>  Calculate an analytical flux from primitive state vector.\n
!>  Assumes standard Euler ordering of primitive equations.
!>
!> @history
!>  17 May 2013 - Initial creation (Eymann)
!>  13 January 2015 - Modified LINADV_FLUX for 2D Burgers' equations (Maeng)
!>  1 February 2015 - Added pressure-less euler fluxes (Maeng)
!>  16 February 2015 - Modified to incorporate multidimensional euler flux (Maeng)
!>
function flux( nDim,nEqns,fluxType,q,lam )

    implicit none

    ! Interface variables
    integer, intent(in) :: nDim,    & !< problem dimension
                           nEqns,   & !< number of equations
                           fluxType   !< equation type

    real(FP), intent(in) :: q(nEqns) !< state

    real(FP), intent(in), optional :: lam(nDim) !< linear wave speed

    ! Function variable
    real(FP) :: flux(nDim,nEqns)
    
    ! Local variables
    integer :: iDir       !< direction index

    real(FP) :: p,    & !< pressure 
                H       !< total specific enthalpy

    flux = 0.0_FP
    
    select case ( fluxType )

        case ( LINADV_FLUX )
            ! linear advection

            if ( nDim  == 1 ) then

                if ( present(lam) ) then
                    flux(1,:) = q(:)*lam(1)
                else
                    ! only evaluate flux for iEq == 1
                    flux(1,1) = q(1)*q(2)
                end if
            else

                if ( present(lam) ) then
                    flux(1,:) = q(:)*lam(1)
                    flux(2,:) = q(:)*lam(2)
                else
                    ! only evaluate flux for iEq == 1
                    flux(1,1) = q(1)*q(UVEL)
                    flux(2,1) = q(1)*q(VVEL)
                end if
            end if

        case ( BURGERS_FLUX )

            if ( nDim == 1 ) then
                flux(1,:) = (/ 0.0_FP, &
                               0.5_FP*q(UVEL)*q(UVEL) /)
            else
                ! only evaluate flux for iEq == 2
                ! scalar conservation law
                ! u_t + (0.5*u^2)_x + (0.5*u^2)_y = 0
                flux(1,:) = (/ 0.0_FP, &
                               0.5_FP*q(UVEL)*q(UVEL), &
                               0.5_FP*q(UVEL)*q(UVEL) /)
                flux(2,:) = (/ 0.0_FP, &
                               0.5_FP*q(VVEL)*q(VVEL), &
                               0.5_FP*q(VVEL)*q(VVEL) /)

                !! TODO: think about what is right
                !flux(1,:) = (/ 0.0_FP, &
                !               0.5_FP*q(UVEL)*q(UVEL), &
                !               0.5_FP*q(UVEL)*q(UVEL) /)
                !flux(2,:) = (/ 0.0_FP, &
                !               0.5_FP*q(UVEL)*q(UVEL), &
                !               0.5_FP*q(UVEL)*q(UVEL) /)
            end if
            
        case ( PLESSEULER_FLUX )
            ! conservative pressureless euler fluxes

            if ( nDim == 1 ) then 
                flux(1,:) = (/ q(DENSITY)*q(UVEL), &
                               q(DENSITY)*q(UVEL)*q(UVEL) /) 
            else
                flux(1,:) = (/ q(DENSITY)*q(UVEL), &
                               q(DENSITY)*q(UVEL)*q(UVEL), &
                               q(DENSITY)*q(UVEL)*q(VVEL) /)
                
                flux(2,:) = (/ q(DENSITY)*q(VVEL), &
                               q(DENSITY)*q(VVEL)*q(UVEL), &
                               q(DENSITY)*q(VVEL)*q(VVEL) /)
            end if

        case default
            write(*,'(a,i0,a)') 'ERROR: Flux type = ',fluxtype,' not supported.'
            stop
    
    end select

end function flux
!-------------------------------------------------------------------------------
end module physics
!-------------------------------------------------------------------------------
