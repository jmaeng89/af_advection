!-------------------------------------------------------------------------------
!> @purpose 
!>  Master program for 2d active flux euler equations solver
!>
!> @author
!>  J. Brad Maeng
!>
!> @history
!>  26 January 2015 - Initial creation
!>  6 May 2015 - Modified for barotropic euler
!>  10 October 2015 - Euler
!>  28 March 2016 - Include dump and shutdown inquiries
!>  20 September 2016 - Integrate all advective systems (Maeng)
!>  11 January 2018 - Tecplot outputs (Maeng)
!>          'tecplotVisualUtil.f90' uses 'tecio.f90' which requires linking
!>          'output_tecplot.f90' doesn't and creates native ascii data for TecPlot
!>          Comment/uncomment tecplot output subroutines according to the module
!>          
!>
program afAdv

    use solverVars, only: nEqns, nIter, FP, tSim, dtIn, tFinal, cfl
    use startStop, only: startUp, shutDown
    use meshUtil, only: nDim, nEdges, nNodes, nCells
    use update, only: advectionSystems, test, deltaAvg
    use timeControl, only: updateDt
    use inputOutput, only: solFile, solFileTmp, solOutputFreq, outputBinary, outputAux, &
                           dumpDataInquiry, shutDownInquiry, outputResidual, &
                           dumpSolName, residFile
    use tecplotVisualUtil
    !use output_tecplot
    use postProc

    implicit none

    ! Local variables
    logical :: switch

    integer :: iter,    & !< iteration number
               iZone      !< zone number

    real(FP) :: t0, & !< starting time stamp
                tf    !< final time

    character(200) :: dummy_name

    ! Initialize simulation
    call startUp

    ! IF tecplotVisualUtil is used !!!!!!!!!!!!!
    call initTecPlotFile()
    call outputTecPlotMesh()
    call outputTecPlotSolution(0,tSim)
    !! IF NOT tecplotVisualUtil !!!!!!!!!!!!!!!!!
    !call initTecPlotFileAscii()
    !call outputTecPlotSolutionAscii(0,tSim)
    !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!

    call outputBinary(0,tSim)

    !call outputResidual(0,tSim)
    !call outputAux(0,tSim)

    call cpu_time(t0)
    iZone = 2
    ! Main loop
    !do while ( tSim <= tFinal ) ! if dtIn varies    
    do iter = 1, nIter

        ! advection systems
        call advectionSystems(nDim,nEqns,iter)

        ! test functions
        !call test()
        
        ! update simulation time
        tSim = tSim + dtIn
        !! update time if time step should vary
        !call updateDt(cfl,dtIn)

        ! update reconstruction with NEW data and plot
        write(*,'(a,i0,4x,a,e24.15e2)') 'Finished iteration: ',iter, 'Solution time: ', tSim
        if (( mod(iter,solOutputFreq) == 0 ) .or. ( iter == nIter )) then

            ! IF tecplotVisualUtil is used !!!!!!!!!!!!!
            call outputTecPlotSolution(iter,tSim)
            !! IF NOT tecplotVisualUtil !!!!!!!!!!!!!!!!!
            !call outputTecPlotSolutionAscii(iter,tSim)
            !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!

            call outputBinary(iter,tSim)

            ! aux data output
            !if ( iter == nIter ) call outputAux(iter,tSim)
            !call outputAux(iter,tSim)

        end if
        !call outputAux(iter,tSim)
        !call outputResidual(iter,tSim)


        ! SOLUTION CONTROL ROUTINES

        ! data dump file is created if 'dump' is created within the directory
        call dumpDataInquiry(iter,tSim,switch)
        if ( switch ) then 

            ! IF tecplotVisualUtil is used !!!!!!!!!!!!!
            call initTecPlotFile(dumpSolName)
            ! switch to dump solution file
            call switchTecPlotFile(2)
            ! create mesh for the new TecPlot file
            call outputTecPlotMesh
            call outputTecPlotSolution(iter,tSim) 
            ! close the new file
            call closeTecPlotFile()
            ! switch to original file
            call switchTecPlotFile(1)
            !! IF NOT tecplotVisualUtil !!!!!!!!!!!!!!!!!
            !call initTecPlotFileAscii(dumpSolName,solFileTmp)
            !call outputTecPlotSolutionAscii(iter,tSim,solFileTmp) 
            !call closeTecPlotFileAscii(solFileTmp)
            !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!

            ! reset switch to prevent further unnecessary activation of inquiry
            switch = .false.
        end if

        ! terminate simulation early if 'shutdown' is created within the directory 
        call shutDownInquiry(switch)
        if ( switch ) then 
            ! wrap up final solution and exit the iteration loop

            ! IF tecplotVisualUtil is used !!!!!!!!!!!!!
            call outputTecPlotSolution(iter,tSim)
            !! IF NOT tecplotVisualUtil !!!!!!!!!!!!!!!!!
            !call outputTecPlotSolutionAscii(iter,tSim)
            !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!

            call outputBinary(iter,tSim)
            !call outputResidual(iter,tSim)
            exit ! exit iteration loop and shut down
        end if

    end do

    ! Shut down
    call cpu_time(tf)
    call shutDown(t0,tf)

    ! IF tecplotVisualUtil is used !!!!!!!!!!!!!
    call closeTecPlotFile()
    !! IF NOT tecplotVisualUtil !!!!!!!!!!!!!!!!!
    !call closeTecPlotFileAscii()
    !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!

end program afAdv
!-------------------------------------------------------------------------------
