!-------------------------------------------------------------------------------
!> @purpose 
!>  Mesh related routines
!>
!> @author
!>  Timothy A. Eymann
!>  
!> @history
!>  8 May 2015 - Added acNodeCells to be used in acoustic solver (Maeng)
!>  10 May 2015 - Added acFaceCells acEdgeCells (Maeng)
!>  20 September 2016 - Integrate all advective systems (Maeng)
!>
module meshUtil

    use solverVars, only: FP
    implicit none

    integer :: nDim,            & !< number of dimensions
               nCells,          & !< number of physical cells
               nGhostCells,     & !< number of ghost cells
               nAllCells,       & !< total number of cells
               physCellStart,   & !< starting index of physical cells
               physCellEnd,     & !< ending index of physical cells
               nNodes,          & !< number of physical nodes
               nAllNodes,       & !< total number of nodes
               physNodeStart,   & !< starting index of physical nodes
               physNodeEnd,     & !< ending index of physical nodes
               nZones,          & !< number of zones in mesh
               nPatches,        & !< number of boundary patches in mesh
               nFaces,          & !< number of faces in mesh
               nEdges,          & !< number of edges in mesh
               maxNodesPerFace, & !< maximum number of nodes per face
               maxFacesPerCell, & !< maximum number of faces per cell
               maxNodesPerCell    !< maximum number of nodes per cell

    integer, parameter :: TRI_MESH = 1, & !< 2D triangular mesh
                          QUAD_MESH = 2   !< 2D quadrilateral mesh

    integer, allocatable :: cellFaces(:,:),     & !< faces of a cell
                            cellNodes(:,:),     & !< nodes in a cell
                            faceNodes(:,:),     & !< nodes in a face
                            faceCells(:,:),     & !< cells sharing face
                            edgeCell(:,:),      & !< cell connected to edge
                            nodeCell(:,:)         !< cell connected to node

    real(FP), allocatable :: cellCentroid(:,:), & !< cell centroid coordinates
                             cellVolume(:),     & !< cell volume ( area / width )
                             cellDetJac(:),     & !< determinant of Jacobian
                             cellInvJac(:,:,:), & !< inverse Jacobian
                             cellJac(:,:,:),    & !< Jacobian
                             cellAngles(:,:),   & !< interior angles for cell
                             faceArea(:),       & !< face area (edge length)
                             faceNormal(:,:),   & !< face normal (points to right cell)
                             edgeCoord(:,:),    & !< coordinate of edge midpoint
                             nodeCoord(:,:),    & !< node coordinates
                             xiNode(:,:),       & !< node coord in ref. space
                             xiEdge(:,:),       & !< edge coord in ref. space
                             xMax(:),           & !< maximum extent of grid
                             xMin(:)              !< minimum extent of grid

    real(FP) :: totalVolume     !< total volume of domain

    ! mesh/geometry variable
    real(FP) :: lMin !< minimum distance in mesh

contains
!-------------------------------------------------------------------------------
!> @purpose 
!>  Fill connectivity (mapping) arrays and calculate mesh geometry
!>
!> @author
!>  Timothy A. Eymann
!>
!> @history
!>  8 March 2012 - Initial Creation
!>  8 May 2012 - Add 1-D connectivity
!>  8 May 2015 - Added acNodeCells to be used in acoustic solver (Maeng)
!>  10 May 2015 - Added acEdgeCells acFaceCells (Maeng)
!>  21 September 2016 - Modified connectivity for quadrilateral elements (Maeng)
!>
subroutine connect

    use solverVars, only: iLeft, iRight, dtIn, pi, eps, &
                          truncDecPts, SHORTINT, LONGINT
    use mathutil, only: pointInTri, pointInQuadril

    implicit none

    ! Local variables
    integer :: iCell,   & !< cell index
               jCell,   & !< cell index
               iFace,   & !< face index
               jFace,   & !< face index
               iNode,   & !< node index
               jNode,   & !< node index
               node1,   &
               node2,   &
               node3,   & !< ordered nodes
               node4,   & !< ordered nodes
               face1,   &
               face2,   &
               face3,   & !< ordered faces
               face4,   & !< ordered faces
               tFace,   & !< temporary face storage
               iDir,    & !< direction index
               iSide,   & !< left/right index
               iP1,     & !< i+1 index
               iM1       !< i-1 index

    real(FP) :: coordSum,           & !< sum of coordinate values (for average)
                xN(nDim),           & !< 
                xVert(nDim,maxNodesPerCell), &
                xP1(nDim),          & !< x(i+1)
                xM1(nDim),          & !< x(i-1)
                x(maxNodesPerCell), & !< x-coordinates of cell nodes
                y(maxNodesPerCell), & !< y-coordinates of cell nodes
                xDiff(nDim),        & !< difference in node coordinates
                tVec(nDim)            !< tangential vector

    real(FP) :: xlmax,              &
                xlmin,              &
                ylmax,              &
                ylmin,              &
                diagVec1(2),        &
                diagVec2(2),        &
                A(2,2),             &
                Ainv(2,2),          &
                coef(2),            &
                xInt(2),            &
                ADet 

    real(FP), parameter :: ds = 1.0e-13_FP ! slightly positive number

    real(FP), parameter :: coordTol = 1.0e-13_FP ! geometric coordinate tolerance

    totalVolume = 0.0_FP
    xMax(:) = -1.0e6_FP
    xMin(:) = 1.0e6_FP

    ! fill cell to face mapping array ( not ordered )
    do iFace = 1, nFaces
        do iSide = iLeft, iRight
            jCell = faceCells(iSide,iFace)

            ! only operate on physical cells
            if ( jCell > 0 ) then
                if ( cellFaces(1,jCell) == 0 ) then
                    cellFaces(1,jCell) = iFace
                else
                    ! find next open spot in array
                    jFace = 2
                    do while ( abs(cellFaces(jFace,jCell)) > 0 ) 
                        jFace = jFace + 1
                    end do
                    cellFaces(jFace,jCell) = iFace
                end if
            end if
        end do
    end do

    ! loop over cells to order nodes, faces, and calculate Jacobian
    select case ( nDim )

        case (1)

            do iCell = 1, nCells
                do iNode = 1, maxNodesPerCell
                    cellNodes(iNode,iCell) = iCell + (iNode-1)
                    cellFaces(iNode,iCell) = iCell + (iNode-1)
                end do
                
                node1 = iCell
                node2 = iCell+1
                x(1) = nodeCoord(1,node1)
                x(2) = nodeCoord(1,node2)
                
                cellJac(1,1,iCell) = x(2) - x(1)
                cellDetJac(iCell) = cellJac(1,1,iCell)
                cellInvJac(1,1,iCell) = 1.0_FP/cellJac(1,1,iCell)
                cellVolume(iCell) = cellJac(1,1,iCell)
                cellCentroid(:,iCell) = 0.5_FP*sum(x(:))

                ! store grid extents
                xMax(1) = max(xMax(1),maxVal(x(:)))
                xMin(1) = min(xMin(1),minVal(x(:)))

                ! calculate total volume of domain
                totalVolume = totalVolume + cellVolume(iCell)
            end do

        case (2)

            do iCell = 1, nCells
              
                select case ( maxFacesPerCell )

                  case ( 3 ) 
                    ! Triangle
                    ! assume cell nodes/faces are properly ordered
                    face1 = cellFaces(1,iCell)
                    node1 = faceNodes(1,face1)
                    node2 = faceNodes(2,face1)

                    do iFace = 2,maxFacesPerCell
                        if ((faceNodes(1,cellFaces(iFace,iCell))==node1).or. &
                            (faceNodes(2,cellFaces(iFace,iCell))==node1)) then
                            face3 = cellFaces(iFace,iCell)
                            if ( faceNodes(1,face3) == node1 ) then
                                node3 = faceNodes(2,face3)
                            else
                                node3 = faceNodes(1,face3)
                            end if
                        else
                            face2 = cellFaces(iFace,iCell)
                        end if
                    end do
 
                    ! enforce ccw ordering 
                    if ( (nodeCoord(1,node2)-nodeCoord(1,node1)) * &
                         (nodeCoord(2,node3)-nodeCoord(2,node1)) - &
                         (nodeCoord(1,node3)-nodeCoord(1,node1)) * &
                         (nodeCoord(2,node2)-nodeCoord(2,node1)) < 0.0_FP ) then
                       
                        ! use node 1 for temp storage to flip nodes 2,3
                        node1 = node2
                        node2 = node3
                        node3 = node1
                        node1 = faceNodes(1,face1)

                        ! flip faces 1,3
                        tFace = face1
                        face1 = face3
                        face3 = tFace
                    end if
                    cellNodes(:,iCell) = (/node1,node2,node3/)

                    ! order faces so that cellFaces(1,iCell) is opposite "node 1"
                    cellFaces(:,iCell) = (/face2,face3,face1/)

                    ! find Jacobian
                    x(:) = nodeCoord(1,cellNodes(:,iCell))
                    y(:) = nodeCoord(2,cellNodes(:,iCell))

                    ! coordinate jacobians are constant for linear triangular element
                    ! NOTE: this is transpose of standard definition of jacobian
                    cellJac(1,1,iCell) = x(2) - x(1)
                    cellJac(1,2,iCell) = x(3) - x(1)
                    cellJac(2,1,iCell) = y(2) - y(1)
                    cellJac(2,2,iCell) = y(3) - y(1)
                    cellDetJac(iCell) = (cellJac(1,1,iCell)*cellJac(2,2,iCell)) - &
                                        (cellJac(1,2,iCell)*cellJac(2,1,iCell))

                    cellInvJac(1,1,iCell) = cellJac(2,2,iCell)
                    cellInvJac(1,2,iCell) = -cellJac(1,2,iCell)
                    cellInvJac(2,1,iCell) = -cellJac(2,1,iCell)
                    cellInvJac(2,2,iCell) = cellJac(1,1,iCell)
                    cellInvJac(:,:,iCell) = (1.0_FP/cellDetJac(iCell)) * &
                                            cellInvJac(:,:,iCell)

                    cellVolume(iCell) = 0.5_FP*cellDetJac(iCell)

                  case ( 4 ) 
                    ! quadrilateral
                    !----------------------------------------------
                    ! node indices are positive 
                    ! edge indices are negative
                    !
                    !        4 -- (-3) -- 3   
                    !        |            |
                    !       (-4)    9   (-2)
                    !        |            |
                    !        1 -- (-1) -- 2
                    !
                    !----------------------------------------------
                    ! assume cell nodes/faces are properly ordered
                    face1 = cellFaces(1,iCell)
                    node1 = faceNodes(1,face1)
                    node2 = faceNodes(2,face1)
                    ! sort through current cell and construct un-ordered cellNodes
                    do iFace = 2,maxFacesPerCell
                        if (( faceNodes(1,cellFaces(iFace,iCell)) /= node1 ) .and. &
                            ( faceNodes(2,cellFaces(iFace,iCell)) /= node1 ) .and. &
                            ( faceNodes(1,cellFaces(iFace,iCell)) /= node2 ) .and. &
                            ( faceNodes(2,cellFaces(iFace,iCell)) /= node2 )) then
                            face3 = cellFaces(iFace,iCell)
                            node3 = faceNodes(1,face3)
                            node4 = faceNodes(2,face3)
                        end if
                    end do
                    do iFace = 2,maxFacesPerCell
                        if ( (( faceNodes(1,cellFaces(iFace,iCell)) == node3 ) .or. &
                              ( faceNodes(2,cellFaces(iFace,iCell)) == node3 ) ) .and. &
                             ( cellFaces(iFace,iCell) /= face3 )) then
                            face2 = cellFaces(iFace,iCell)
                        end if
                        if ( (( faceNodes(1,cellFaces(iFace,iCell)) == node4 ) .or. &
                              ( faceNodes(2,cellFaces(iFace,iCell)) == node4 ) ) .and. &
                             ( cellFaces(iFace,iCell) /= face3 )) then
                            face4 = cellFaces(iFace,iCell)
                        end if
                    end do
                    cellNodes(:,iCell) = (/node1,node2,node3,node4/) ! un-ordered cellNode index
                    cellFaces(:,iCell) = (/face1,face2,face3,face4/) 

                    x(:) = nodeCoord(1,cellNodes(:,iCell))
                    y(:) = nodeCoord(2,cellNodes(:,iCell))
                    do iNode = 1,4
                        xlmax = maxval(x(:))
                        ylmax = maxval(y(:))
                        xlmin = minval(x(:))
                        ylmin = minval(y(:))
                    end do
                    ! evaluate convex hull of n = 4 
                    ! for node1 there is only two possible choises for diagonal vector 
                    ! enforce ccw ordering 
                    do iNode = 3,4
                        jNode = iNode+1 
                        if ( jNode == 5 ) jNode = 3
                        diagVec1 = nodeCoord(:,cellNodes(iNode,iCell)) - nodeCoord(:,node1)
                        diagVec2 = nodeCoord(:,cellNodes(jNode,iCell)) - nodeCoord(:,node2)

                        ! construct matrix containing diagonal vectors
                        A(:,1) = diagVec1(:)
                        A(:,2) = diagVec2(:)
                        ADet = A(1,1)*A(2,2) - A(1,2)*A(2,1)

                        if ( abs(ADet) <= 20.0_FP*eps ) then
                            ! FIXME: better tolerance
                            !write(*,*) 'Skip singular element case or parallel lines'
                            cycle 
                        end if

                        ! invert A
                        Ainv(1,1) = A(2,2)
                        Ainv(1,2) = -A(1,2)
                        Ainv(2,1) = -A(2,1)
                        Ainv(2,2) = A(1,1)
                        Ainv(:,:) = Ainv(:,:)/ADet
                         
                        coef(:) = matmul(Ainv,(nodeCoord(:,node3)-nodeCoord(:,node1)))
                        ! intersection point            
                        xInt(:) = nodeCoord(:,node1) + coef(1)*diagVec1(:) 
                        ! is intersection point in the range?
                        if ( (xInt(1) > xlmin) .and. (xInt(1) < xlmax) .and. &
                             (xInt(2) > ylmin) .and. (xInt(2) < ylmax) ) then
                            ! check cross product for positive area == ccw ordering
                            ! FIXME: check again!
                            if ( ADet >= eps ) then
                                ! reverse order 
                                cellNodes(:,iCell) = (/node1,node2,node4,node3/) 
                                cellFaces(:,iCell) = (/face1,face4,face3,face2/) 
                            else
                                cellNodes(:,iCell) = (/node2,node1,node3,node4/) 
                                cellFaces(:,iCell) = (/face1,face2,face3,face4/) 
                            end if
                        end if
                        !write(*,*) iCell, cellNodes(:,iCell), ADet
                        !write(*,*) iCell, cellFaces(:,iCell)
                    end do

                    ! find Jacobian
                    x(:) = nodeCoord(1,cellNodes(:,iCell))
                    y(:) = nodeCoord(2,cellNodes(:,iCell))

                    ! coordinate jacobians are constant for linear quadrilateral element
                    ! NOTE: this is transpose of standard definition of jacobian
                    ! NOTE2: change reference coordinate range from [-1,1] to [0,1] thereby 
                    !        multiply each element of jacobian by 2
                    cellJac(1,1,iCell) = x(2) - x(1)
                    if ( abs(cellJac(1,1,iCell)) <= 10.0*eps ) cellJac(1,1,iCell) = 0.0_FP
                    cellJac(1,2,iCell) = x(4) - x(1)
                    if ( abs(cellJac(1,2,iCell)) <= 10.0*eps ) cellJac(1,2,iCell) = 0.0_FP
                    cellJac(2,1,iCell) = y(2) - y(1)
                    if ( abs(cellJac(2,1,iCell)) <= 10.0*eps ) cellJac(2,1,iCell) = 0.0_FP
                    cellJac(2,2,iCell) = y(4) - y(1)
                    if ( abs(cellJac(2,2,iCell)) <= 10.0*eps ) cellJac(2,2,iCell) = 0.0_FP

                    !cellJac(1,1,iCell) = (x(2) - x(1))/2.0_FP
                    !cellJac(1,2,iCell) = (x(4) - x(1))/2.0_FP
                    !cellJac(2,1,iCell) = (y(2) - y(1))/2.0_FP
                    !cellJac(2,2,iCell) = (y(4) - y(1))/2.0_FP
                    cellDetJac(iCell) = (cellJac(1,1,iCell)*cellJac(2,2,iCell)) - &
                                        (cellJac(1,2,iCell)*cellJac(2,1,iCell))

                    cellInvJac(1,1,iCell) = cellJac(2,2,iCell) 
                    cellInvJac(1,2,iCell) = -cellJac(1,2,iCell)
                    cellInvJac(2,1,iCell) = -cellJac(2,1,iCell)
                    cellInvJac(2,2,iCell) = cellJac(1,1,iCell)
                    cellInvJac(:,:,iCell) = (1.0_FP/cellDetJac(iCell)) * &
                                            cellInvJac(:,:,iCell)

                    !! check it out
                    !if ( iCell == 18722 ) write(*,*) x, y, eps
                    !if ( iCell == 18722 ) write(*,*) cellDetJac(iCell), cellJac(1,1,iCell)*cellJac(2,2,iCell)
                    !if ( iCell == 18722 ) write(*,*) cellJac(:,:,iCell)
                    !if ( iCell == 18722 ) write(*,*) cellInvJac(:,:,iCell)
                    
                    cellVolume(iCell) = cellDetJac(iCell)

                end select

                cellCentroid(:,iCell) = 1.0_FP/(real(maxFacesPerCell,FP))* &
                                        (/sum(x(:)),sum(y(:))/)

                !write(*,*) iCell, cellVolume(iCell), cellJac(:,:,iCell), cellDetJac(iCell)
                !write(*,*) iCell, cellVolume(iCell), cellCentroid(1,iCell), cellCentroid(2,iCell)
                !write(*,*) iCell, cellNodes(:,iCell)
                !write(*,*) iCell, cellFaces(:,iCell)

                ! store grid extents
                xMax(1) = max(xMax(1),maxVal(x(:)))
                xMin(1) = min(xMin(1),minVal(x(:)))
                xMax(2) = max(xMax(2),maxVal(y(:)))
                xMin(2) = min(xMin(2),minVal(y(:)))

                ! check for global node and edge for the cell
                !if ( iCell == 29 .or. iCell == 27 .or.  &
                !    iCell == 19 .or. iCell == 23 ) then
                !    write(*,*) 'cell: ', iCell
                !    write(*,*) 'node: ', cellNodes(:,iCell)
                !    write(*,*) 'edge: ', cellFaces(:,iCell)
                !end if

                ! calculate total volume of domain
                totalVolume = totalVolume + cellVolume(iCell)

            end do

    end select
    !write(*,*) cellNodes(:,11), cellJac(:,:,11)
    !write(*,*) cellNodes(:,12), cellJac(:,:,12)

    ! Loop over faces to calculate normals and coordinates
    do iFace = 1, nFaces

        ! calculate edge coordinate
        do iDir = 1, nDim
            coordSum = 0.0_FP
            do iNode = 1, maxNodesPerFace
                coordSum = coordSum + nodeCoord(iDir,faceNodes(iNode,iFace))
            end do
            ! in 1D and 2D, faces and edges are the same thing...
            !edgeCoord(iDir,iFace) = coordSum / maxNodesPerFace
            edgeCoord(iDir,iFace) = coordSum / real(maxNodesPerFace,FP)
        end do

        ! only need faceCells for 2d solver, but edge array needed for 3d
        edgeCell(1,iFace) = faceCells(iLeft,iFace)

        ! calculate face normals and face area
        select case ( nDim )

            case (1)
                if ( iFace == 1 ) then
                    faceNormal(:,iFace) = -1.0_FP
                else
                    faceNormal(:,iFace) = 1.0_FP
                end if
                faceArea(iFace) = 1.0_FP

            case (2)
                xDiff(:) = nodeCoord(:,faceNodes(2,iFace)) - &
                           nodeCoord(:,faceNodes(1,iFace))
                tVec(:) = xDiff(:)/norm2(xDiff(:))
               
                faceNormal(1,iFace) = -tVec(2)
                faceNormal(2,iFace) =  tVec(1)

                ! make sure normal points away from "left" cell
                iCell = faceCells(iLeft,iFace)
                xVert(1,:) = nodeCoord(1,cellNodes(:,iCell))
                xVert(2,:) = nodeCoord(2,cellNodes(:,iCell))
                xN(:) = edgeCoord(:,iFace) - ds*faceNormal(:,iFace)
                select case ( maxFacesPerCell )

                  case ( 3 ) ! triangle
                    if ( .not. pointInTri(xN,xVert) ) then
#ifdef VERBOSE
                        write(*,'(a,i0)') 'WARNING: Flipping normal for face ',iFace
#endif
                        faceNormal(:,iFace) = -1.0_FP*faceNormal(:,iFace)
                    end if

                  case ( 4 ) ! quadrilateral
                    if ( .not. pointInQuadril(xN,xVert) ) then
#ifdef VERBOSE
                        write(*,'(a,i0)') 'WARNING: Flipping normal for face ',iFace
#endif
                        faceNormal(:,iFace) = -1.0_FP*faceNormal(:,iFace)
                    end if
                end select

                faceArea(iFace) = norm2(xDiff(:))
        
        end select

    end do

    ! node to cell mapping (needed to start neighbor search)
    do iNode = 1, nNodes

        ! find first cell containing node
        do iCell = 1, nCells
            do jNode = 1, maxNodesPerCell
                if ( cellNodes(jNode,iCell) == iNode ) then
                    nodeCell(1,iNode) = iCell
                    exit
                end if
            end do
            if ( nodeCell(1,iNode) /= 0 ) exit
        end do

    end do

    inode = 0
    do iCell = 1, nCells
#ifdef VERBOSE
        ! check for negative Jacobians ( clockwise ordering )
        if ( cellDetJac(iCell) < 0 ) then
            inode = inode + 1
            write(*,*) ' cell: ',iCell
            write(*,*) 'nodes: ',cellNodes(:,iCell)
            write(*,*) 'faces: ',cellFaces(:,iCell)
            read(*,*)
        end if
#endif
        ! calculate angles in each cell
        if ( nDim == 2 ) then

            select case ( maxFacesPerCell ) 

                case (3)
                    do iNode = 1, 3
                        iP1 = iNode + 1; if ( iP1 > 3 ) iP1 = 1
                        iM1 = iNode - 1; if ( iM1 < 1 ) iM1 = 3

                        xP1 = nodeCoord(:,cellNodes(ip1,iCell))
                        xM1 = nodeCoord(:,cellNodes(im1,iCell))
                        xN = nodeCoord(:,cellNodes(iNode,iCell))
                        cellAngles(iNode,iCell) = &
                            acos( dot_product(xP1-xN,xM1-xN) / &
                                  ( faceArea(cellFaces(iM1,iCell)) * &
                                    faceArea(cellFaces(iP1,iCell)) ) )
                    end do
#ifdef VERBOSE
                    if ( abs(sum(cellAngles(:,iCell))-pi) > 4.0*eps ) then
                        write(*,*) 'ERROR: cell angle sum incorrect.'
                        write(*,*)  iCell, abs(sum(cellAngles(:,iCell))-pi) 
                        stop
                    end if
#endif
                case (4) 
                    do iNode = 1, 4
                        iP1 = iNode + 1; if ( iP1 > 4 ) iP1 = 1
                        iM1 = iNode - 1; if ( iM1 < 1 ) iM1 = 4

                        xP1 = nodeCoord(:,cellNodes(ip1,iCell))
                        xM1 = nodeCoord(:,cellNodes(im1,iCell))
                        xN = nodeCoord(:,cellNodes(iNode,iCell))
                        cellAngles(iNode,iCell) = &
                            acos( dot_product(xP1-xN,xM1-xN) / &
                                  ( faceArea(cellFaces(iM1,iCell)) * &
                                    faceArea(cellFaces(iP1,iCell)) ) )
                    end do
#ifdef VERBOSE
                    if ( abs(sum(cellAngles(:,iCell))-2.0_FP*pi) > 4.0*eps ) then
                        write(*,*) 'ERROR: cell angle sum incorrect.'
                        write(*,*) iCell, abs(sum(cellAngles(:,iCell))-2.0_FP*pi) 
                        stop
                    end if
#endif
            end select
        end if
    end do

    ! node and edge coordinates in reference space
    select case ( nDim )
        case (1)
            xiNode(:,1) = 0.0_FP
            xiNode(:,2) = 1.0_FP

            ! set the same as xiNode, in 1D xiNode = xiEdge
            xiEdge(:,1) = 0.0_FP
            xiEdge(:,2) = 1.0_FP

        case (2)

            select case ( maxFacesPerCell )

                case ( 3 ) 
                    xiNode(:,1) = (/0.0_FP, 0.0_FP/)
                    xiNode(:,2) = (/1.0_FP, 0.0_FP/)
                    xiNode(:,3) = (/0.0_FP, 1.0_FP/)

                    xiEdge(:,3) = (/0.5_FP, 0.0_FP/)
                    xiEdge(:,1) = (/0.5_FP, 0.5_FP/)
                    xiEdge(:,2) = (/0.0_FP, 0.5_FP/)

                case ( 4 )
                    ! quadrilateral
                    xiNode(:,1) = (/0.0_FP, 0.0_FP/)
                    xiNode(:,2) = (/1.0_FP, 0.0_FP/)
                    xiNode(:,3) = (/1.0_FP, 1.0_FP/)
                    xiNode(:,4) = (/0.0_FP, 1.0_FP/)

                    xiEdge(:,1) = (/0.5_FP, 0.0_FP/)
                    xiEdge(:,2) = (/1.0_FP, 0.5_FP/)
                    xiEdge(:,3) = (/0.5_FP, 1.0_FP/)
                    xiEdge(:,4) = (/0.0_FP, 0.5_FP/)

            end select

    end select

end subroutine connect
!!-------------------------------------------------------------------------------
!!> @purpose 
!!>  Fill connectivity (mapping) arrays and calculate mesh geometry
!!>
!!> @author
!!>  Timothy A. Eymann
!!>
!!> @history
!!>  8 March 2012 - Initial Creation
!!>  8 May 2012 - Add 1-D connectivity
!!>  8 May 2015 - Added acNodeCells to be used in acoustic solver (Maeng)
!!>  10 May 2015 - Added acEdgeCells acFaceCells (Maeng)
!!>
!subroutine connect
!
!    use solverVars, only: iLeft, iRight, dtIn, pi, eps, &
!                          truncDecPts, SHORTINT, LONGINT
!    use mathutil, only: pointInTri
!#ifdef FORT2K8
!    use mathutil, only: norm2
!#endif
!    implicit none
!
!    ! Local variables
!    integer :: iCell,   & !< cell index
!               jCell,   & !< cell index
!               iFace,   & !< face index
!               jFace,   & !< face index
!               iNode,   & !< node index
!               jNode,   & !< node index
!               node1,   &
!               node2,   &
!               node3,   & !< ordered nodes
!               face1,   &
!               face2,   &
!               face3,   & !< ordered faces
!               tFace,   & !< temporary face storage
!               iDir,    & !< direction index
!               iSide ,  & !< left/right index
!               iP1,     & !< i+1 index
!               iM1       !< i-1 index
!
!    real(FP) :: coordSum,           & !< sum of coordinate values (for average)
!                xN(nDim),           & !< 
!                xVert(nDim,maxNodesPerCell), &
!                xP1(nDim),          & !< x(i+1)
!                xM1(nDim),          & !< x(i-1)
!                x(maxNodesPerCell), & !< x-coordinates of cell nodes
!                y(maxNodesPerCell), & !< y-coordinates of cell nodes
!                xDiff(nDim),        & !< difference in node coordinates
!                tVec(nDim)            !< tangential vector
!
!    real(FP), parameter :: ds = 1.0e-13_FP ! slightly positive number
!    real(FP), parameter :: coordTol = 1.0e-13_FP ! geometric coordinate tolerance
!
!    totalVolume = 0.0_FP
!    xMax(:) = -1.0e6_FP
!    xMin(:) = 1.0e6_FP
!
!    ! fill cell to face mapping array ( not ordered )
!    do iFace = 1, nFaces
!        do iSide = iLeft, iRight
!            jCell = faceCells(iSide,iFace)
!
!            ! only operate on physical cells
!            if ( jCell > 0 ) then
!                if ( cellFaces(1,jCell) == 0 ) then
!                    cellFaces(1,jCell) = iFace
!                else
!                    ! find next open spot in array
!                    jFace = 2
!                    do while ( abs(cellFaces(jFace,jCell)) > 0 ) 
!                        jFace = jFace + 1
!                    end do
!                    cellFaces(jFace,jCell) = iFace
!                end if
!            end if
!        end do
!    end do
!
!    ! loop over cells to order nodes, faces, and calculate Jacobian
!    select case ( nDim )
!
!        case (1)
!
!            do iCell = 1, nCells
!                do iNode = 1, maxNodesPerCell
!                    cellNodes(iNode,iCell) = iCell + (iNode-1)
!                    cellFaces(iNode,iCell) = iCell + (iNode-1)
!                end do
!                
!                node1 = iCell
!                node2 = iCell+1
!                x(1) = nodeCoord(1,node1)
!                x(2) = nodeCoord(1,node2)
!                
!                cellJac(1,1,iCell) = x(2) - x(1)
!                cellDetJac(iCell) = cellJac(1,1,iCell)
!                cellInvJac(1,1,iCell) = 1.0_FP/cellJac(1,1,iCell)
!                cellVolume(iCell) = cellJac(1,1,iCell)
!                cellCentroid(:,iCell) = 0.5_FP*sum(x(:))
!
!                ! store grid extents
!                xMax(1) = max(xMax(1),maxVal(x(:)))
!                xMin(1) = min(xMin(1),minVal(x(:)))
!
!                ! calculate total volume of domain
!                totalVolume = totalVolume + cellVolume(iCell)
!            end do
!
!        case (2)
!
!            do iCell = 1, nCells
!              
!                ! assume cell nodes/faces are properly ordered
!                face1 = cellFaces(1,iCell)
!                node1 = faceNodes(1,face1)
!                node2 = faceNodes(2,face1)
!                
!                do iFace = 2,maxFacesPerCell
!                    if ((faceNodes(1,cellFaces(iFace,iCell))==node1).or. &
!                        (faceNodes(2,cellFaces(iFace,iCell))==node1)) then
!                        face3 = cellFaces(iFace,iCell)
!                        if ( faceNodes(1,face3) == node1 ) then
!                            node3 = faceNodes(2,face3)
!                        else
!                            node3 = faceNodes(1,face3)
!                        end if
!                    else
!                        face2 = cellFaces(iFace,iCell)
!                    end if
!                end do
! 
!                ! enforce ccw ordering 
!                if ( (nodeCoord(1,node2)-nodeCoord(1,node1)) * &
!                     (nodeCoord(2,node3)-nodeCoord(2,node1)) - &
!                     (nodeCoord(1,node3)-nodeCoord(1,node1)) * &
!                     (nodeCoord(2,node2)-nodeCoord(2,node1)) < 0.0_FP ) then
!                   
!                    ! use node 1 for temp storage to flip nodes 2,3
!                    node1 = node2
!                    node2 = node3
!                    node3 = node1
!                    node1 = faceNodes(1,face1)
!
!                    ! flip faces 1,3
!                    tFace = face1
!                    face1 = face3
!                    face3 = tFace
!                end if
!                cellNodes(:,iCell) = (/node1,node2,node3/)
!
!                ! order faces so that cellFaces(1,iCell) is opposite "node 1"
!                cellFaces(:,iCell) = (/face2,face3,face1/)
!
!                ! find Jacobian
!                x(:) = nodeCoord(1,cellNodes(:,iCell))
!                y(:) = nodeCoord(2,cellNodes(:,iCell))
!
!                ! shift the difference and round in order to reduce round
!                ! off error - I don't think this is a good idea
!                cellJac(1,1,iCell) = x(2) - x(1)
!                cellJac(1,2,iCell) = x(3) - x(1)
!                cellJac(2,1,iCell) = y(2) - y(1)
!                cellJac(2,2,iCell) = y(3) - y(1)
!                !cellDetJac(iCell) = (x(2)-x(1))*(y(3)-y(1)) - &
!                !                    (x(3)-x(1))*(y(2)-y(1))
!                cellDetJac(iCell) = (cellJac(1,1,iCell)*cellJac(2,2,iCell)) - &
!                                    (cellJac(1,2,iCell)*cellJac(2,1,iCell))
!
!                cellInvJac(1,1,iCell) = y(3)-y(1)
!                cellInvJac(1,2,iCell) = x(1)-x(3)
!                cellInvJac(2,1,iCell) = y(1)-y(2)
!                cellInvJac(2,2,iCell) = x(2)-x(1)
!                cellInvJac(:,:,iCell) = (1.0_FP/cellDetJac(iCell)) * &
!                                        cellInvJac(:,:,iCell)
!                !write(*,*) iCell, cellInvJac(1,1,iCell), cellInvJac(1,2,iCell), &
!                !    cellInvJac(2,1,iCell), cellInvJac(2,2,iCell)
!                !if ( iCell == 90 ) write(*,*) cellInvJac(1,:,iCell)
!
!                cellVolume(iCell) = 0.5_FP*cellDetJac(iCell)
!                cellCentroid(:,iCell) = 1.0_FP/3.0_FP*(/sum(x(:)),sum(y(:))/)
!
!                !write(*,*) cellVolume(iCell)
!                !if ( iCell == 22 .or. iCell == 24 ) then
!                !write(*,*) iCell, cellVolume(iCell), cellInvJac(:,:,iCell), cellDetJac(iCell)
!                !end if
!
!                ! store grid extents
!                xMax(1) = max(xMax(1),maxVal(x(:)))
!                xMin(1) = min(xMin(1),minVal(x(:)))
!                xMax(2) = max(xMax(2),maxVal(y(:)))
!                xMin(2) = min(xMin(2),minVal(y(:)))
!
!                ! check for global node and edge for the cell
!                !if ( iCell == 29 .or. iCell == 27 .or.  &
!                !    iCell == 19 .or. iCell == 23 ) then
!                !    write(*,*) 'cell: ', iCell
!                !    write(*,*) 'node: ', cellNodes(:,iCell)
!                !    write(*,*) 'edge: ', cellFaces(:,iCell)
!                !end if
!
!                ! calculate total volume of domain
!                totalVolume = totalVolume + cellVolume(iCell)
!
!            end do
!
!    end select
!
!    ! Loop over faces to calculate normals and coordinates
!    do iFace = 1, nFaces
!
!        ! calculate edge coordinate
!        do iDir = 1, nDim
!            coordSum = 0.0_FP
!            do iNode = 1, maxNodesPerFace
!                coordSum = coordSum + nodeCoord(iDir,faceNodes(iNode,iFace))
!            end do
!            ! in 1D and 2D, faces and edges are the same thing...
!            !edgeCoord(iDir,iFace) = coordSum / maxNodesPerFace
!            edgeCoord(iDir,iFace) = coordSum / real(maxNodesPerFace,FP)
!        end do
!
!        ! only need faceCells for 2d solver, but edge array needed for 3d
!        edgeCell(1,iFace) = faceCells(iLeft,iFace)
!
!        ! calculate face normals and face area
!        select case ( nDim )
!
!            case (1)
!                if ( iFace == 1 ) then
!                    faceNormal(:,iFace) = -1.0_FP
!                else
!                    faceNormal(:,iFace) = 1.0_FP
!                end if
!                faceArea(iFace) = 1.0_FP
!
!            case (2)
!                xDiff(:) = nodeCoord(:,faceNodes(2,iFace)) - &
!                           nodeCoord(:,faceNodes(1,iFace))
!                tVec(:) = xDiff(:)/norm2(xDiff(:))
!               
!                faceNormal(1,iFace) = -tVec(2)
!                faceNormal(2,iFace) =  tVec(1)
!
!                ! make sure normal points away from "left" cell
!                iCell = faceCells(iLeft,iFace)
!                xVert(1,:) = nodeCoord(1,cellNodes(:,iCell))
!                xVert(2,:) = nodeCoord(2,cellNodes(:,iCell))
!                !xN(:) = edgeCoord(:,iFace) - dtIn*faceNormal(:,iFace)
!                xN(:) = edgeCoord(:,iFace) - ds*faceNormal(:,iFace)
!                if ( .not. pointInTri(xN,xVert) ) then
!#ifdef VERBOSE
!                    write(*,'(a,i0)') 'WARNING: Flipping normal for face ',iFace
!#endif
!                    !write(*,'(a,i0)') 'WARNING: Flipping normal for face ',iFace
!                    faceNormal(:,iFace) = -1.0_FP*faceNormal(:,iFace)
!                end if
!
!                faceArea(iFace) = norm2(xDiff(:))
!        
!        end select
!
!    end do
!
!    ! node to cell mapping (needed to start neighbor search)
!    do iNode = 1, nNodes
!
!        ! find first cell containing node
!        do iCell = 1, nCells
!            do jNode = 1, maxNodesPerCell
!                if ( cellNodes(jNode,iCell) == iNode ) then
!                    nodeCell(1,iNode) = iCell
!                    exit
!                end if
!            end do
!            if ( nodeCell(1,iNode) /= 0 ) exit
!        end do
!
!    end do
!
!    inode = 0
!    do iCell = 1, nCells
!#ifdef VERBOSE
!        ! check for negative Jacobians ( clockwise ordering )
!        if ( cellDetJac(iCell) < 0 ) then
!            inode = inode + 1
!            write(*,*) ' cell: ',iCell
!            write(*,*) 'nodes: ',cellNodes(:,iCell)
!            write(*,*) 'faces: ',cellFaces(:,iCell)
!            read(*,*)
!        end if
!#endif
!        ! calculate angles in each cell
!        if ( nDim == 2 ) then
!            do iNode = 1, 3
!                iP1 = iNode + 1; if ( iP1 > 3 ) iP1 = 1
!                iM1 = iNode - 1; if ( iM1 < 1 ) iM1 = 3
!
!                xP1 = nodeCoord(:,cellNodes(ip1,iCell))
!                xM1 = nodeCoord(:,cellNodes(im1,iCell))
!                xN = nodeCoord(:,cellNodes(iNode,iCell))
!                cellAngles(iNode,iCell) = &
!                    acos( dot_product(xP1-xN,xM1-xN) / &
!                          ( faceArea(cellFaces(iM1,iCell)) * &
!                            faceArea(cellFaces(iP1,iCell)) ) )
!            end do
!#ifdef VERBOSE
!            if ( abs(sum(cellAngles(:,iCell))-pi) > 4.0*eps ) then
!                write(*,*) 'WARNING: cell angle sum incorrect.'
!                write(*,*)abs(sum(cellAngles(:,iCell))-pi) 
!            end if
!#endif
!        end if
!    end do
!
!    ! node and edge coordinates in reference space
!    select case ( nDim )
!        case (1)
!            xiNode(:,1) = 0.0_FP
!            xiNode(:,2) = 1.0_FP
!
!            ! set the same as xiNode, in 1D xiNode = xiEdge
!            xiEdge(:,1) = 0.0_FP
!            xiEdge(:,2) = 1.0_FP
!
!        case (2)
!            xiNode(:,1) = (/0.0_FP, 0.0_FP/)
!            xiNode(:,2) = (/1.0_FP, 0.0_FP/)
!            xiNode(:,3) = (/0.0_FP, 1.0_FP/)
!
!            xiEdge(:,3) = (/0.5_FP, 0.0_FP/)
!            xiEdge(:,1) = (/0.5_FP, 0.5_FP/)
!            xiEdge(:,2) = (/0.0_FP, 0.5_FP/)
!    end select
!
!end subroutine connect
!-------------------------------------------------------------------------------
!> @purpose 
!>  Evaluate face normal vector and face area for a specific face and cell
!>
!> @author
!>  J. Brad Maeng
!>
!> @history
!>  4 January 2016 - Initial creation
!>
subroutine faceNormalArea(nDim,iFace,iCell,fNormal,fArea)

    use solverVars, only: iLeft, iRight, dtIn, pi, eps
    use mathutil, only: pointInTri, pointInQuadril

    implicit none

    !> Interface variables
    integer :: nDim,    & !< problem dimension
               iFace,   & !< global face index
               iCell      !< global cell index

    real(FP), intent(out) :: fNormal(nDim),    & !< face normal vector
                             fArea               !< face area

    !> Local variables
    real(FP), parameter :: ds = 1.0e-13_FP ! slightly positive number

    real(FP) :: xN(nDim),                    & !< 
                xVert(nDim,maxNodesPerCell), &
                xDiff(nDim),                 & !< difference in node coordinates
                tVec(nDim)                     !< tangential vector

    ! calculate face normals and face area
    select case ( nDim )

        case (1)
            if ( iFace == 1 ) then
                fNormal(:) = -1.0_FP
            else
                fNormal(:) = 1.0_FP
            end if
            fArea = 1.0_FP

        case (2)
            xDiff(:) = nodeCoord(:,faceNodes(2,iFace)) - &
                       nodeCoord(:,faceNodes(1,iFace))
            tVec(:) = xDiff(:)/norm2(xDiff(:))
           
            fNormal(1) = -tVec(2)
            fNormal(2) =  tVec(1)

            ! make sure normal points away from "left" cell
            xVert(1,:) = nodeCoord(1,cellNodes(:,iCell))
            xVert(2,:) = nodeCoord(2,cellNodes(:,iCell))
            !xN(:) = edgeCoord(:,iFace) - dtIn*fNormal(:)
            xN(:) = edgeCoord(:,iFace) - ds*fNormal(:)
            
            select case ( maxFacesPerCell )

                case ( 3 ) ! triangle
                    if ( .not. pointInTri(xN,xVert) ) then
                        fNormal(:) = -1.0_FP*fNormal(:)
                    end if

                case ( 4 ) ! quadrilateral
                    if ( .not. pointInQuadril(xN,xVert) ) then
                        fNormal(:) = -1.0_FP*fNormal(:)
                    end if
            end select

            fArea = norm2(xDiff(:))
    
    end select
end subroutine faceNormalArea
!-------------------------------------------------------------------------------
!> @purpose 
!>  Find node angle
!>
!> @author
!>  J. Brad Maeng
!>
!> @history
!>  26 August 2016 - Initial Creation
!>
function nodeAngle(nDim,iNode,iCell)

    implicit none

    ! Interface variables
    integer, intent(in) :: nDim,    & !< problem dimension
                           iNode,   & !< local node index
                           iCell      !< cell index

    ! Function output
    real(FP) :: nodeAngle
    
    ! Local variables
    integer :: iEdge,   & !< edge index
               lEdge,   & !< edge index
               rEdge    

    if ( nDim < 2 ) then
        write(*,*) 'ERROR: no angles for 1 dimensional problem'
        stop
    end if

    ! local edge index
    iEdge = iNode
    lEdge = mod(iNode+1,3)
    if ( lEdge == 0 ) lEdge = 3
    rEdge = mod(iNode+2,3)
    if ( rEdge == 0 ) rEdge = 3
    
    ! node angle
    nodeAngle = acos( (faceArea(cellFaces(lEdge,iCell))**2.0_FP + &
                       faceArea(cellFaces(rEdge,iCell))**2.0_FP - &
                       faceArea(cellFaces(iEdge,iCell))**2.0_FP)  / &
        (2.0_FP*faceArea(cellFaces(lEdge,iCell))*faceArea(cellFaces(rEdge,iCell))) )

end function nodeAngle
!-------------------------------------------------------------------------------
!> @purpose 
!>  Convert reference coordinates to Cartesian coordinates
!>
!> @author
!>  Timothy A. Eymann
!>
!> @history
!>  26 March 2012 - Initial Creation
!>
function ref2cart(nDim,iCell,xi)

    implicit none
    
    ! Interface variables
    integer, intent(in) :: nDim,    & !< simulation dimension
                           iCell      !< cell index

    real(FP), intent(in) :: xi(nDim) !< reference coordinates

    ! Function variable
    real(FP) :: ref2cart(nDim)

    ! Local variables
    integer :: j !< column index

    real(FP) :: Jcol(nDim), & !< column of cell Jacobian
                Jprod(nDim)   !< product of Jacobian and reference vector
    

    Jprod = 0.0_FP
    do j = 1, nDim
        Jcol(:) = cellJac(:,j,iCell)
        Jprod(:) = Jprod(:) + xi(j)*Jcol(:)
    end do

    ref2cart(:) = nodeCoord(:,abs(cellNodes(1,iCell))) + Jprod(:)

end function ref2cart
!-------------------------------------------------------------------------------
!> @purpose 
!>  Convert Cartesian coordinates to reference coordinates
!>
!> @author
!>  Timothy A. Eymann
!>
!> @history
!>  26 March 2012 - Initial Creation
!>
function cart2ref(nDim,iCell,x)

    implicit none
    
    ! Interface variables
    integer, intent(in) :: nDim,    & !< simulation dimension
                           iCell      !< cell index

    real(FP), intent(in) :: x(nDim) !< reference coordinates

    ! Function variable
    real(FP) :: cart2ref(nDim)

    ! Local variables
    integer :: j !< column index

    real(FP) :: xDiff(nDim),    & !< difference of Cartesian coordinates
                Jcol(nDim),     & !< column of cell inverse Jacobian
                Jprod(nDim)       !< product of inverse Jacobian and Cart. vector
 
    xDiff(:) = x(:) - nodeCoord(:,abs(cellNodes(1,iCell)))

    Jprod = 0.0_FP
    do j = 1, nDim
        Jcol(:) = cellInvJac(:,j,iCell)
        Jprod(:) = Jprod(:) + xDiff(j)*Jcol(:)
    end do

    cart2ref(:) = Jprod(:)

end function cart2ref
!-------------------------------------------------------------------------------
!> @purpose 
!>  Find global minimum distance for time step constraint
!>
!> @author
!>  J. Brad Maeng
!>
!> @history
!>  27 April 2016 - Initial creation
!>
subroutine minDistance(minLen)
    
    implicit none

    !< Interface variable
    real(FP), intent(out) :: minLen

    !< Local variable
    integer :: iCell,   & !< cell index
               iEdge,   & !< edge index
               gEdge      !< global edge index

    minLen = 1.0e06

    select case ( nDim )

        case ( 1 )
            do iCell = 1,nCells
                minLen = min( minLen, cellVolume(iCell) )
            end do

        case ( 2 )
            ! TODO: maybe a second look?
            select case ( maxFacesPerCell )

              case ( 3 ) ! triangle
                do iCell = 1,nCells
                    do iEdge = 1, maxFacesPerCell
                        gEdge = cellFaces(iEdge,iCell)
                        minLen = min( minLen, cellVolume(iCell)/faceArea(gEdge) )
                    end do
                end do

              case ( 4 ) ! quadrilateral
                do iCell = 1,nCells
                    do iEdge = 1, maxFacesPerCell
                        gEdge = cellFaces(iEdge,iCell)
                        minLen = min( minLen, 0.5_FP*cellVolume(iCell)/faceArea(gEdge) )
                    end do
                end do

            end select

        case default
            write(*,*) 'Error: 3D not implemented'
            stop

    end select

end subroutine minDistance
!-------------------------------------------------------------------------------
!> @purpose 
!>  Find cell containing Cartesian coordinate.
!>
!> @author
!>  Timothy A. Eymann
!>
!> @history
!>  26 March 2012 - Initial Creation\n
!>  8 May 2012 - Extend to 1-D cases
!>
recursive function findCell(nDim,iCell,x) result (cellIndex)

    use solverVars, only : iLeft, iRight
    implicit none

    ! Interface variables
    integer, intent(in) :: nDim,    & !< dimensions
                           iCell      !< starting point of neighbor search

    real(FP), intent(in) :: x(nDim) !< coordinate

    ! Function variable
    integer :: cellIndex

    ! Local variables
    integer :: jCell,   & !< next cell to search
               iFace      !< reference face to cross for neighbor walk

    real(FP) :: xi(nDim) !< search point in reference coordinates
  

    xi(:) = cart2ref(nDim,iCell,x)
   
    select case ( nDim )

        case (1)
            if ( ( xi(1) >= 0.0_FP ) .and. ( xi(1) <= 1.0_FP ) ) then
                cellIndex = iCell
                return
            elseif ( xi(1) < 0.0_FP ) then
                iFace = 1
            elseif ( xi(1) > 1.0_FP ) then
                iFace = 2
            else
                write(*,*) 'ERROR: problem with neighbor walk logic.'
                stop
            end if

        case (2)
            if ( (( xi(2) <= 1.0_FP ) .and. ( xi(2) >= 0.0_FP )) .and. &
                 (( xi(1) >= 0.0_FP ) .and. ( xi(1) <= 1.0_FP-xi(2))) ) then 
                
                ! coordinate is inside cell
                cellIndex = iCell
                return

            elseif ( xi(2) < 0.0_FP ) then ! look across face 3
                iFace = 3
            
            elseif ( xi(1) > 1.0_FP - xi(2) ) then ! look across face 1
                iFace = 1

            elseif ( xi(1) < 0.0_FP ) then ! look across face 2
                iFace = 2

            else
                write(*,*) 'ERROR: problem with neighbor walk logic.'
                stop
            end if
    end select

    ! move to neighbor cell
    if ( faceCells(iLeft,cellFaces(iFace,iCell)) == iCell ) then
        jCell = faceCells(iRight,abs(cellFaces(iFace,iCell)))
    else    
        jCell = faceCells(iLeft,abs(cellFaces(iFace,iCell)))
    end if

    if ( jCell <= 0 ) then
        write(*,*) 'Negative or zero cell index encountered in neighbor walk...'
        write(*,*) 'jCell: ',jCell
        write(*,*) 'iCell: ',iCell
        write(*,*)
        write(*,*) '  cell face: ',iFace
        write(*,*) 'global face: ',cellFaces(iFace,iCell)
        write(*,*) '     neighs: ',faceCells(:,cellFaces(iFace,iCell))
        read(*,*)
    end if
    cellIndex = findCell(nDim,jCell,x)

end function findCell
!-------------------------------------------------------------------------------
end module meshUtil
