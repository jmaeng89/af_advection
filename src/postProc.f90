!-------------------------------------------------------------------------------
!> @purpose 
!>  Common post-processing routines
!>
!> @author
!>  Timothy A. Eymann
!>
!> @history
!>  20 September 2016 - Integrate all advective systems (Maeng)
!>
module postProc

    use solverVars, only: FP

contains
!-------------------------------------------------------------------------------
!> @purpose 
!>  Gather data at reconstruction coefficient points for each cell
!>
!> @author
!>  J. Brad Maeng
!>
!> @history
!>  10 February 2017 - Initial creation
!>
subroutine recCoefData(nDim,nEqns,nPts,nCells,interData)

    use reconstruction, only: reconValue
    use update, only: updateReconData
    use meshUtil, only: cellNodes, cellFaces, cellInvJac,&
                        maxFacesPercell
    use mathUtil, only: gaussQuadLoc, symTriLoc
    implicit none

    ! Interface variables
    integer, intent(in) :: nDim,    & !< problem dimension
                           nEqns,   & !< number of equations
                           nPts,    & !< number of integration points
                           nCells     !< number of cells

    real(FP), intent(out) :: interData(:,:,:)
  
    ! Local variables
    integer :: iEq,     & !< equation index
               iCell,   & !< cell index
               iPt,     & !< integration point
               jPt        !< integration point

    real(FP) :: xi(nDim,nPts),      & !< evaluation coordinates (in reference space)
                xiTemp(nDim)

    ! use latest values
    call updateReconData()

    ! hardcode for single equation
    do iEq = 1, nEqns

        select case ( maxFacesPerCell )

          case ( 3 )  ! triangle
            ! set locations
            xi(:,:) = symTriLoc(nPts)
            do iCell = 1, nCells
                ! natural reconstruction, with bubble function 
                do iPt = 1, nPts
                    interData(iEq,iPt,iCell) = &
                        reconValue(nDim,iEq,iCell,xi(:,iPt))
                end do
            end do

          case ( 4 )  ! quadrilateral
            xi(1,:) = gaussQuadLoc(nPts)
            do iCell = 1, nCells
                ! natural reconstruction, with bubble function 
                do jPt = 1, nPts
                    do iPt = 1, nPts
                        xiTemp(1) = 0.5_FP*(xi(1,iPt) + 1.0_FP)
                        xiTemp(2) = 0.5_FP*(xi(1,jPt) + 1.0_FP)
                        interData(iEq,jPt+nPts*(iPt-1),iCell) = &
                            reconValue(nDim,iEq,iCell,xiTemp)
                    end do
                end do
            end do

        end select
    end do

end subroutine recCoefData
!-------------------------------------------------------------------------------
!> @purpose 
!>  Gather data at Dunavant points for each cell
!>
!> @author
!>  Timothy A. Eymann
!>
!> @history
!>  19 April 2012 - Initial creation
!>  3 Sep. 2015 - Included updateRecondata for latest reconstruction 
!>                                                      values (Maeng)
!>
subroutine intData(nDim,nEqns,nPts,nCells,interData,nBasis)

    use reconstruction, only: reconValue
    use update, only: updateReconData
    use meshUtil, only: cellNodes, cellFaces, cellInvJac,&
                        maxFacesPercell
    use mathUtil, only: dunavantLoc, gaussQuadLoc, &
                        symTriLoc
    implicit none

    ! Interface variables
    integer, intent(in) :: nDim,    & !< problem dimension
                           nEqns,   & !< number of equations
                           nPts,    & !< number of integration points
                           nCells     !< number of cells

    integer, intent(in), optional :: nBasis !< number of basis functions

    real(FP), intent(out) :: interData(:,:,:)
  
    ! Local variables
    integer :: iEq,     & !< equation index
               iCell,   & !< cell index
               iPt,     & !< integration point
               jPt        !< integration point

    real(FP) :: xi(nDim,nPts),      & !< evaluation coordinates (in reference space)
                xiTemp(nDim)

    real(FP) :: cIn((nDim+2)*(nDim+3)/(2*(3-nDim))) !< reconstruction coeff

    ! use latest values
    call updateReconData()

    ! hardcode for single equation
    do iEq = 1, nEqns

        !! if basis functions are specified, we're working with 2d xflow file
        !if ( present(nBasis) ) then
        !    !xi(:,:) = dunavantLoc(nPts)
        !    do iCell = 1, nCells
        !        do iPoint = 1, nPts
        !            intData(iEq,iPoint,iCell) = &
        !                xReconValue(nDim,nBasis,iEq,iCell,xi(:,iPoint))
        !        end do
        !    end do
        !    cycle ! fix this
        !end if
        
        ! set locations
        select case ( nDim )
          case ( 1 )
            xi(1,:) = gaussQuadLoc(nPts)
            do iCell = 1, nCells
                ! assume natural reconstruction
                do iPt = 1, nPts
                    ! convert Gaussian quadrature location to interval from [0,1]
                    ! This is VERY important
                    ! Fortran doesn't do overwrite to variables well so
                    ! it is important to assign value to another variable
                    xiTemp(1) = 0.5*(xi(1,iPt) + 1.0)
                    interData(iEq,iPt,iCell) = &
                            reconValue(nDim,iEq,iCell,xiTemp)
                end do
            end do

          case ( 2 )
            ! 2d quadrature inside element
            select case ( maxFacesPerCell )
              case ( 3 )  ! triangle
                xi(:,:) = dunavantLoc(nPts)
                do iCell = 1, nCells
                    ! natural reconstruction, with bubble function 
                    do iPt = 1, nPts
                        interData(iEq,iPt,iCell) = &
                            reconValue(nDim,iEq,iCell,xi(:,iPt))
                    end do
                end do

              case ( 4 )  ! quadrilateral
                xi(1,:) = gaussQuadLoc(nPts)
                do iCell = 1, nCells
                    ! natural reconstruction, with bubble function 
                    do jPt = 1, nPts
                        do iPt = 1, nPts
                    ! This is VERY important
                    ! Fortran doesn't do overwrite to variables well so
                    ! it is important to assign value to another variable
                            xiTemp(1) = 0.5_FP*(xi(1,iPt) + 1.0_FP)
                            xiTemp(2) = 0.5_FP*(xi(1,jPt) + 1.0_FP)
                            interData(iEq,jPt+nPts*(iPt-1),iCell) = &
                                reconValue(nDim,iEq,iCell,xiTemp)
                        end do
                    end do
                end do

            end select
        end select
    end do

end subroutine intData
!-------------------------------------------------------------------------------
!> @purpose 
!>  Compute error norm between numerical cell average and "exact" cell average
!>
!> @author
!>  J. Brad Maeng
!>
!> @history
!>  18 November 2015 - Initial creation
!>
function cellAvgError(nDim,nEqns,nCells,cellAvg0,cellAvg,norm)

    use meshUtil, only: cellDetJac, cellVolume, faceArea, cellFaces, &
                        cellCentroid
    implicit none

    ! Interface variables
    integer, intent(in) :: nDim,    & !< dimension number
                           nEqns,   & !< number of equations
                           nCells     !< number of cells

    real(FP), intent(in) :: cellAvg0(nEqns,nCells),& !< numerical
                            cellAvg(nEqns,nCells), & !< exact data
                            norm                     !< error norm

    ! Function variable
    real(FP) :: cellAvgError(nEqns)

    ! Local variables
    integer :: iCell,   & !< cell index
               iEq        !< equation index

    real(FP) :: area,       & !< domain area
                quad,       & !< quadrature sum
                errSum,     & !< sum of error terms
                x,          & !< centroid x coordinate
                y,          & !< centroid y coordinate
                r

    do iEq = 1, nEqns

        errSum = 0.0_FP
        area = 0.0_FP
        ! error norm
        select case ( nDim )
          case ( 1 ) 

            do iCell = 1, nCells

                x = cellCentroid(1,iCell)

                ! to truncate outside domain,
                !if ( abs(x) <= 0.8_FP ) then
                    errSum = errSum + cellVolume(iCell)* &
                            abs(cellAvg(iEq,iCell)-cellAvg0(iEq,iCell))**norm
                    area = area + cellVolume(iCell)
                !end if

            end do

          case ( 2 )

            do iCell = 1, nCells

                x = cellCentroid(1,iCell)
                y = cellCentroid(2,iCell)
                ! radial position
                r = sqrt(x**2.0_FP + y**2.0_FP)

                ! to truncate outside domain,
                !if ( r <= 4.0_FP ) then
                    errSum = errSum + cellVolume(iCell)* &
                             abs(cellAvg(iEq,iCell)-cellAvg0(iEq,iCell))**norm
                    area = area + cellVolume(iCell)
                !end if
            end do
            
        end select

        cellAvgError(iEq) = (errSum/area)**(1.0_FP/norm)

    end do

end function cellAvgError
!-------------------------------------------------------------------------------
!> @purpose 
!>  Compute error norm between cell reconstructions at first and final iteration 
!>
!> @author
!>  Timothy A. Eymann
!>
!> @history
!>  19 April 2012 - Initial creation
!>  30 July 2012 - Add variable norm
!>  26 October 2012 - Add multiple equations
!>  27 April 2016 - reconstruction coefficients included (Maeng)
!>
function cellReconError(nDim,nEqns,nPts,nCells,intData0,intData,norm,nBasis)

    use meshUtil, only: cellDetJac, cellVolume, faceArea, cellFaces, &
                        cellCentroid, maxFacesPerCell
    use mathUtil, only: dunavantWt, gaussQuadWt, symTriWt, lagrange1dWt
    implicit none

    ! Interface variables
    integer, intent(in) :: nDim,    & !< dimension number
                           nEqns,   & !< number of equations
                           nPts,    & !< quadrature points
                           nCells     !< number of cells

    integer, intent(in), optional :: nBasis !< number of basis functions

    real(FP), intent(in) :: intData0(:,:,:),          & !< initial data
                            intData(:,:,:),           & !< final data
                            norm                        !< error norm

    ! Function variable
    real(FP) :: cellReconError(nEqns)

    ! Local variables
    integer :: iCell,   & !< cell index
               iPt,     & !< point index
               jPt,     & !< point index
               iEq        !< equation index

    real(FP) :: area,       & !< domain area
                quad,       & !< quadrature sum
                errSum,     & !< sum of error terms
                w(nPts),    & !< weights
                x,          & !< centroid x coordinate
                y,          & !< centroid y coordinate
                r

    real(FP) :: factor 

    do iEq = 1, nEqns

        errSum = 0.0_FP
        area = 0.0_FP
        ! error norm
        select case ( nDim )

          case ( 1 )

            if ( present(nBasis) ) then
                ! 2nd order integration weights
                w(:) = lagrange1dWt(nPts)
                factor = 1.0_FP
            else
                w(:) = gaussQuadWt(nPts)
                factor = 0.5_FP
            end if

            do iCell = 1, nCells
                x = cellCentroid(1,iCell)
                ! to truncate outside domain,
                !if ( abs(x) <= 0.8_FP ) then
                    quad = 0.0_FP
                    do iPt = 1, nPts
                        quad = quad + w(iPt) * &
                               abs(intData(iEq,iPt,iCell) - &
                                   intData0(iEq,iPt,iCell))**norm
                    end do
                    ! factor of 1/2 accounts for interval shift
                    ! Integration normalization factor is 0.5
                    errSum = errSum + cellVolume(iCell)*(factor*quad)
                    area = area + cellVolume(iCell)
                !end if
            end do

          case ( 2 )

            select case ( maxFacesPercell )

              case ( 3 )  ! triangle

                if ( present(nBasis) ) then
                    ! 2nd order integration weights
                    w(:) = symTriWt(nPts)
                else
                    ! 2d quadrature inside element
                    w(:) = dunavantWt(nPts)
                end if

                do iCell = 1, nCells
                    x = cellCentroid(1,iCell)
                    y = cellCentroid(2,iCell)
                    r = sqrt(x**2.0_FP + y**2.0_FP)

                    ! to truncate outside domain,
                    !if ( r <= 4.0_FP ) then
                        quad = 0.0_FP
                        do iPt = 1, nPts
                            quad = quad + w(iPt)* &
                                   abs(intData(iEq,iPt,iCell) - &
                                       intData0(iEq,iPt,iCell))**norm
                        end do
                        errSum = errSum + cellVolume(iCell)*(quad)
                        area = area + cellVolume(iCell)
                    !end if
                end do

              case ( 4 ) ! quadrilateral

                w(:) = gaussQuadWt(nPts)
                do iCell = 1, nCells
                    x = cellCentroid(1,iCell)
                    y = cellCentroid(2,iCell)
                    r = sqrt(x**2.0_FP + y**2.0_FP)

                    ! to truncate outside domain,
                    !if ( r <= 4.0_FP ) then
                        quad = 0.0_FP
                        do jPt = 1, nPts
                            do iPt = 1, nPts
                                quad = quad + 0.5_FP*w(iPt)*0.5_FP*w(jPt)*&
                                   abs(intData(iEq,jPt+nPts*(iPt-1),iCell) - &
                                       intData0(iEq,jPt+nPts*(iPt-1),iCell))**norm
                            end do
                        end do
                        errSum = errSum + cellVolume(iCell)*(quad)
                        area = area + cellVolume(iCell)
                    !end if
                end do

            end select

        end select

        cellReconError(iEq) = (errSum/area)**(1.0_FP/norm)

    end do

end function cellReconError
!-------------------------------------------------------------------------------
!> @purpose 
!>  Compute error norm between cell reconstruction coefficients at 
!>  first and final iteration 
!>
!> @author
!>  J. Brad Maeng
!>
!> @history
!>  2 October 2017 - Initial creation
!>
function cellRecCoefError(nDim,nEqns,nPts,nCells,recCoefData0,recCoefData,norm)

    use meshUtil, only: cellDetJac, cellVolume, faceArea, cellFaces, &
                        cellCentroid, maxFacesPerCell
    use mathUtil, only: dunavantWt, gaussQuadWt, symTriWt, lagrange1dWt
    implicit none

    ! Interface variables
    integer, intent(in) :: nDim,    & !< dimension number
                           nEqns,   & !< number of equations
                           nPts,    & !< quadrature points
                           nCells     !< number of cells

    real(FP), intent(in) :: recCoefData0(:,:,:),          & !< initial data
                            recCoefData(:,:,:),           & !< final data
                            norm                            !< error norm

    ! Function variable
    real(FP) :: cellRecCoefError(nEqns)

    ! Local variables
    integer :: iCell,   & !< cell index
               iPt,     & !< point index
               jPt,     & !< point index
               iEq        !< equation index

    real(FP) :: area,       & !< domain area
                quad,       & !< quadrature sum
                errSum,     & !< sum of error terms
                w(nPts),    & !< weights
                x,          & !< centroid x coordinate
                y,          & !< centroid y coordinate
                r

    real(FP) :: factor 

    do iEq = 1, nEqns

        errSum = 0.0_FP
        area = 0.0_FP
        ! error norm
        select case ( maxFacesPercell )

          case ( 3 )  ! triangle

            ! 2nd order integration weights
            w(:) = symTriWt(nPts)

            do iCell = 1, nCells
                x = cellCentroid(1,iCell)
                y = cellCentroid(2,iCell)
                r = sqrt(x**2.0_FP + y**2.0_FP)

                ! to truncate outside domain,
                !if ( r <= 4.0_FP ) then
                    quad = 0.0_FP
                    do iPt = 1, nPts
                        quad = quad + w(iPt)* &
                               abs(recCoefData(iEq,iPt,iCell) - &
                                   recCoefData0(iEq,iPt,iCell))**norm
                    end do
                    errSum = errSum + cellVolume(iCell)*(quad)
                    area = area + cellVolume(iCell)
                !end if
            end do

          case ( 4 ) ! quadrilateral

            w(:) = gaussQuadWt(nPts)
            do iCell = 1, nCells
                x = cellCentroid(1,iCell)
                y = cellCentroid(2,iCell)
                r = sqrt(x**2.0_FP + y**2.0_FP)

                ! to truncate outside domain,
                !if ( r <= 4.0_FP ) then
                    quad = 0.0_FP
                    do jPt = 1, nPts
                        do iPt = 1, nPts
                            quad = quad + 0.5_FP*w(iPt)*0.5_FP*w(jPt)*&
                               abs(recCoefData(iEq,jPt+nPts*(iPt-1),iCell) - &
                                   recCoefData0(iEq,jPt+nPts*(iPt-1),iCell))**norm
                        end do
                    end do
                    errSum = errSum + cellVolume(iCell)*(quad)
                    area = area + cellVolume(iCell)
                !end if
            end do

        end select

        cellRecCoefError(iEq) = (errSum/area)**(1.0_FP/norm)

    end do

end function cellRecCoefError
!-------------------------------------------------------------------------------
!> @purpose 
!>  Return numerical solution averaged over specified domain
!>
!> @author
!>  J. Brad Maeng
!>
!> @history
!>  22 March 2016 - Initial creation
!>  14 September 2016 - Modified a bit for nonlinear acoustic error estimation
!>
function cellReconAvgVal(nDim,nEqns,nPts,nCells,intData,norm,nBasis)

    use meshUtil, only: cellDetJac, cellVolume, faceArea, cellFaces, &
                        cellCentroid, ref2cart
    use mathUtil, only: dunavantWt, gaussQuadWt, symTriWt, &
                        dunavantLoc, symTriLoc
    implicit none

    ! Interface variables
    integer, intent(in) :: nDim,    & !< dimension number
                           nEqns,   & !< number of equations
                           nCells,  & !< number of cells
                           nPts       !< number of integration points

    integer, intent(in), optional :: nBasis !< number of basis functions

    real(FP), intent(in) :: intData(nEqns,nPts,nCells), & !< final data
                            norm                          !< error norm

    ! Function variable
    real(FP) :: cellReconAvgVal(nEqns)

    ! Local variables
    integer :: iCell,   & !< cell index
               iPt,     & !< point index
               iEq        !< equation index

    real(FP) :: area,       & !< domain area
                quad,       & !< quadrature sum
                errSum,     & !< sum of error terms
                w(nPts),    & !< quadrature weights
                x,          & !< centroid x coordinate
                y,          & !< centroid y coordinate
                r

    real(FP) :: gaussFunc,       & 
                xi(nDim,nPts),   & !< quadrature weights
                xcart(nDim)

    do iEq = 1, nEqns

        errSum = 0.0_FP
        area = 0.0_FP

        ! 2d quadrature inside element
        if ( present(nBasis) ) then
            w(:) = symTriWt(nPts)
            xi(:,:) = symTriLoc(nPts)
        else
            w(:) = dunavantWt(nPts)
            xi(:,:) = dunavantLoc(nPts)
        end if

        do iCell = 1, nCells
            !x = cellCentroid(1,iCell)
            !y = cellCentroid(2,iCell)
            !r = sqrt(x**2.0_FP + y**2.0_FP)
            
            ! to truncate outside domain,
            !if ( x >= 0.0_FP .and. x <= 1.0_FP ) then
            !if ( r <= 0.5_FP ) then

            quad = 0.0_FP
            do iPt = 1, nPts
                ! for nonlinear acoustic error estimation
                xCart = ref2cart(nDim,iCell,xi(:,iPt))
                gaussFunc = exp(-0.5_FP*(xCart(1)**2.0_FP + xCart(2)**2.0_FP)) 
                ! for nonlinear acoustic error estimation

                quad = quad + gaussFunc* &
                       abs(intData(iEq,iPt,iCell))**norm

                !quad = quad + w(iPt) * &
                !       abs(intData(iEq,iPt,iCell))**norm

            end do
            errSum = errSum + cellVolume(iCell)*(1.0_FP*quad)  
            area = area + cellVolume(iCell)
            !end if

        end do

        cellReconAvgVal(iEq) = (errSum/area)**(1.0_FP/norm)
    end do

end function cellReconAvgVal
!-------------------------------------------------------------------------------
!> @purpose 
!>  Solve nonlinear equations iteratively for characteristic origins/velocities 
!>
!> @author
!>  J. Brad Maeng
!>
!> @history
!>  31 August 2015 - Initial creation
!>
function iterateSol(nDim,nEqns,nPts,nCells,finalSol,tf,nBasis)

    use meshUtil, only: ref2cart, maxFacesPercell
    use mathUtil, only: dunavantLoc, gaussQuadLoc, symTriLoc, lagrange1dLoc
    use analyticFunctions, only: evalFunction
    use update, only: eqFlag, LINADV_EQ, PLESSEULER_EQ, BURGERS_EQ
    use iterativeFunctions

    implicit none
    
    ! Interface variables
    integer, intent(in) :: nDim,    & !< problem dimension
                           nEqns,   & !< number of equations
                           nPts,    & !< number of integration points
                           nCells     !< number of cells

    integer, intent(in), optional :: nBasis !< number of basis functions

    real(FP), intent(in) :: finalSol(:,:,:),    & !< final solution from simulation 
                            tf                    !< final time

    ! Function variable
    real(FP) :: iterateSol(nEqns,size(finalSol,2),nCells)
  
    ! Local variables
    integer :: iEq,     & !< equation index
               iDir,    & !< dimension index
               iCell,   & !< cell index
               iPt,     & !< integration index
               jPt        !< integration point

    real(FP) :: xi(nDim,nPts),  & !< reference coorindate
                xCart(nDim),    & !< physical coordinate
                xiTemp(nDim)

    select case ( eqFlag )

      case ( LINADV_EQ ) 
          ! iterate for solutions
        if ( nDim == 1 ) then
            if ( present(nBasis) ) then
                xi(1,:) = lagrange1dLoc(nPts)
            else
                xi(1,:) = gaussQuadLoc(nPts)
            end if
            ! convert Gaussian quadrature location to interval from [0,1]
            do iCell = 1, nCells
                do iPt = 1, nPts
                    xiTemp(1) = 0.5*(xi(1,iPt) + 1.0)
                    xCart(:) = ref2cart(nDim,iCell,xiTemp)
                    ! functions with exact temporal solutions available  
                    iterateSol(:,iPt,iCell) = evalFunction(nDim,nEqns,xCart,t=tf)
                end do
            end do
        else
          select case ( maxFacesPerCell )

            case ( 3 ) ! triangle
              if ( present(nBasis) ) then
                  xi(:,:) = symTriLoc(nPts)
              else
                  xi(:,:) = dunavantLoc(nPts)
              end if
              do iCell = 1, nCells
                  do iPt = 1, nPts
                      xCart(:) = ref2cart(nDim,iCell,xi(:,iPt))
                      ! functions with exact temporal solutions available  
                      iterateSol(:,iPt,iCell) = evalFunction(nDim,nEqns,xCart,t=tf)
                  end do
              end do

            case ( 4 ) ! quadrilateral
              xi(1,:) = gaussQuadLoc(nPts)
              do iCell = 1, nCells
                  do jPt = 1, nPts
                      do iPt = 1, nPts
                          ! convert Gaussian quadrature location to interval from [0,1]
                          xiTemp(1) = 0.5*(xi(1,iPt) + 1.0)
                          xiTemp(2) = 0.5*(xi(1,jPt) + 1.0)
                          xCart(:) = ref2cart(nDim,iCell,xiTemp)
                          ! functions with exact temporal solutions available  
                          iterateSol(:,jPt+nPts*(iPt-1),iCell) = &
                              evalFunction(nDim,nEqns,xCart,t=tf)
                      end do
                  end do
              end do
          end select
        end if

      case ( BURGERS_EQ ) 
        ! iterate for solutions
        if ( nDim == 1 ) then
            if ( present(nBasis) ) then
                xi(1,:) = lagrange1dLoc(nPts)
            else
                xi(1,:) = gaussQuadLoc(nPts)
            end if
            do iCell = 1, nCells
                do iPt = 1, nPts
                    ! convert Gaussian quadrature location to interval from [0,1]
                    xiTemp(1) = 0.5*(xi(1,iPt) + 1.0)
                    xCart(:) = ref2cart(nDim,iCell,xiTemp)
                    ! burgers equations, iterated implicit solution
                    iterateSol(:,iPt,iCell) = burgSol(nDim,nEqns, &
                            finalSol(:,iPt,iCell),xCart,tf)
                end do
            end do
        else
          ! 2D
          select case ( maxFacesPerCell )
            case ( 3 ) ! triangle
              if ( present(nBasis) ) then
                  xi(:,:) = symTriLoc(nPts)
              else
                  xi(:,:) = dunavantLoc(nPts)
              end if
              do iCell = 1, nCells
                  do iPt = 1, nPts
                      xCart(:) = ref2cart(nDim,iCell,xi(:,iPt))
                      ! burgers equations, iterated implicit solution
                      iterateSol(:,iPt,iCell) = burgSol(nDim,nEqns, &
                              finalSol(:,iPt,iCell),xCart,tf)
                  end do
              end do

            case ( 4 ) ! quadrilateral 
              xi(1,:) = gaussQuadLoc(nPts)
              do iCell = 1, nCells
                  do jPt = 1, nPts
                      do iPt = 1, nPts
                          ! convert Gaussian quadrature location to interval from [0,1]
                          xiTemp(1) = 0.5*(xi(1,iPt) + 1.0)
                          xiTemp(2) = 0.5*(xi(1,jPt) + 1.0)
                          xCart(:) = ref2cart(nDim,iCell,xiTemp)
                          ! pressureless euler equations, iterated implicit solution
                          iterateSol(:,jPt+nPts*(iPt-1),iCell) = burgSol(nDim,nEqns, &
                                finalSol(:,jPt+nPts*(iPt-1),iCell),xCart,tf)
                      end do
                  end do
              end do
          end select

        end if

      case ( PLESSEULER_EQ ) 
        ! iterate for solutions
        if ( nDim == 1 ) then
            if ( present(nBasis) ) then
                xi(1,:) = lagrange1dLoc(nPts)
            else
                xi(1,:) = gaussQuadLoc(nPts)
            end if
            do iCell = 1, nCells
                do iPt = 1, nPts
                    ! convert Gaussian quadrature location to interval from [0,1]
                    xiTemp(1) = 0.5*(xi(1,iPt) + 1.0)
                    xCart(:) = ref2cart(nDim,iCell,xiTemp)
                    ! pressureless euler equations, iterated implicit solution
                    iterateSol(:,iPt,iCell) = pLessEulerSol(nDim,nEqns, &
                            finalSol(:,iPt,iCell),xCart,tf)
                end do
            end do
        else
          ! 2D
          select case ( maxFacesPerCell )

            case ( 3 ) ! triangle
              if ( present(nBasis) ) then
                  xi(:,:) = symTriLoc(nPts)
              else
                  xi(:,:) = dunavantLoc(nPts)
              end if
              do iCell = 1, nCells
                  do iPt = 1, nPts
                      xCart(:) = ref2cart(nDim,iCell,xi(:,iPt))
                      ! pressureless euler equations, iterated implicit solution
                      iterateSol(:,iPt,iCell) = pLessEulerSol(nDim,nEqns, &
                              finalSol(:,iPt,iCell),xCart,tf)
                  end do
              end do

            case ( 4 ) ! quadrilateral
              xi(1,:) = gaussQuadLoc(nPts)
              do iCell = 1, nCells
                  do jPt = 1, nPts
                      do iPt = 1, nPts
                          ! convert Gaussian quadrature location to interval from [0,1]
                          xiTemp(1) = 0.5*(xi(1,iPt) + 1.0)
                          xiTemp(2) = 0.5*(xi(1,jPt) + 1.0)
                          xCart(:) = ref2cart(nDim,iCell,xiTemp)
                          ! pressureless euler equations, iterated implicit solution
                          iterateSol(:,jPt+nPts*(iPt-1),iCell) = &
                              pLessEulerSol(nDim,nEqns, & 
                                finalSol(:,jPt+nPts*(iPt-1),iCell),xCart,tf)
                      end do
                  end do
              end do
          end select
        end if
      
    end select

end function iterateSol
!-------------------------------------------------------------------------------
!> @purpose 
!>  Calculate internal value using xFlow basis functions
!>
!> @author
!>  Timothy A. Eymann
!>
!> @history
!>  22 June 2012 - Initial creation
!>  1 August 2012 - Add p=1 basis function
!>
function xReconValue(nDim,nBasis,iEq,iCell,xi)

    use reconstruction, only: reconData
    implicit none

    ! Interface variables
    integer, intent(in) :: nDim,    & !< problem dimension
                           iEq,     & !< equation index
                           iCell,   & !< element index
                           nBasis     !< number of basis functions

    real, intent(in) :: xi(nDim) !< reference coordinate

    ! Function variable
    real(FP) :: xReconValue

    ! Local variable
    integer :: iPhi !< basis index

    real(FP) :: phi(nBasis) !< basis value

    phi(:) = 0.0
    select case ( nBasis )
        case (3)
            phi(1) = 1.0 - xi(1) - xi(2)
            phi(2) = xi(1)
            phi(3) = xi(2)

        case (6)
            phi(1) = 1.0 - 3.0*xi(1) + 2.0*xi(1)**2 - 3.0*xi(2) + &
                     4.0*xi(1)*xi(2) + 2.0*xi(2)**2
            phi(2) = 4.0*(xi(1) - xi(1)**2 - xi(1)*xi(2))
            phi(3) = -xi(1) + 2.0*xi(1)**2
            phi(4) = 4.0*(xi(2) - xi(2)**2 - xi(1)*xi(2))
            phi(5) = 4.0*xi(1)*xi(2)
            phi(6) = 2.0*xi(2)**2 - xi(2)
    end select

    xReconValue = 0.0
    do iPhi = 1, nBasis
        xReconValue = xReconValue + phi(iPhi)*reconData(iEq,iPhi,iCell)
    end do

end function xReconValue
!-------------------------------------------------------------------------------
!> @purpose 
!>  Read data from an output file produced by xflow
!>
!> @author
!>  Timothy A. Eymann
!>
!> @history
!>  27 June 2012 - Initial creation
!>
subroutine readXflowData(fileUnit,nBasis)

    use reconstruction, only: reconData
    use meshUtil, only: nCells
    implicit none

    ! Interface variables
    integer, intent(in) :: fileUnit !< file unit number

    integer, intent(out), optional :: nBasis !< number of basis functions

    ! Local variables
    character(1) :: header !< file metadata
    
    integer :: line,    & !< line index
               iCell,   & !< cell index
               nBas,    & !< number of basis functions
               p          !< order (assumed to be same for all cells)

    read(fileUnit,'(a)',advance='yes') (header, line=1,11)
    do iCell = 1, nCells
        read(fileUnit,*) p, nBas
        read(fileUnit,*) reconData(1,1:nBas,iCell)
    end do

    if ( present(nBasis) ) then
        nBasis = nBas
    end if

end subroutine readXflowData
!-------------------------------------------------------------------------------
end module
!-------------------------------------------------------------------------------
