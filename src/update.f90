!-------------------------------------------------------------------------------
!> @purpose 
!>  Routines that advance the solution in time
!>  Advection updates using Characteristic Tracing scheme
!>
!> @history
!>  3 October 2014 - Implementation of RD scheme (Maeng)
!>  26 January 2015 - Started looking into pressure-less Euler 2d (Maeng)
!>  20 September 2016 - Integrate all advective systems (Maeng)
!>  
module update

    use solverVars, only: nEqns, dtIn, FP
    use meshUtil, only: nDim, nCells, nEdges, nNodes
    implicit none

    ! Shared module variables 
    integer, parameter :: LINADV_EQ = 1,     & !< flag for linear advection solution
                          PLESSEULER_EQ = 3, & !< flag for pressureless Euler solution
                          BURGERS_EQ = 2       !< flag for burgers solution

    integer :: eqFlag   !< flag for equation type

    real(FP), allocatable :: cellAvg(:,:),    & !< conserved state variable at centroids
                             cellAvgN(:,:),   & !< conserved variable at timestep "n"
                             primAvg(:,:),    & !< primitive variable average at cell centroids
                             primAvgN(:,:),   & !< primitive variable average at timestep "n"
                             deltaAvg(:,:),   & !< discrepancy value
                             edgeDataN(:,:),  & !< edge solution at iteration n
                             edgeData(:,:),   & !< edge solution at iteration n+1
                             edgeDataH(:,:),  & !< edge solution at iteration n+1/2
                             nodeDataN(:,:),  & !< node solution at iteration n
                             nodeData(:,:),   & !< node solution at iteration n+1
                             nodeDataH(:,:)     !< node solution at iteration n+1/2

    real(FP), allocatable :: residMax(:),   & !< max flux residual 
                             residNorm(:),  & !< L1 norm of flux residual over all cells
                             primNorm(:),   & !< L1 norm of primitive variables
                             conservNorm(:)   !< L1 norm of conserved variables

contains
!-------------------------------------------------------------------------------
!> @purpose 
!>  2D AF advective system solver   
!>
!> @history
!>  20 September 2016 - Initial creation (Maeng)
!>
subroutine advectionSystems(nDim,nEqns,iter)

    use reconstruction, only: reconData
    use physics, only: LINADV_FLUX, PLESSEULER_FLUX, BURGERS_FLUX
    use meshUtil, only: nodeCoord, edgeCoord   

    implicit none

    !> Interface variables
    integer, intent(in) :: nDim,    & !< problem dimension
                           nEqns,   & !< number of equations
                           iter       !< iteration number

    ! Set all values equal in time
    nodeDataH = nodeDataN
    nodeData = nodeDataN
    edgeDataH = edgeDataN
    edgeData = edgeDataN


    select case ( eqFlag )

        case ( LINADV_EQ ) 
            !! linear advection with constant velocity
            !call advectionUpdate( nDim,nEqns,iter,ws )
            !call updateCells( nDim,nEqns,nCells,LINADV_FLUX, &
            !                  cellAvgN,cellAvg,ws )

            ! linear advection with prescribed varying velocity
            call advectionUpdate( nDim,nEqns,iter )

            ! conservation stage
            call updateCells( nDim,nEqns,nCells,LINADV_FLUX, &
                              cellAvgN,cellAvg )
            
        case ( BURGERS_EQ )
            ! burgers equation
            call advectionUpdate( nDim,nEqns,iter )
            
            ! conservation stage
            call updateCells( nDim,nEqns,nCells,BURGERS_FLUX, &
                              cellAvgN,cellAvg )

        case ( PLESSEULER_EQ )
            ! pressureless euler 
            call advectionUpdate( nDim,nEqns,iter )

            ! conservation stage
            call updateCells( nDim,nEqns,nCells,PLESSEULER_FLUX, &
                              cellAvgN,cellAvg )
        
    end select

    call conserv2primAvg( nDim,nEqns,nNodes,nEdges,nCells, &
                          nodeData,edgeData,cellAvg,primAvg )

    !! RECONCILIATION
    !call reconcileEdgeData( nDim,nEqns,nCells,nNodes,nEdges, &
    !                        cellAvg,nodeData,edgeData )

    ! Store for next time step
    edgeDataN = edgeData 
    nodeDataN = nodeData 
    cellAvgN = cellAvg 
    primAvgN = primAvg 

    !! check conservation
    !call conservationCheck()

end subroutine advectionSystems
!-------------------------------------------------------------------------------
!> @purpose 
!>  Update values of reconData array
!>
!> @author
!>  Timothy A. Eymann
!>
!> @history
!>  13 June 2012 - Initial creation
!>
subroutine updateReconData()

    use reconstruction, only: reconData

    implicit none

    ! Local variables
    integer :: iCell,       & !< cell index
               iEq

    ! set reconstruction coefficients
    do iCell = 1, nCells
        do iEq = 1, nEqns
            reconData(iEq,:,iCell) = coefficients(nDim,iEq,iCell)
        end do
    end do

end subroutine updateReconData
!-------------------------------------------------------------------------------
!> @purpose 
!>  Apply advection operator using Characteristic Origin Tracing scheme.
!>  Advection speed is determined by node/edge velocity
!>
!> @history
!>  14 May 2013 - Initial creation (Eymann)
!>  4 September 2013 - Linear advection constant wave speed is added (Maeng)
!>  13 January 2015 - 2D Burgers' equations (Maeng) 
!>  1 February 2015 - 2D Pressureless Euler equations (Maeng) 
!>
subroutine advectionUpdate(nDim,nEqns,iter,lam)

    use meshUtil, only: maxNodesPerCell, cellNodes, &
                        maxFacesPerCell, cellFaces, &
                        nodeCoord, edgeCoord
    use boundaryConditions, only: applyBC, nodeBC, edgeBC

    implicit none

    ! Interface variables
    integer, intent(in) :: nDim,    & !< problem dimension
                           nEqns,   & !< number of equations in system
                           iter       !< iteration number

    real(FP), intent(in), optional :: lam(nDim)

    ! Local variables
    integer :: iCell,           & !< cell index in loop
               iNode,           & !< local node index within cell
               iEdge,           & !< local edge index witn cell
               gNode,           & !< global node index
               gEdge,           & !< global edge index
               neglectedEdgesH, & !< number of edges without an update
               neglectedNodesH, & !< number of nodes without an update
               neglectedEdges,  & !< number of edges without an update
               neglectedNodes     !< number of nodes without an update

    logical, target :: nodeUpdatedH(nNodes), & !< flag that a node has been updated
                       edgeUpdatedH(nEdges), & !< flag that an edge has been updated
                       nodeUpdated(nNodes),  & !< flag that a node has been updated
                       edgeUpdated(nEdges)     !< flag that an edge has been updated

    real(FP) :: dt,             & !< time step
                vel(nDim),      & !< local advection velocity 
                signal(nEqns)     !< cell's contribution to node/edge value

    nodeUpdatedH = .false.
    edgeUpdatedH = .false.
    nodeUpdated = .false.
    edgeUpdated = .false.

    ! default
    dt = dtIn

    ! Set reconstruction coefficients for primitive variables
    call updateReconData()

    do iCell = 1,nCells

        do iNode = 1, maxNodesPerCell
            gNode = cellNodes(iNode,iCell)

            ! euler equations advection velocity 
            if ( present(lam) ) then
                ! linear advection
                vel(:) = lam(:)
            else
                if ( nDim == 1 ) then
                    vel(:) = nodeDataN(2,gNode) 
                else 
                    vel(:) = nodeDataN(2:3,gNode) 
                end if
            end if

            ! half time step
            signal(:) = signalValue( nEqns,nDim,iCell,iNode, &
                            0.5_FP*dt,vel,nodeDataN(:,gNode), &
                                 nodeUpdatedH(gNode) )
            nodeDataH(:,gNode) = nodeDataH(:,gNode) + signal
            ! apply boundary condition
            call applyBC(nEqns,gNode,signal,nodeDataH,0.5_FP*dt, &
                                nodeUpdatedH)

            ! full time step
            signal(:) = signalValue( nEqns,nDim,iCell,iNode, &
                            dt,vel,nodeDataN(:,gNode), &
                                 nodeUpdated(gNode) )
            nodeData(:,gNode) = nodeData(:,gNode) + signal
            ! apply boundary condition
            call applyBC(nEqns,gNode,signal,nodeData,dt, &
                                nodeUpdated)

        end do

        do iEdge = 1, maxFacesPerCell
            gEdge = cellFaces(iEdge,iCell)

            ! euler equations advection velocity 
            if ( present(lam) ) then
                ! linear advection
                vel(:) = lam(:)
            else
                if ( nDim == 1 ) then
                    vel(:) = edgeDataN(2,gEdge) 
                else
                    vel(:) = edgeDataN(2:3,gEdge) 
                end if
            end if

            ! half time step
            signal(:) = signalValue( nEqns,nDim,iCell,-iEdge, &
                            0.5_FP*dt,vel,edgeDataN(:,gEdge), &
                                 edgeUpdatedH(gEdge) )
            edgeDataH(:,gEdge) = edgeDataH(:,gEdge) + signal
            ! apply boundary condition
            call applyBC(nEqns,-gEdge,signal,edgeDataH,0.5_FP*dt, &
                                edgeUpdatedH)

            ! full time step
            signal(:) = signalValue( nEqns,nDim,iCell,-iEdge, &
                            dt,vel,edgeDataN(:,gEdge),  &
                                 edgeUpdated(gEdge) )
            edgeData(:,gEdge) = edgeData(:,gEdge) + signal
            ! apply boundary condition
            call applyBC(nEqns,-gEdge,signal,edgeData,dt, &
                                edgeUpdated)
        end do

    end do

    !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
    !! Specific inflow
    !!
    !! 18 February 2016
    !!
    !! use this for solid body rotation where inflow is needed 
    !!
    !do gNode = 1, nNodes
    !    if ( .not. nodeUpdatedH(gNode) ) then
    !       nodeDataH(:,gNode) = pLessEulerSol(nDim,nEqns,&
    !           nodeDataH(:,gNode),nodeCoord(:,gNode),tSim+0.5_FP*dtIn)
    !    end if
    !    if ( .not. nodeUpdated(gNode) ) then
    !       nodeData(:,gNode) = pLessEulerSol(nDim,nEqns,&
    !           nodeData(:,gNode),nodeCoord(:,gNode),tSim+dtIn)
    !    end if
    !end do
    !do gEdge = 1, nEdges
    !    if ( .not. edgeUpdatedH(gEdge) ) then
    !       edgeDataH(:,gEdge) = pLessEulerSol(nDim,nEqns,&
    !           edgeDataH(:,gEdge),edgeCoord(:,gEdge),tSim+0.5_FP*dtIn)
    !    end if
    !    if ( .not. edgeUpdated(gEdge) ) then
    !       edgeData(:,gEdge) = pLessEulerSol(nDim,nEqns,&
    !           edgeData(:,gEdge),edgeCoord(:,gEdge),tSim+dtIn)
    !    end if
    !end do
    !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!

    ! print out neglected node/edge
    neglectedEdgesH = nEdges - count( edgeUpdatedH )
    neglectedNodesH = nNodes - count( nodeUpdatedH )
    neglectedEdges = nEdges - count( edgeUpdated )
    neglectedNodes = nNodes - count( nodeUpdated )
    if ( ( neglectedEdges > 0 ) .or. ( neglectedNodes > 0 ) .or. &
         ( neglectedEdgesH > 0 ) .or. ( neglectedNodesH > 0 ) ) then
        write(*,*)
        write(*,*) 'Adevection update'
        write(*,'(2(a,i0),a)') 'WARNING: ',neglectedNodesH,' nodes and ', &
                   neglectedEdgesH,' edges at half time have not been updated.'
        write(*,'(2(a,i0),a)') 'WARNING: ',neglectedNodes,' nodes and ', &
                   neglectedEdges,' edges at full time have not been updated.'
        write(*,*)
        write(*,'(a7,2a12,a4)') 'Node','x','y','bc'
        write(*,'(a7,2a12,a4)') '------','-----------','-----------','---'
        do iNode = 1, nNodes
            if ( .not. nodeUpdated(iNode) ) then
                write(*,'(i7,2e12.4e2,i4)') iNode,nodeCoord(:,iNode), &
                                            nodeBC(iNode)
                !if ( nodebc(iNode) == 0 ) then
                !    read(*,*)
                !end if
            end if
        end do
        write(*,*)
        write(*,'(a7,2a12,a4)') 'Edge','x','y','bc'
        write(*,'(a7,2a12,a4)') '------','-----------','-----------','---'
        do iEdge = 1, nEdges
            if ( .not. edgeUpdated(iEdge) ) then
                write(*,'(i7,2e12.4e2,i4)') iEdge,edgeCoord(:,iEdge), &
                                            edgeBC(iEdge)
                !if ( edgebc(iedge) == 0 ) then
                !    read(*,*)
                !end if
            end if
        end do
        write(*,*)
    end if
    
    ! Check for edges/nodes without an update
#ifdef VERBOSE
    if ( ( neglectedEdges > 0 ) .or. ( neglectedNodes > 0 ) .or. &
         ( neglectedEdgesH > 0 ) .or. ( neglectedNodesH > 0 ) ) then
        write(*,*)
        write(*,*) 'Adevection update'
        write(*,'(2(a,i0),a)') 'WARNING: ',neglectedNodesH,' nodes and ', &
                   neglectedEdgesH,' edges at half time have not been updated.'
        write(*,'(2(a,i0),a)') 'WARNING: ',neglectedNodes,' nodes and ', &
                   neglectedEdges,' edges at full time have not been updated.'
        write(*,*)
        write(*,'(a7,2a12,a4)') 'Node','x','y','bc'
        write(*,'(a7,2a12,a4)') '------','-----------','-----------','---'
        do iNode = 1, nNodes
            if ( .not. nodeUpdated(iNode) ) then
                write(*,'(i7,2e12.4e2,i4)') iNode,nodeCoord(:,iNode), &
                                            nodeBC(iNode)
                !if ( nodebc(iNode) == 0 ) then
                !    read(*,*)
                !end if
            end if
        end do
        write(*,*)
        write(*,'(a7,2a12,a4)') 'Edge','x','y','bc'
        write(*,'(a7,2a12,a4)') '------','-----------','-----------','---'
        do iEdge = 1, nEdges
            if ( .not. edgeUpdated(iEdge) ) then
                write(*,'(i7,2e12.4e2,i4)') iEdge,edgeCoord(:,iEdge), &
                                            edgeBC(iEdge)
                !if ( edgebc(iedge) == 0 ) then
                !    read(*,*)
                !end if
            end if
        end do
        write(*,*)
    end if
#endif

end subroutine advectionUpdate
!-------------------------------------------------------------------------------
!> @purpose 
!>  Calculate the signal sent from a cell at a specified time
!>
!> @history
!>  26 June 2012 - Initial creation (Eymann)
!>  14 May 2013 - Use local advection speed (Eymann)
!>  1 February 2015 - Pressure-less Euler system. Velocity divergence
!>      correction for density (Maeng)
!>  5 October 2015 - Incorporated a mandatory update flag, and onEdge flag
!>      no longer used. 
!>  9 October 2015 - Euler equations in 2d (Maeng)
!>  11 November 2015 - Remove velocity divergence effect from pressureless Euler
!>      for the implementation of Euler equations (Maeng)
!>
function signalValue(nEqns,nDim,iCell,iNodeEdge,dt,lam,qN,updated,iter,cIn)
   
    use solverVars, only: eps
    use reconstruction, only: reconState, reconValue, reconValueBFdiff, &
                              gradVal, refgradVal, refsecondGradVal  
    use meshUtil, only: cellFaces, faceCells, cellNodes, &
                        cellInvJac, ref2cart, xiNode, xiEdge, faceNormal

    implicit none

    ! Interface variables
    integer, intent(in) :: nEqns,   & !< number of equations in system
                           nDim,    & !< number of dimensions
                           iCell,   & !< cell index
                           iNodeEdge  !< local node index (negative for edge)

    real(FP), intent(in) :: dt,                 & !< timestep for signal
                            lam(nDim),          & !< local advection speed
                            qN(nEqns)             !< state at time n

    logical, intent(inout) :: updated !< flag that update should be applied

    integer, intent(in), optional :: iter

    real(FP), intent(in), optional :: cIn(nEqns,(nDim+2)*(nDim+3)/(2*(3-nDim))) 

    ! Function variables
    real(FP) :: signalValue(nEqns)

    ! Local variables
    integer :: iEq,     & !< equation index
               onEdge     !< edge containing char. origin

    integer :: gFace,   & !< global face index
               gNode,   &
               pEdge,   & !< periodic edge
               lCell,   & !< left cell index
               rCell      !< right cell index

    real(FP) :: xi0(nDim),          & !< char origin in ref space
                qXi0(nEqns),        & !< state at char. origin
                cVel(nDim),         & !< corrected wave speed
                gradU(nDim,nDim),   & !< velocity gradient
                tol,                & !< tolerance
                divSignal,          & !< divergence effect on density signal 
                divU                  !< velocity divergence

    integer :: iDir, gFace2

    real(FP) :: xi(nDim),   &  
                xiL(nDim),  &
                xiR(nDim),  &  
                xiNorm(nDim), &
                lEdgeNorm(nDim),&
                align, align2, &
                xCart(nDim)   

    logical :: inCell = .false.     !< flag that signal originates in cell

    tol = eps
    signalValue = 0.0_FP
    qXi0 = 0.0_FP

    ! corrected advection velocity
    if ( eqFlag == LINADV_EQ ) then
        ! linear advection
        cVel = lam
    else
        ! nonlinear advection - velocity correction
        cVel = corrWaveSpeed(nDim,iCell,iNodeEdge,dt,lam)
    end if
    call charOrigin(nDim,iCell,iNodeEdge,dt,cVel,xi0,inCell,onEdge)

    lCell = iCell
    rCell = iCell
    xiL = xi0
    xiR = xi0
    ! only modify values if signal for wave originates in cell
    if ( ( inCell ) .and. ( .not. updated ) ) then  
        ! find characteristic origins in cross wind directions
        !call charOriginCrossWind(nDim,iCell,iNodeEdge,dt,cVel,xi0,xiL,xiR,lCell,rCell)

        select case ( eqFlag )
        
          case ( LINADV_EQ ) 
            ! original characteristic origin tracing
            ! do not update other variables, other than iEq == 1
            qXi0(1) = reconValue(nDim,1,iCell,xi0(:))
            signalValue(1) = qXi0(1) - qN(1)
            if ( abs(signalValue(1)) <= tol ) signalValue(1) = 0.0_FP
            
          case ( BURGERS_EQ ) 

            do iEq = 1,nEqns
                qXi0(iEq) = reconValue(nDim,iEq,iCell,xi0(:))
                signalValue(iEq) = qXi0(iEq) - qN(iEq) 
                if ( abs(signalValue(iEq)) <= tol ) signalValue(iEq) = 0.0_FP
            end do

          case ( PLESSEULER_EQ ) 

            do iEq = 1,nEqns
                ! original characteristic origin tracing
                qXi0(iEq) = reconValue(nDim,iEq,iCell,xi0(:))
                !qXi0(iEq) = reconValueBFDiff(nDim,iEq,iCell,lCell,rCell,xi0(:),xiL,xiR)
                if ( iEq == 1 ) then 
                    ! density correction by divergence of vel.
                    divSignal = 0.0_FP
                    if ( nDim == 2 ) then
                        ! velocity gradients
                        gradU(1,:) = gradVal(nDim,2,iCell,xi0) ! grad u
                        gradU(2,:) = gradVal(nDim,3,iCell,xi0) ! grad v
                        ! divergence of velocity
                        !divU = velocityDiv( nDim, iCell, xi0, dt )
                        divU = gradU(1,1)+gradU(2,2)
                        ! old
                        divSignal = qXi0(iEq)/(1.0_FP + dt*(divU)  + &
                                        dt*dt*(gradU(1,1)*gradU(2,2) - gradU(1,2)*gradU(2,1)))
                        !divSignal = qXi0(iEq)/( 1.0_FP + dt*(divU) )
                        ! new 3/3/2017
                        ! These two approaches are numerically identical
                        !divSignal = qXi0(iEq)*(1.0_FP - dt*(divU) + dt*dt*(divU*divU) - &
                        !                dt*dt*(gradU(1,1)*gradU(2,2) - gradU(1,2)*gradU(2,1)))
                    else    
                        ! nDim == 1
                        ! velocity gradients
                        gradU(1,:) = gradVal(nDim,2,iCell,xi0) ! grad u
                        !divU = velocityDiv( nDim, iCell, xi0, dt )
                        divU = gradU(1,1)
                        ! old
                        divSignal = qXi0(iEq)/(1.0_FP + dt*(divU)) 
                        ! new 3/3/2017
                        ! These two approaches are numerically identical
                        !divSignal = qXi0(iEq)*(1.0_FP - dt*(divU) + dt*dt*(divU*divU))
                    end if
                    ! signal value
                    signalValue(iEq) = divSignal - qN(iEq)
                else    
                    ! velocities
                    signalValue(iEq) = qXi0(iEq) - qN(iEq) 
                end if

                if ( abs(signalValue(iEq)) <= tol ) signalValue(iEq) = 0.0_FP
            end do

        end select

        ! mark node/edge as updated
        updated = .true.
    end if

end function signalValue
!-------------------------------------------------------------------------------
!> @purpose
!>  Calculate the origin of characteristic intersecting a space-time face
!>
!> @author
!>  Timothy A. Eymann
!>
!> @history
!>  8 May 2012 - Initial creation\n
!>  23 May 2012 - Add Burgers equation\n
!>  3 October 2012 - Add flag for origin lying on interface
!>  4 November 2014 - Add tolerance on origins (Maeng)
!>
subroutine charOrigin(nDim,iCell,iNodeEdge,dt,lam,xi0,inCell,onEdge)

    use solverVars, only: eps
    use meshUtil, only: cellInvJac, xiNode, xiEdge, &
                        maxFacesPerCell, cellFaces, cellNodes

    implicit none

    !< Interface variables
    integer, intent(in) :: nDim,        & !< problem dimension
                           iCell,       & !< cell index
                           iNodeEdge      !< origin node

    real(FP), intent(in) :: dt,             & !< time at interface
                            lam(nDim)         !< average velocity in cell

    real(FP), intent(out) :: xi0(nDim) !< characterisitic origin

    logical, intent(out) :: inCell !< flag that cell contains characteristic

    integer, intent(out) :: onEdge !< local index of edge for char. origin

    !< Local variables
    real(FP) :: xi(nDim),     & !< reference coordinate of interface
                xiTemp(nDim), &
                tol

    tol = eps

    if ( iNodeEdge > 0 ) then
        xi(:) = xiNode(:,iNodeEdge)
    else
        xi(:) = xiEdge(:,abs(iNodeEdge))
    end if

    ! find originTemp
    xiTemp(:) = -1.0_FP*dt*matmul(cellInvJac(:,:,iCell),lam(:))  
    xi0(:) = xi(:) + xiTemp(:)
    
    !if ( iNodeEdge > 0 .and. cellNodes(abs(iNodeEdge),iCell) == 18840 ) then
    !    write(*,*) iCell, iNodeEdge, xi0, cellInvJac(:,:,iCell), lam
    !end if

    ! determine if char. origin is inside cell
    inCell = .false.
    onEdge = 0
    select case ( nDim )

        case (1)
            !! zero tolerance check
            !if ( abs(xi0(1)) <= tol ) xi0(1) = 0.0_FP

            if ( (( xi0(1) > 0.0_FP ) .and. ( xi0(1) < 1.0_FP )) ) then
                inCell = .true.
            elseif ( xi0(1) == 1.0_FP ) then
                inCell = .true.
                onEdge = 2
            elseif ( xi0(1) == 0.0_FP ) then
                inCell = .true.
                onEdge = 1
            end if

        case (2)
            ! zero tolerance check
            ! FIXME: don't like the tolerance check
            ! Needs to be better
            if ( abs(xi0(1)) <= 1.0e-13 ) xi0(1) = 0.0_FP
            if ( abs(xi0(2)) <= 1.0e-13 ) xi0(2) = 0.0_FP

            select case ( maxFacesPerCell )
            
              case ( 3 )
                ! standard triangle
                if ( (abs(xi0(1)) <= tol) .and. &
                     ((xi0(2) >= 0.0_FP) .and. (xi0(2) <= 1.0_FP)) ) then
                    inCell = .true.
                    onEdge = 2
                elseif ( (abs(xi0(2)) <= tol) .and. &
                         ((xi0(1) >= 0.0_FP) .and. (xi0(1) <= 1.0_FP)) ) then
                    inCell = .true.
                    onEdge = 3
                elseif ( (abs(sum(xi0(:))-1.0_FP) <= tol ) .and. &
                         ((xi0(1) >= 0.0_FP) .and. (xi0(1) <= 1.0_FP)) ) then
                    inCell = .true.
                    onEdge = 1
                elseif ( ((xi0(1) > 0.0_FP) .and. (xi0(1) < 1.0_FP)) .and. &
                     ((xi0(2) > 0.0_FP ) .and. (sum(xi0(:)) < 1.0_FP)) ) then
                    inCell = .true.
                    onEdge = 0
                end if

              case ( 4 )
                ! standard quadrilateral xi,eta in [0,1]
                if ( ( abs(xi0(2)) <= tol ) .and. &
                     ( (xi0(1) >= 0.0_FP) .and. (xi0(1) <= 1.0_FP) ) ) then
                    inCell = .true.
                    onEdge = 1 
                elseif ( ( abs(xi0(1)-1.0_FP) <= tol ) .and. &
                     ( (xi0(2) >= 0.0_FP) .and. (xi0(2) <= 1.0_FP) ) ) then
                    inCell = .true.
                    onEdge = 2
                elseif ( ( abs(xi0(2)-1.0_FP) <= tol ) .and. &
                     ( (xi0(1) >= 0.0_FP) .and. (xi0(1) <= 1.0_FP) ) ) then
                    inCell = .true.
                    onEdge = 3
                elseif ( ( abs(xi0(1)) <= tol ) .and. &
                     ( (xi0(2) >= 0.0_FP) .and. (xi0(2) <= 1.0_FP) ) ) then
                    inCell = .true.
                    onEdge = 4 
                elseif ( ( (xi0(1) > 0.0_FP) .and. (xi0(1) < 1.0_FP) ) .and. &
                         ( (xi0(2) > 0.0_FP) .and. (xi0(2) < 1.0_FP) ) ) then
                    inCell = .true.
                    onEdge = 0 
                end if

            end select

    end select


end subroutine charOrigin
!-------------------------------------------------------------------------------
!> @purpose
!>  First order correction for nonlinear advection velocity 
!>
!> @author
!>  J. Brad Maeng
!>
!> @history
!>  21 January 2015 - Initial creation
!>  24 February 2015 - Included second order correction term
!>  16 April 2015 - Exact correction terms added for comparison
!>
function corrWaveSpeed(nDim,iCell,iNodeEdge,dt,lam,cIn)

    use solverVars, only: eps, tSim
    use meshUtil, only: cellInvJac, xiNode, xiEdge, ref2cart
    use reconstruction, only: gradVal

    implicit none

    !< Interface variables
    integer, intent(in) :: nDim,        & !< problem dimension
                           iCell,       & !< cell index
                           iNodeEdge      !< origin node

    real(FP), intent(in) :: dt,         & !< time step
                            lam(nDim)     !< waveSpeed

    real(FP), intent(in), optional :: cIn(nEqns, &
        (nDim+2)*(nDim+3)/(2*(3-nDim))) !< function coefficients

    !< Function variable
    real(FP) :: corrWaveSpeed(nDim)  

    !< Local variables
    integer :: iDir       !< dimension index

    real(FP) :: tol,                    & !< tolerance
                xi(nDim),               & !< reference coordinate of interface
                gradU(nDim,nDim),       & !< gradient of vector u
                corrMat(nDim,nDim),     & !< correction matrix
                corrMattemp(nDim,nDim), & !< correction matrix, temp
                detMat                    !< determinant of matrix

    tol = eps

    if ( iNodeEdge > 0 ) then
        xi(:) = xiNode(:,iNodeEdge)
    else
        xi(:) = xiEdge(:,abs(iNodeEdge))
    end if

    gradU(:,:) = 0.0_FP 
    select case ( nDim )

        case ( 1 )

            ! evaluate gradient 
            gradU(1,:) = gradVal(nDim,2,iCell,xi)

            ! Exact correction
            !corrMat(1,:) = 1.0_FP + 1.0_FP*dt*gradU(1,:) 
            !corrWaveSpeed(:) = lam(:)/corrMat(1,:)  

            ! Approximation of geometric series 
            corrWaveSpeed(:) = lam(:) - lam(:)*gradU(1,:)*dt  

        case ( 2 ) 

            ! evaluate gradient 
            gradU(1,:) = gradVal(nDim,2,iCell,xi) 
            gradU(2,:) = gradVal(nDim,3,iCell,xi) 

            !! Exact correction by inverting matrix
            !corrMat(:,:) = 1.0_FP*dt*gradU(:,:)
            !! add Identity matrix
            !corrMat(1,1) = 1.0_FP + corrMat(1,1) 
            !corrMat(2,2) = 1.0_FP + corrMat(2,2) 
            !! assign the matrix to temporary variable
            !corrMatTemp(:,:) = corrMat(:,:)
            !! invert the matrix
            !corrMat(:,:) = 0.0_FP
            !detMat = (corrMatTemp(1,1)*corrMatTemp(2,2) - &
            !          corrMatTemp(1,2)*corrMatTemp(2,1))
            !if ( abs(detMat) <= tol ) then
            !    ! in case corrMat is sigular
            !    !corrWaveSpeed(:) = lam(:)
            !    corrWaveSpeed(1) = lam(1) - (lam(1)*gradU(1,1) + &
            !        lam(2)*gradU(1,2))*dt  
            !    corrWaveSpeed(2) = lam(2) - (lam(1)*gradU(2,1) + &
            !        lam(2)*gradU(2,2))*dt  
            !else
            !    ! invert corrMat
            !    corrMat(1,1) = (1.0_FP/detMat)*corrMatTemp(2,2) 
            !    corrMat(1,2) = -(1.0_FP/detMat)*corrMatTemp(1,2) 
            !    corrMat(2,1) = -(1.0_FP/detMat)*corrMatTemp(2,1) 
            !    corrMat(2,2) = (1.0_FP/detMat)*corrMatTemp(1,1) 
            !    ! corrected waveSpeed
            !    corrWaveSpeed(:) = matmul(corrMat(:,:),lam(:))  
            !end if

            ! Approximation of matrix multiplication
            corrWaveSpeed(1) = lam(1) - (lam(1)*gradU(1,1)+lam(2)*gradU(1,2))*dt  
            corrWaveSpeed(2) = lam(2) - (lam(1)*gradU(2,1)+lam(2)*gradU(2,2))*dt  

    end select

end function corrWaveSpeed
!-------------------------------------------------------------------------------
!> @purpose 
!>  Velocity divergence accounting for the first order term
!>
!> @author
!>  J. Brad Maeng
!>  
!> @history
!>  24 September 2015 - Initial creation (Maeng)
!>
function velocityDiv( nDim, iCell, xi, dt )

    use reconstruction, only: reconValue, gradVal

    implicit none

    !< Interface variables
    integer, intent(in) :: nDim,        & !< problem dimension
                           iCell          !< cell index

    real(FP), intent(in) :: xi(nDim),   & !< evaluation location 
                            dt            !< time

    !< Function variable
    real(FP) :: velocityDiv

    !< Local variables
    real(FP) :: gradU(nDim,nDim) !< gradient of velocity vector

    velocityDiv = 0.0_FP
    select case ( nDim ) 
        
        case (1)
            ! gradient of velocity vector
            gradU(1,:) = gradVal(nDim,2,iCell,xi) ! ux

            ! velocity divergence
            velocityDiv = gradU(1,1) 

        case (2)
            ! gradient of velocity vector
            gradU(1,:) = gradVal(nDim,2,iCell,xi) ! grad u
            gradU(2,:) = gradVal(nDim,3,iCell,xi) ! grad v 

            ! without second order gradients
            ! divergence of velocity
            ! velocity divergence evaluated at char. org contains second order derivatives
            ! no need to evaluate it again
            velocityDiv = (gradU(1,1)+gradU(2,2))

    end select

end function velocityDiv
!-------------------------------------------------------------------------------
!> @purpose 
!>  Use fluxes to update cell values
!>
!> @history
!>  17 May 2013 - Initial creation (Eymann)
!>  10 April 2015 - Updated for pressureless Euler (Maeng) 
!>
subroutine updateCells( nDim,nEqns,nCells,fluxType,qCellN,qCell,lam )
    
    use solverVars, only: tSim, iRight, iLeft
    use meshUtil, only: faceCells, faceNormal, faceArea, cellVolume, &
                        cellFaces, faceNodes, ref2cart, totalVolume, &
                        faceNormalArea, nodeCoord
    
    implicit none

    ! Interface variables
    integer, intent(in) :: nDim,     & !< problem dimension
                           nEqns,    & !< number of equations
                           nCells,   & !< number of cells
                           fluxType    !< flux used for update

    real(FP), intent(in) :: qCellN(nEqns,nCells)       !< state at time n

    real(FP), intent(inout) :: qCell(nEqns,nCells)     !< state at time n+1

    real(FP), intent(in), optional :: lam(nDim)

    ! Local variables
    integer :: iEq,     & !< equation index
               iEdge,   & !< edge index
               iNode,   &
               gEdge,   &
               lNode,   & !< node index
               rNode,   & !< node index
               iCell,   & !< cell index
               lCell,   & !< left cell index
               rCell      !< right cell index

    real(FP) :: faceFlux(nDim,nEqns),   & !< flux at edge
                resid(nEqns,nCells),    & !< cell residual
                residNormSum(nEqns),    & !< residual norm storage
                dR(nEqns)                 !< change in residual value

    real(FP) :: fNorm(nDim),  &
                fArea
                
    qCell(:,:) = qCellN(:,:)
    resid(:,:) = 0.0_FP
    do iEdge = 1, nEdges
        ! left and right cell index
        lCell = faceCells(iLeft,iEdge)
        rCell = faceCells(iRight,iEdge)

        ! evaluate average flux over a face
        faceFlux(:,:) = averageFlux( nDim,nEqns,fluxType,iEdge,lam )

        do iEq = 1,nEqns
            dR(iEq) = dot_product(faceFlux(:,iEq),faceNormal(:,iEdge)) * &
                      faceArea(iEdge)
        end do

        ! TODO: cell update counter?
        resid(:,lCell) = resid(:,lCell) - dR(:)/cellVolume(lCell)
        qCell(:,lCell) = qCell(:,lCell) - dtIn*dR(:)/cellVolume(lCell)
        if ( rCell > 0 ) then
            resid(:,rCell) = resid(:,rCell) + dR(:)/cellVolume(rCell)
            qCell(:,rCell) = qCell(:,rCell) + dtIn*dR(:)/cellVolume(rCell)
        end if
        
    end do

    ! Residual calculation 
    residNormSum(:) = 0.0_FP
    residMax(:) = 0.0_FP
    ! TODO: select a range from which the residual is taken
    do iEq = 1, nEqns
        do iCell = 1, nCells
            residNormSum(iEq) = residNormSum(iEq) + &
                    cellVolume(iCell)*abs(resid(iEq,iCell))**(2.0_FP)
            residMax(iEq) = max(residMax(iEq),abs(resid(iEq,iCell)))
        end do
    end do
    do iEq = 1, nEqns
        residNorm(iEq) = (residNormSum(iEq)/totalVolume)**(1.0_FP/2.0_FP)   
    end do

end subroutine updateCells
!-------------------------------------------------------------------------------
!> @purpose
!>  Use node and interface values to calculate an average flux
!>
!> @history
!>  14 May 2013 - Initial creation (Eymann)
!>  10 January 2014 - l/rNode correction for 1D (Maeng)
!>  11 March 2015 - 1D flux calculation (Maeng)
!>
function averageFlux(nDim,nEqns,fluxType,iEdge,lam)

    use meshUtil, only: faceNodes, faceCells
    use physics, only: flux
    use mathutil, only: simpson2d
    implicit none

    ! Interface variables
    integer, intent(in) :: nDim,        & !< problem dimension
                           nEqns,       & !< equation for flux
                           fluxType,    & !< flux equation
                           iEdge          !< interface

    real(FP), intent(in), optional :: lam(nDim) !< advection speed

    ! Function variable
    real(FP) :: averageFlux(nDim,nEqns)

    ! Local variables
    integer :: lNode,   & !< left face node
               rNode,   & !< right face node
               iCell,   &
               lCell,   &
               iDir,    & !< direction index
               iEq,     & !< equation index
               i,       & !< row index
               j          !< column index

    real(FP) :: fluxQuad(3,3,nDim,nEqns),   & !< analytical fluxes at quadrature points
                fluxSlice(3,3)                !< flux values for given dimen and eqn

    select case ( nDim )

        case (1)
            lNode = faceNodes(1,iEdge)
            rNode = faceNodes(1,iEdge)

        case (2)
            ! assuming ccw ordering of node
            lNode = faceNodes(2,iEdge)
            rNode = faceNodes(1,iEdge)

    end select

    ! The 'flux' function expects primitive state vectors !
    if ( present(lam) ) then
        fluxQuad(1,1,:,:) = flux( nDim,nEqns,fluxType,nodeData(:,lNode),lam )
        fluxQuad(1,2,:,:) = flux( nDim,nEqns,fluxType,edgeData(:,iEdge),lam )
        fluxQuad(1,3,:,:) = flux( nDim,nEqns,fluxType,nodeData(:,rNode),lam )
        fluxQuad(2,1,:,:) = flux( nDim,nEqns,fluxType,nodeDataH(:,lNode),lam )
        fluxQuad(2,2,:,:) = flux( nDim,nEqns,fluxType,edgeDataH(:,iEdge),lam )
        fluxQuad(2,3,:,:) = flux( nDim,nEqns,fluxType,nodeDataH(:,rNode),lam )
        fluxQuad(3,1,:,:) = flux( nDim,nEqns,fluxType,nodeDataN(:,lNode),lam )
        fluxQuad(3,2,:,:) = flux( nDim,nEqns,fluxType,edgeDataN(:,iEdge),lam )
        fluxQuad(3,3,:,:) = flux( nDim,nEqns,fluxType,nodeDataN(:,rNode),lam )
    else
        fluxQuad(1,1,:,:) = flux( nDim,nEqns,fluxType,nodeData(:,lNode) )
        fluxQuad(1,2,:,:) = flux( nDim,nEqns,fluxType,edgeData(:,iEdge) )
        fluxQuad(1,3,:,:) = flux( nDim,nEqns,fluxType,nodeData(:,rNode) )
        fluxQuad(2,1,:,:) = flux( nDim,nEqns,fluxType,nodeDataH(:,lNode) )
        fluxQuad(2,2,:,:) = flux( nDim,nEqns,fluxType,edgeDataH(:,iEdge) )
        fluxQuad(2,3,:,:) = flux( nDim,nEqns,fluxType,nodeDataH(:,rNode) )
        fluxQuad(3,1,:,:) = flux( nDim,nEqns,fluxType,nodeDataN(:,lNode) )
        fluxQuad(3,2,:,:) = flux( nDim,nEqns,fluxType,edgeDataN(:,iEdge) )
        fluxQuad(3,3,:,:) = flux( nDim,nEqns,fluxType,nodeDataN(:,rNode) )
    end if

    ! TODO: optimize loop for speed
    averageFlux(:,:) = 0.0_FP
    do iDir = 1,nDim
        do iEq = 1,nEqns
            fluxSlice(:,:) = 0.0_FP
            do i = 1,3
                do j = 1,3
                    fluxSlice(i,j) = fluxQuad(i,j,iDir,iEq)
                end do 
            end do
            averageFlux(iDir,iEq) = simpson2d( fluxSlice(:,:) )
        end do
    end do

end function averageFlux
!-------------------------------------------------------------------------------
!> @purpose 
!>  Loop around the domain and evaluate conservation
!>
!> @history
!>  25 November 2014 - Initial creation (Maeng)
!>  30 October 2015 - simplified
!>
subroutine conservationCheck()

    use meshUtil, only: faceCells, cellVolume, totalVolume, cellFaces
    use physics, only: prim2conserv

    implicit none

    ! Local variables
    integer :: iCell,   & !< cell index
               iEdge,   & !< edge index
               iEq        !< equation index
    
    real(FP) :: primQ(nEqns),   & !< primitive variable sum
                consQ(nEqns)      !< conserved variable sum

    primQ(:) = 0.0_FP
    consQ(:) = 0.0_FP
    do iEq = 1,nEqns
        do iCell = 1,nCells
            primQ(iEq) = primQ(iEq) + (primAvg(iEq,iCell))*cellVolume(iCell) 
            consQ(iEq) = consQ(iEq) + (cellAvg(iEq,iCell))*cellVolume(iCell) 
        end do
        primNorm(iEq) = (primQ(iEq)/totalVolume)**(1.0_FP)
        conservNorm(iEq) = (consQ(iEq)/totalVolume)**(1.0_FP)
    end do
    !write(*,*) 'Conserved variables'
    !write(*,'(*(e24.16e02))') consQ

    ! calculate inconsistency between cell average and primitive states
    deltaAvg = 0.0_FP
    if ( nDim == 2 ) then
        do iCell = 1, nCells
            deltaAvg(:,iCell) = cellAvg(:,iCell) 
            do iEdge = 1,3
                consQ = 0.0_FP
                if ( eqFlag == LINADV_EQ ) then
                    consQ(:) = edgeData(:,cellFaces(iEdge,iCell))  
                else
                    consQ(:) = prim2conserv(nDim,nEqns,edgeData(:,cellFaces(iEdge,iCell)))  
                end if
                deltaAvg(:,iCell) = deltaAvg(:,iCell) - 1.0_FP/3.0_FP*(consQ(:))  
            end do
        end do
    end if

end subroutine conservationCheck
!-------------------------------------------------------------------------------
!> @purpose 
!>  Calculate primitive cell average from conservative variables 
!>  for coupled systems. The primitive cell average are used for 
!>  constructing cell reconstruction coefficients.
!>  
!> @author
!>  J. Brad Maeng
!>
!> @history
!>  17 March 2015 - Initial Creation
!>  21 September 2015 - Implementation of a new conservative 
!>      variable conversion for 1D pressureless euler
!>  23 September 2015 - Implementation of a new conservative
!>      variable conversion for 2D pressureless euler
!>  9 October 2015 - 2d euler equations, polytropic EOS for p
!>
subroutine conserv2primAvg(nDim,nEqns,nNodes,nEdges,nCells, &
                                    qNode,qEdge,qCons,qPrim)

    use solverVars, only: eps
    use meshUtil, only: cellNodes, cellFaces, maxFacesPerCell

    implicit none

    !< Interface variables
    integer, intent(in) :: nDim,    & !< problem dimension
                           nNodes,  & !< number of nodes
                           nEdges,  & !< number of edges
                           nCells,  & !< number of cells
                           nEqns      !< number of equations

    real(FP), intent(in) :: qNode(nEqns,nNodes),  & !< nodeData 
                            qEdge(nEqns,nEdges),  & !< edgeData
                            qCons(nEqns,nCells)     !< conservative state variable

    real(FP), intent(out) :: qPrim(nEqns,nCells)  !< primitive state variable

    !< Local variables
    integer :: iEq,     & !< equation index
               node1,   &
               node2,   &
               node3,   &
               node4,   &
               edge1,   &
               edge2,   &
               edge3,   &
               edge4,   &
               iCell      !< cell index

    real(FP) :: qAvg(nEqns),  & !< local cell average
                rDiff(nDim),  & !< density derivatives
                uDiff(nDim),  & !< u derivatives
                vDiff(nDim),  & !< v derivatives
                sCorrR,       & !< second order vel. correction for conservation 
                sCorrU,       & !< second order vel. correction for conservation 
                sCorrV          !< second order vel. correction for conservation 

    qPrim(:,:) = 0.0_FP
    select case ( nDim )

      case (1)

        select case ( eqFlag )

          case ( PLESSEULER_EQ )
            qAvg = 0.0_FP
            do iCell = 1,nCells
                node1 = cellNodes(1,iCell)
                node2 = cellNodes(2,iCell)
                sCorrU = 1.0_FP/12.0_FP*(qNode(1,node2)-qNode(1,node1))*&
                                        (qNode(2,node2)-qNode(2,node1)) 
                qAvg(1) = qCons(1,iCell) 
                qAvg(2) = 1.0_FP/qCons(1,iCell)*(qCons(2,iCell)-sCorrU)
                qPrim(:,iCell) = qAvg(:) 
            end do

          case default
            ! scalar conservation laws
            qPrim(:,:) = qCons(:,:)
        end select

      case (2)

        select case ( eqFlag )

          case ( PLESSEULER_EQ )
            ! pressureless euler/isentropic euler equations; rho, u, v
            select case ( maxFacesPerCell )

              case ( 3 ) ! triangle
                do iCell = 1,nCells
                    node1 = cellNodes(1,iCell)
                    node2 = cellNodes(2,iCell)
                    node3 = cellNodes(3,iCell)
                    rDiff(1) = qNode(1,node2)-qNode(1,node1) ! drho/dxi
                    rDiff(2) = qNode(1,node3)-qNode(1,node1) ! drho/deta
                    uDiff(1) = qNode(2,node2)-qNode(2,node1)
                    uDiff(2) = qNode(2,node3)-qNode(2,node1)
                    vDiff(1) = qNode(3,node2)-qNode(3,node1)
                    vDiff(2) = qNode(3,node3)-qNode(3,node1)
                    ! correction for velocity
                    sCorrU = 1.0_FP/36.0_FP*( &
                        rDiff(1)*(2.0_FP*uDiff(1)-uDiff(2)) + &
                        rDiff(2)*(2.0_FP*uDiff(2)-uDiff(1)) )
                    sCorrV = 1.0_FP/36.0_FP*( &
                        rDiff(1)*(2.0_FP*vDiff(1)-vDiff(2)) + &
                        rDiff(2)*(2.0_FP*vDiff(2)-vDiff(1)) )
                    qAvg(1) = qCons(1,iCell) ! rho average
                    qAvg(2) = 1.0_FP/qCons(1,iCell)*(qCons(2,iCell)-sCorrU) ! u average
                    qAvg(3) = 1.0_FP/qCons(1,iCell)*(qCons(3,iCell)-sCorrV) ! v average
                    qPrim(1:3,iCell) = qAvg(1:3)
                end do
              
              case ( 4 ) ! quadrilateral
                do iCell = 1,nCells
                    node1 = cellNodes(1,iCell)
                    node2 = cellNodes(2,iCell)
                    node3 = cellNodes(3,iCell)
                    node4 = cellNodes(4,iCell)
                    edge1 = cellFaces(1,iCell)
                    edge2 = cellFaces(2,iCell)
                    edge3 = cellFaces(3,iCell)
                    edge4 = cellFaces(4,iCell)
                    sCorrR = 1.0_FP/16.0_FP*( 36.0_FP*qCons(1,iCell) - (qNode(1,node1) + &
                        qNode(1,node2) + qNode(1,node3) + qNode(1,node4)) - &
                        4.0_FP*(qEdge(1,edge1) + qEdge(1,edge2) + qEdge(1,edge3) + &
                               qEdge(1,edge4)) )
                    sCorrU = 1.0_FP/(sCorrR*16.0_FP)*( 36.0_FP*qCons(2,iCell) - (qNode(1,node1)*qNode(2,node1) + &
                        qNode(1,node2)*qNode(2,node2) + qNode(1,node3)*qNode(2,node3) + &
                        qNode(1,node4)*qNode(2,node4)) - &
                        4.0_FP*(qEdge(1,edge1)*qEdge(2,edge1) + qEdge(1,edge2)*qEdge(2,edge2) + &
                               qEdge(1,edge3)*qEdge(2,edge3) + qEdge(1,edge4)*qEdge(2,edge4)) )
                    sCorrV = 1.0_FP/(sCorrR*16.0_FP)*( 36.0_FP*qCons(3,iCell) - (qNode(1,node1)*qNode(3,node1) + &
                        qNode(1,node2)*qNode(3,node2) + qNode(1,node3)*qNode(3,node3) + &
                        qNode(1,node4)*qNode(3,node4)) - &
                        4.0_FP*(qEdge(1,edge1)*qEdge(3,edge1) + qEdge(1,edge2)*qEdge(3,edge2) + &
                               qEdge(1,edge3)*qEdge(3,edge3) + qEdge(1,edge4)*qEdge(3,edge4)) )
                    qAvg(1) = qCons(1,iCell) ! rho average
                    qAvg(2) = 1.0_FP/36.0_FP*( (qNode(2,node1)+qNode(2,node2)+qNode(2,node3)+qNode(2,node4)) + &
                         4.0_FP*(qEdge(2,edge1)+qEdge(2,edge2)+qEdge(2,edge3)+qEdge(2,edge4)) + 16.0_FP*sCorrU )! u average
                    qAvg(3) = 1.0_FP/36.0_FP*( (qNode(3,node1)+qNode(3,node2)+qNode(3,node3)+qNode(3,node4)) + &
                         4.0_FP*(qEdge(3,edge1)+qEdge(3,edge2)+qEdge(3,edge3)+qEdge(3,edge4)) + 16.0_FP*sCorrV )! v average
                    qPrim(1:3,iCell) = qAvg(1:3)
                end do

            end select

          case default
            ! scalar conservation laws
            qPrim(:,:) = qCons(:,:)

        end select

    end select

end subroutine conserv2primAvg
!-------------------------------------------------------------------------------
!> @purpose 
!>  Update edgeData consistently and conservatively by consulting immediate 
!>   neighboring elements.
!>
!> @history
!>  22 February 2016 - Initial creation (Maeng)
!>  25 April 2016 - Re-examine the formulation 
!>
subroutine reconcileEdgeData( nDim,nEqns,nCells,nNodes,nEdges,qCell,qNode,qEdge )
    
    use solverVars, only: eps, iRight, iLeft
    use physics, only: prim2conserv
    use meshUtil, only: faceCells, cellVolume, cellFaces, cellNodes, &
                        faceNormal, faceArea, faceNodes
    use boundaryConditions, only: edgeBC, periodicID, pEdgePair
    use reconstruction, only: reconValue

    implicit none

    ! Interface variables
    integer, intent(in) :: nDim,     & !< problem dimension
                           nEqns,    & !< number of equations
                           nCells,   & !< number of cells
                           nNodes,   & !< number of nodes
                           nEdges      !< number of edges

    real(FP), intent(in) :: qCell(nEqns,nCells), & 
                            qNode(nEqns,nNodes)       !< node data

    real(FP), intent(inout) :: qEdge(nEqns,nEdges) !< edge data

    ! Local variables
    integer :: iEq,     & !< equation index
               iPt,     & !< point index
               nPts,    & !< number of quadrature point
               iEdge,   & !< edge index
               iCell,   & !< cell index
               lCell,   & !< left cell index
               rCell      !< right cell index

    integer :: edgeL1,   & !<
               edgeL2,   & !<
               edgeL3,   & !<
               edgeR1,   &
               edgeR2,   &
               edgeR3

    integer :: node1, node2, node3, node4 

    real(FP) :: res(nEqns),             & 
                qCIncons(nEqns),        &
                qPIncons(nEqns),        &
                qAvgL(nEqns),           &
                qAvgR(nEqns),           &
                coeffL(10),             &
                coeffR(10),             &
                qNodeEdgeL(nEqns,6),    &
                qNodeEdgeR(nEqns,6),    &
                qCenterL(nEqns),        &
                qCenterR(nEqns),        &
                qPrimAvg(nEqns)

    real(FP) :: dqEdge(nEqns,nEdges)    !< temporary edge data storage

    dqEdge(:,:) = 0.0_FP
    do iEdge = 1, nEdges
        lCell = faceCells(iLeft,iEdge)
        rCell = faceCells(iRight,iEdge) ! ghost cell or physical periodic cell
        node1 = faceNodes(1,iEdge)
        node2 = faceNodes(2,iEdge)

        if ( rCell < 0 ) then 
            if ( edgeBC(iEdge) == periodicID ) then
                ! for periodic BC, negative cell index
                rCell = abs(faceCells(iRight,iEdge))
            else
                ! other boundary, skip 
                cycle
            end if
        end if
        
        ! edge indices
        edgeL1 = cellFaces(1,lCell)
        edgeL2 = cellFaces(2,lCell)
        edgeL3 = cellFaces(3,lCell)
        edgeR1 = cellFaces(1,rCell)
        edgeR2 = cellFaces(2,rCell)
        edgeR3 = cellFaces(3,rCell)
        ! inconsistency between conservative cell average and primitive average
        select case ( eqFlag ) 

            case ( LINADV_EQ )
                qAvgL(:) = 1.0_FP/3.0_FP*(qEdge(:,edgeL1) + qEdge(:,edgeL2) + qEdge(:,edgeL3))
                qAvgR(:) = 1.0_FP/3.0_FP*(qEdge(:,edgeR1) + qEdge(:,edgeR2) + qEdge(:,edgeR3))

            case ( BURGERS_EQ )
                qAvgL(:) = 1.0_FP/3.0_FP*(qEdge(:,edgeL1) + qEdge(:,edgeL2) + qEdge(:,edgeL3))
                qAvgR(:) = 1.0_FP/3.0_FP*(qEdge(:,edgeR1) + qEdge(:,edgeR2) + qEdge(:,edgeR3))

            case ( PLESSEULER_EQ )
                ! density
                qAvgL(1) = 1.0_FP/3.0_FP*(qEdge(1,edgeL1) + qEdge(1,edgeL2) + qEdge(1,edgeL3))
                qAvgR(1) = 1.0_FP/3.0_FP*(qEdge(1,edgeR1) + qEdge(1,edgeR2) + qEdge(1,edgeR3))
                ! momentum
                qAvgL(2) = 1.0_FP/3.0_FP*(qEdge(1,edgeL1)*qEdge(2,edgeL1) + &
                                          qEdge(1,edgeL2)*qEdge(2,edgeL2) + &
                                          qEdge(1,edgeL3)*qEdge(2,edgeL3) )
                qAvgR(2) = 1.0_FP/3.0_FP*(qEdge(1,edgeR1)*qEdge(2,edgeR1) + &
                                          qEdge(1,edgeR2)*qEdge(2,edgeR2) + &
                                          qEdge(1,edgeR3)*qEdge(2,edgeR3) )
                                        
                qAvgL(3) = 1.0_FP/3.0_FP*(qEdge(1,edgeL1)*qEdge(3,edgeL1) + &
                                          qEdge(1,edgeL2)*qEdge(3,edgeL2) + &
                                          qEdge(1,edgeL3)*qEdge(3,edgeL3) )
                qAvgR(3) = 1.0_FP/3.0_FP*(qEdge(1,edgeR1)*qEdge(3,edgeR1) + &
                                          qEdge(1,edgeR2)*qEdge(3,edgeR2) + &
                                          qEdge(1,edgeR3)*qEdge(3,edgeR3) )
                            
        end select

        ! FIXME
        ! evaluate inconsistencies in conserved variables
        ! Linear interpolation - inverse area weighted average
        qCIncons(:) = 1.0_FP/(3.0_FP)*( &
                   ( cellVolume(rCell)*(qCell(:,lCell) - qAvgL(:)) + &
                     cellVolume(lCell)*(qCell(:,rCell) - qAvgR(:)) ) / &
                   ( cellVolume(lCell)+cellVolume(rCell) ) )
        ! original definition
        !qIncons(:) = 1.0_FP/(3.0_FP*2.0_FP)*( &
        !           ( cellVolume(rCell)*(qCell(:,lCell) - qAvgL(:)) + &
        !             cellVolume(lCell)*(qCell(:,rCell) - qAvgR(:)) ) / &
        !           ( cellVolume(lCell)+cellVolume(rCell) ) )

        qPIncons(:) = 0.0_FP
        select case ( eqFlag )
            case ( LINADV_EQ )
                qPIncons(1) = qCIncons(1)
                qPIncons(2:3) = 0.0_FP

            case ( BURGERS_EQ )
                qPIncons(1) = 0.0_FP
                qPIncons(2:3) = qCIncons(2:3)
            
            case ( PLESSEULER_EQ ) 
                ! evaluate primitive dual cell average
                qPrimAvg(:) = qEdge(:,iEdge)

                qPIncons(1) = qCIncons(1)
                qPIncons(2) = (qCIncons(2)-qPrimAvg(2)*qPIncons(1))/qPrimAvg(1)
                qPIncons(3) = (qCIncons(3)-qPrimAvg(3)*qPIncons(1))/qPrimAvg(1)

        end select

        dqEdge(:,iEdge) = qPIncons(:)

    end do 

    ! Apply changes to edgeData
    qEdge(:,:) = qEdge(:,:) + dqEdge(:,:)

    !deallocate( xi, quadL, quadR, qTempL, qTempR )

end subroutine reconcileEdgeData
!-------------------------------------------------------------------------------
!> @purpose 
!>  Calculate the reconstruction coefficients for each equation for a 
!>  given cell.
!>  
!> @author
!>  Timothy A. Eymann
!>
!> @history
!>  29 March 2012 - Initial Creation\n
!>  8 May 2012 - Extend to 1-D
!>  6 January 2016 - Modified to get rid of bubble function (Maeng)
!>
function coefficients(nDim,iEq,iCell)

    use solverVars, only: eps
    use meshUtil, only: cellNodes, cellFaces, maxFacesPerCell
    implicit none

    ! Interface variables
    integer, intent(in) :: nDim,    & !< problem dimension
                           iEq,     & !< number of equations
                           iCell      !< cell index

    ! Function variable
    real(FP) :: coefficients((nDim+2)*(nDim+3)/(2*(3-nDim)))

    ! Local variables
    real(FP) :: uPoint((nDim+2)*(nDim+3)/(2*(3-nDim))),  & !< vector of edge and node values
                uAvg                                       !< cell average

    coefficients = 0.0_FP
    uPoint = 0.0_FP

    uAvg = primAvgN(iEq,iCell)

    select case ( nDim )

        case (1)
           uPoint(1) = edgeDataN(iEq,cellFaces(1,iCell))
           uPoint(2) = edgeDataN(iEq,cellFaces(2,iCell))

        case (2)
          select case ( maxFacesPerCell )
            case ( 3 )
              uPoint(1) = nodeDataN(iEq,cellNodes(1,iCell))
              uPoint(3) = nodeDataN(iEq,cellNodes(2,iCell))
              uPoint(5) = nodeDataN(iEq,cellNodes(3,iCell))

              uPoint(2) = edgeDataN(iEq,cellFaces(1,iCell))
              uPoint(4) = edgeDataN(iEq,cellFaces(2,iCell))
              uPoint(6) = edgeDataN(iEq,cellFaces(3,iCell))
            case ( 4 )
              uPoint(1) = nodeDataN(iEq,cellNodes(1,iCell))
              uPoint(2) = nodeDataN(iEq,cellNodes(2,iCell))
              uPoint(3) = nodeDataN(iEq,cellNodes(3,iCell))
              uPoint(4) = nodeDataN(iEq,cellNodes(4,iCell))

              uPoint(5) = edgeDataN(iEq,cellFaces(1,iCell))
              uPoint(6) = edgeDataN(iEq,cellFaces(2,iCell))
              uPoint(7) = edgeDataN(iEq,cellFaces(3,iCell))
              uPoint(8) = edgeDataN(iEq,cellFaces(4,iCell))
          end select
    end select

    ! set reconstruction coefficients
    coefficients(:) = coeffFromVal(nDim,uAvg,uPoint)

end function coefficients
!-------------------------------------------------------------------------------
!> @purpose 
!>  Calculate the reconstruction coefficients \f$c_i$\f given point values and 
!>  conserved quantity\n
!>  See basis function definitions in 'reconValue' function
!>
!> @author
!>  Timothy A. Eymann
!>
!> @history
!>  4 June 2012 - Initial Creation
!>  27 July 2013 - Add tolerance for bubble coefficient
!>  26 December 2016 - Try out internal numerical quadrature points for triangle
!>                     numerical average (Maeng)
!>
function coeffFromVal(nDim,uAvg,uPoint)

    use solverVars, only: eps
    use meshUtil, only: maxFacesPerCell
    use reconstruction, only: reconValue
    implicit none

    ! Interface variables
    integer, intent(in) :: nDim !< problem dimension

    real(FP), intent(in) :: uPoint((nDim+2)*(nDim+3)/(2*(3-nDim))),  & !< point values
                            uAvg                                       !< primitive average

    ! Function variable
    real(FP) :: uNumAvg,    &
                coeffFromVal((nDim+2)*(nDim+3)/(2*(3-nDim)))

    ! Local variables
    real(FP), parameter :: tol = 2.0e-10_FP !< tolerance for bubble coeff (FIXME)

    coeffFromVal(:) = 0.0_FP
    select case ( nDim )

        case (1)
            coeffFromVal(1) = uPoint(1)
            coeffFromVal(2) = 0.25_FP*(6.0_FP*uAvg-uPoint(1)-uPoint(2))
            coeffFromVal(3) = uPoint(2)

        case (2)
          select case ( maxFacesPerCell )
            case ( 3 ) ! triangle
              coeffFromVal(1) = uPoint(1)
              coeffFromVal(2) = uPoint(2)
              coeffFromVal(3) = uPoint(3)
              coeffFromVal(4) = uPoint(4)
              coeffFromVal(5) = uPoint(5)
              coeffFromVal(6) = uPoint(6)
              ! ORIGINAL: numerical average using lagrange points
              uNumAvg = (1.0_FP/3.0_FP)*(uPoint(2)+uPoint(4)+uPoint(6)) ! 2nd order accurate average

              ! bubble function
              coeffFromVal(7) = (20.0_FP/9.0_FP)*(uAvg - uNumAvg)
              if ( abs(coeffFromVal(7)) <= 1.0_FP*eps ) coeffFromVal(7) = 0.0_FP
              coeffFromVal(8:10) = 0.0_FP

            case ( 4 )  ! quadrilateral
              coeffFromVal(1) = uPoint(1)
              coeffFromVal(2) = uPoint(2)
              coeffFromVal(3) = uPoint(3)
              coeffFromVal(4) = uPoint(4)
              coeffFromVal(5) = uPoint(5)
              coeffFromVal(6) = uPoint(6)
              coeffFromVal(7) = uPoint(7)
              coeffFromVal(8) = uPoint(8)
              coeffFromVal(9) = 1.0_FP/16.0_FP*(36.0_FP*uAvg - &
                  (uPoint(1)+uPoint(2)+uPoint(3)+uPoint(4)) - &
                  4.0_FP*(uPoint(5)+uPoint(6)+uPoint(7)+uPoint(8)) )
              coeffFromVal(10) = 0.0_FP
          end select
    end select

end function coeffFromVal
!-------------------------------------------------------------------------------
!> @purpose 
!>  Test 
!>
!> @author
!>  J. Brad Maeng
!>
!> @history
!>  21 April 2015 - Initial creation
!>
subroutine test()

    use solverVars, only: eps, nIter
    use meshUtil, only: nDim, cellNodes, cellFaces, ref2cart, &
                        nodeCoord, edgeCoord, &
                        faceNodes, faceCells, &
                        cellInvJac, cellJac, cellVolume, &
                        faceNormal, faceArea, &
                        cellCentroid, faceNormalArea, cart2ref
    use reconstruction, only: reconData, reconValue, gradVal, secondGradVal
    use boundaryConditions, only: nodeBC, edgeBC

    implicit none

    !> Local variables
    integer :: iEq,     & !< equation index 
               iCell,   & !< cell index
               iFace,   & !< face index
               iEdge,   & !< edge index
               iNode      !< node index

    real(FP) :: xi(nDim),           & 
                xcart(nDim),        &
                sumNormal(nDim),    &
                vel(nDim),          &
                gradient(nDim),     &
                gradU(nDim,nDim),   &
                sGradU(nDim,nDim),  &
                sGradV(nDim,nDim)

    !write(*,*) "fortran "
    !write(*,*) nCells
    !write(*,*) nEqns
    !write(*,*) shape(nodeDataN), size(nodeDataN)
    !write(*,*) shape(nodeBC), size(nodeBC)
    !write(*,*) nodeDataN(:,124)
    !write(*,*) nodeBC(5)

    !!! check second order gradient evaluation routines !!!
    iCell = 2
    xi = (/0.5_FP, 0.5_FP/)
    write(*,*) 'test', primAvgN(:,iCell)
    write(*,*) 'test', cellAvgN(:,iCell)
    write(*,*) 'x, y',  ref2cart(nDim,iCell,xi)
    xCart = ref2Cart(nDim,iCell,xi)
    write(*,*) 'xi, yi',  cart2ref(nDim,iCell,xcart)

    vel(1) = reconValue( nDim,2,iCell,xi )
    vel(2) = reconValue( nDim,3,iCell,xi )
    write(*,*) 'U, V', vel

    gradU(:,:) = 0.0_FP
    gradU(1,:) = gradVal( nDim, 2, iCell, xi ) 
    !!write(*,*) 'dU/dXi'
    !!gradU(1,:) = matmul(gradient,cellInvJac(:,:,iCell)) ! dv/dx, dv/dy
    !!gradU(1,1) = gradient(1)*cellInvJac(1,1,iCell) + gradient(2)*cellInvJac(2,1,iCell)! du/dx
    !!gradU(1,2) = gradient(1)*cellInvJac(1,2,iCell) + gradient(2)*cellInvJac(2,2,iCell)! du/dy
    !!gradU(1,:) = matmul(cellInvJac(:,:,iCell),gradient) ! dv/dx, dv/dy
    gradU(2,:) = gradVal( nDim, 3, iCell, xi ) 
    !!write(*,*) gradient
    !!gradU(2,:) = matmul(gradient,cellInvJac(:,:,iCell)) ! dv/dx, dv/dy
    !!gradU(2,1) = gradient(1)*cellInvJac(1,1,iCell) + gradient(2)*cellInvJac(2,1,iCell)! du/dx
    !!gradU(2,2) = gradient(1)*cellInvJac(1,2,iCell) + gradient(2)*cellInvJac(2,2,iCell)! du/dy
    !!gradU(2,:) = matmul(cellInvJac(:,:,iCell),gradient) ! dv/dx, dv/dy
    !write(*,*) 'dU/dX'
    write(*,*) 
    write(*,*) gradU(1,1), gradU(1,2)
    write(*,*) gradU(2,1), gradU(2,2)

    !write(*,*)
    !write(*,*) cellJac(1,:,iCell) 
    !write(*,*) cellJac(2,:,iCell) 
    !write(*,*)
    !write(*,*) cellInvJac(1,:,iCell) 
    !write(*,*) cellInvJac(2,:,iCell) 
    !!sGradU = secondGradVal( nDim,2,iCell,xi )
    !write(*,*) 'd2U/dX2'
    !write(*,*) sGradU(1,1), sGradU(1,2)
    !write(*,*) sGradU(2,1), sGradU(2,2)

    !sGradV = secondGradVal( nDim,3,iCell,xi )
    !write(*,*) 'd2V/dX2'
    !write(*,*) sGradV(1,1), sGradV(1,2)
    !write(*,*) sGradV(2,1), sGradV(2,2)

    !! check gradient evaluation routines !!!
    !iCell = 10
    !xi = (/1.0_FP, 0.0_FP/)
    !write(*,*) 'x, y',  ref2cart(nDim,iCell,xi)
    !write(*,*) 'eps', eps
    !gradient = gradVal( nDim, 2, iCell, xi ) 
    !write(*,*) 'dU/dXi'
    !write(*,*) gradient
    !gradU(1,:) = matmul(gradient,cellInvJac(:,:,iCell)) ! du/dx, du/dy
    !gradient = gradVal( nDim, 3, iCell, xi ) 
    !write(*,*) gradient
    !gradU(2,:) = matmul(gradient,cellInvJac(:,:,iCell)) ! dv/dx, dv/dy
    !write(*,*) 
    !write(*,*) 'cellInvJac'
    !write(*,*) cellInvJac(1,1,iCell), cellInvJac(1,2,iCell)
    !write(*,*) cellInvJac(2,1,iCell), cellInvJac(2,2,iCell)
    !write(*,*)
    !write(*,*) 'dU/dXi * cellInvJac'
    !write(*,*) 'du/dx, du/dy'
    !write(*,*) gradU(1,1), gradU(1,2)
    !write(*,*) 'dv/dx, dv/dy'
    !write(*,*) gradU(2,1), gradU(2,2)

    !gradient = gradVal( nDim, 2, iCell, xi ) 
    !gradU(1,1) = cellInvJac(1,1,iCell)*gradient(1) + &
    !    cellInvJac(2,1,iCell)*gradient(2)
    !gradU(1,2) = cellInvJac(1,2,iCell)*gradient(1) + &
    !    cellInvJac(2,2,iCell)*gradient(2)    
    !gradient = gradVal( nDim, 3, iCell, xi ) 
    !!gradU(2,:) = matmul(cellInvJac(:,:,iCell),gradient) ! dv/dx, dv/dy
    !gradU(2,1) = cellInvJac(1,1,iCell)*gradient(1) + &
    !    cellInvJac(2,1,iCell)*gradient(2)
    !gradU(2,2) = cellInvJac(1,2,iCell)*gradient(1) + &
    !    cellInvJac(2,2,iCell)*gradient(2)    
    !write(*,*)
    !write(*,*)
    !write(*,*) 'dU/dXi * cellInvJac by manual multiplication'
    !write(*,*) 'du/dx, du/dy'
    !write(*,*) gradU(1,1), gradU(1,2)
    !write(*,*) 'dv/dx, dv/dy'
    !write(*,*) gradU(2,1), gradU(2,2)
    !! check gradient evaluation routines !!!
    
end subroutine test
!-------------------------------------------------------------------------------
end module update
!-------------------------------------------------------------------------------
