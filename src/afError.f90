!-------------------------------------------------------------------------------
!> @purpose 
!>  Stand-alone error analysis program for use with active-flux solver.
!>  Assumes first and last iterations in file are at the same location
!>
!> @history
!>  18 April 2012 - Initial creation (Eymann)
!>  8 October 2013 - Added error data writing option (Maeng)
!>  22 January 2015 - Burgers' equations (Maeng)
!>  30 January 2015 - pressureless Euler equations (Maeng)
!>  23 March 2015 - added a routine to evaluate error for 
!>                   conserved variables (Maeng)
!>  20 September 2016 - Integrate all advective systems (Maeng)
!>  24 October 2016 - Quadrilateral error evaluation (Maeng)
!>
program afError

    use solverVars
    use update, only: eqFlag, LINADV_EQ, PLESSEULER_EQ, &
                      BURGERS_EQ, cellAvgN
    use meshUtil, only: nDim, nNodes, nEdges, nFaces, nCells, &
                        totalVolume, cellVolume, connect, &
                        maxFacesPerCell, cellFaces
    use mathUtil, only: numAverage, numAverageQuadril
    use physics, only: prim2conserv
    use inputOutput, only: reconFile, reconFileName, getDriverFile, &
                           parseDriverFile, deallocateMeshVars, &
                           filePath, drvFileName, getIterNum, &
                           skipConnectivity, skipData, readData
    use postProc

    implicit none

    ! Local variables
    integer :: DOF,         & !< number of degrees of freedom 
               nPts,        & !< number of integration points
               nRec,        & !< number of reconstruction coef.
               nPtsQ,       & !< number of integration points, quadratic
               maxIter,     & !< maximum iteration number in file
               iPt,         &
               jPt,         &
               iEq,         &
               iErr,        & !< error flag
               iCell,       & !< cell index
               iter           !< iteration number

    real(FP) :: solTime       !< final solution time

    real(FP) :: h,          & !< mesh spacing
                area,       & !< domain area
                quad,       & !< quadrature sum
                errSum        !< sum of error terms

    real(FP), allocatable :: l1err(:),          & !< L1 error of cell reconstruction
                             l2err(:),          & !< L2 error of cell reconstruction
                             conservL1err(:),   & !< L1 error of conserved variables
                             conservL2err(:),   & !< L2 error of conserved variables
                             cellAvgRef(:,:),   & !< Reference cell averages at final time
                             dunPt0(:,:,:),     & !< initial value at cell Dunavant point
                             dunPt(:,:,:),      & !< final value at cell Dunavant point
                             dunPtRef(:,:,:),   & !< Reference at cell Dunavant point
                             recPt0(:,:,:),    & !< initial reconstruction coefficients
                             recPt(:,:,:),     & !< final reconstruction coefficients
                             recPtRef(:,:,:),  & !< final reconstruction coefficients
                             conservDunPt(:,:,:),  & !< Reference conserved value at cell Dunavant point
                             conservDunPtRef(:,:,:)  !< Reference conserved value at cell Dunavant point

    character(120) :: varfmt,            &
                      errFileName,       &
                      errFileName2,      &
                      cErrFileName

    integer :: errFile = 180,   & !< error data file unit
               cErrFile = 184     !< conservative error data file unit 

    !integer :: errFile2 = 183

    ! get connectivity information/allocate data
    call calcConstants
    call getDriverFile
    call parseDriverFile 
    call connect

    ! Open a file to store error data
    errFileName = 'errData.dat'
    errFileName = trim(adjustl(filePath))//'/'//trim(adjustl(errFileName))
    open( unit = errFile, file = errFileName, form = 'formatted', &
          access = 'append', iostat = iErr  )
    if ( iErr /= 0 ) then
        write(*,*) 'ERROR: opening ', trim(adjustl(errFileName))
        stop
    end if
    cErrFileName = 'conserverrData.dat'
    cErrFileName = trim(adjustl(filePath))//'/'//trim(adjustl(cerrFileName))
    open( unit = cErrFile, file = cErrFileName, form = 'formatted', &
          access = 'append', iostat = iErr  )
    if ( iErr /= 0 ) then
        write(*,*) 'ERROR: opening ', trim(adjustl(cErrFileName))
        stop
    end if

    !errFileName2 = 'errData2.dat'
    !errFileName2 = trim(adjustl(filePath))//'/'//trim(adjustl(errFileName2))
    !open( unit = errFile2, file = errFileName2, form = 'formatted', &
    !      access = 'append', iostat = iErr  )
    !if ( iErr /= 0 ) then
    !    write(*,*) 'ERROR: opening ', trim(adjustl(errFileName2))
    !    stop
    !end if

    ! Get number of iterations in file
    open( unit = reconFile, file = trim(adjustl(reconFileName)), form = 'unformatted', &
         access = 'stream', status = 'OLD', iostat = iErr )
    if ( iErr /= 0 ) then
        write(*,*) 'ERROR: opening ', trim(adjustl(reconFileName))
        stop
    end if
    maxIter = getIterNum(reconFile)

    !write(*,*) maxIter
    ! set number of integration points
    if ( nDim == 2 ) then
        nPts = 16    !< for conservative averaged quantity
        nPtsQ = 6        
    else
        nPts = 4
        nPtsQ = 3
    end if

    allocate( cellAvgRef(nEqns,nCells), &
              l1err(nEqns), l2err(nEqns), &
              conservL1err(nEqns), conservL2err(nEqns) )

    select case ( maxFacesPerCell )
      case ( 4 ) ! quadrilateral 
        allocate( dunPt0(nEqns,nPts*nPts,nCells), dunPt(nEqns,nPts*nPts,nCells), &
                  dunPtRef(nEqns,nPts*nPts,nCells), &
                  recPt0(nEqns,nPtsQ*nPtsQ,nCells), recPt(nEqns,nPtsQ*nPtsQ,nCells), &
                  recPtRef(nEqns,nPtsQ*nPtsQ,nCells), &
                  conservDunPt(nEqns,nPts*nPts,nCells), conservDunPtRef(nEqns,nPts*nPts,nCells) )
      case default
        allocate( dunPt0(nEqns,nPts,nCells), dunPt(nEqns,nPts,nCells), &
                  dunPtRef(nEqns,nPts,nCells), &
                  recPt0(nEqns,nPtsQ,nCells), recPt(nEqns,nPtsQ,nCells), &
                  recPtRef(nEqns,nPtsQ,nCells), &
                  conservDunPt(nEqns,nPts,nCells), conservDunPtRef(nEqns,nPts,nCells) )
    end select

    ! store values at integration points for initial data
    rewind(reconFile)

    ! iteration 0 - output 1
    call skipConnectivity(reconFile)
    call readData(reconFile)
    ! initial data
    call intData(nDim,nEqns,nPts,nCells,dunPt0) 
    call recCoefData(nDim,nEqns,nPtsQ,nCells,recPt0) 
    
    ! skip to last iteration in file (maxIter-2 since read iter 0 already)
    do iter = 1, maxIter-2
        iErr = skipData(reconFile)
    end do

    ! final iteration - final output
    call readData(reconFile,iter,solTime)
    write(*,*) 'solTime:', solTime, 'iter: ', iter
    call intData(nDim,nEqns,nPts,nCells,dunPt) 
    call recCoefData(nDim,nEqns,nPtsQ,nCells,recPt) 

    ! Reference numerical/analytic solution to compare numerical solution
    if ( eqFlag == LINADV_EQ ) then
        dunPtRef(:,:,:) = dunPt0(:,:,:)     ! initial solution
        !recPtRef(:,:,:) = recPt0(:,:,:) 
    else
        ! pressureless or burgers
        dunPtRef(:,:,:) = iterateSol(nDim,nEqns,nPts,nCells,dunPt,solTime) ! iterated solution data
        !! FIXME: 
        !recPtRef(:,:,:) = iterateSol(nDim,nEqns,nPtsQ,nCells,recPt,solTime,nPtsQ)  ! lagrange points
    end if

    ! compute error in reconstructions at final step
    l1err(:) = cellReconError(nDim,nEqns,nPts,nCells,dunPt,dunPtRef,1.0)
    l2err(:) = cellReconError(nDim,nEqns,nPts,nCells,dunPt,dunPtRef,2.0)
    !l1err(:) = cellRecCoefError(nDim,nEqns,nPtsQ,nCells,recPt,recPtRef,1.0)
    !l2err(:) = cellRecCoefError(nDim,nEqns,nPtsQ,nCells,recPt,recPtRef,2.0)

    select case ( maxFacesPerCell )

      case ( 4 ) ! quadrilateral
        ! calculate reference cell average
        do iCell = 1,nCells
            do jPt = 1,nPts
                do iPt = 1,nPts
                    ! assume quadratic variation of conserved variables
                    if ( eqFlag == PLESSEULER_EQ ) then 
                        conservDunPt(:,jPt+nPts*(iPt-1),iCell) = &
                            prim2conserv(nDim,nEqns,dunPt(:,jPt+nPts*(iPt-1),iCell))
                        conservDunPtRef(:,jPt+nPts*(iPt-1),iCell) = &
                            prim2conserv(nDim,nEqns,dunPtRef(:,jPt+nPts*(iPt-1),iCell))
                    else
                        conservDunPt(:,jPt+nPts*(iPt-1),iCell) = &
                            dunPt(:,jPt+nPts*(iPt-1),iCell)
                        conservDunPtRef(:,jPt+nPts*(iPt-1),iCell) = &
                            dunPtRef(:,jPt+nPts*(iPt-1),iCell)
                    end if
                end do
            end do
            do iEq = 1,nEqns
                cellAvgRef(iEq,iCell) = numAverageQuadril(nDim,nPts,conservDunPtRef(iEq,:,iCell))
            end do
        end do

      case default ! 2D triangle and 1D
        ! calculate reference cell average
        do iCell = 1,nCells
            do iPt = 1,nPts
                ! assume quadratic variation of conserved variables
                if ( eqFlag == PLESSEULER_EQ ) then 
                    conservDunPt(:,iPt,iCell) = prim2conserv(nDim,nEqns,dunPt(:,iPt,iCell))
                    conservDunPtRef(:,iPt,iCell) = prim2conserv(nDim,nEqns,dunPtRef(:,iPt,iCell))
                else
                    conservDunPt(:,iPt,iCell) = dunPt(:,iPt,iCell)
                    conservDunPtRef(:,iPt,iCell) = dunPtRef(:,iPt,iCell)
                end if
            end do
            do iEq = 1,nEqns
                cellAvgRef(iEq,iCell) = numAverage(nDim,nPts,conservDunPtRef(iEq,:,iCell))
            end do
        end do

    end select
    ! compute error in conservative averages
    conservL1err(:) = cellAvgError(nDim,nEqns,nCells,cellAvgN,cellAvgRef,1.0)
    conservL2err(:) = cellAvgError(nDim,nEqns,nCells,cellAvgN,cellAvgRef,2.0)

    ! output error
    !write(errFile,*) '% nCell, DOF, 1/sqrt(DOF), L1err, L2err' 
    !write(cErrFile,*) '% nCell, DOF, 1/sqrt(DOF), L1err, L2err' 
    select case ( nDim )
      case ( 1 )
        ! 1D
        ! calculate DOF and h
        DOF = nCells+(nNodes/2)
        h = 1.0_FP/real(DOF,FP)

        write(varfmt,'(a,i0,a)') '(i7,i7,e24.15e2,', 2*nEqns, 'e24.15e2,a40)'
        write(*,*) 'primitive variables'
        write(*,varfmt) nCells, DOF, h, l1err,l2err
        write(errFile,varfmt) nCells, DOF, h, &
                              l1err,l2err,'%'//trim(adjustl(drvFileName))  
        write(*,*) 'conserved variables'
        write(*,varfmt) nCells, DOF, h, conservL1err,conservL2err
        write(cErrFile,varfmt) nCells, DOF, h, &
                               conservL1err,conservL2err,'%'//trim(adjustl(drvFileName)) 
      case ( 2 ) 
        ! 2D
        ! calculate DOF and h
        select case ( maxFacesPerCell )
          case ( 3 ) ! triangles 
            DOF = nCells+(nNodes/6)+(nEdges/2)
          case ( 4 ) ! quadrilateral
            DOF = nCells+(nNodes/4)+(nEdges/2)
        end select
        !DOF = nCells+nNodes+nEdges
        h = sqrt(1.0_FP/real(DOF,FP))

        write(varfmt,'(a,i0,a)') '(i7,i7,e24.15e2,', 2*nEqns,'e24.15e2,a80)'
        write(*,*) 'primitive variables'
        write(*,varfmt) nCells, DOF, h, l1err,l2err
        write(errFile,varfmt) nCells, DOF, h, &
                              l1err,l2err,'%'//trim(adjustl(drvFileName))

        write(*,*) 'conserved variables'
        write(*,varfmt) nCells, DOF, h, conservL1err,conservL2err
        write(cErrFile,varfmt) nCells, DOF, h, & 
                               conservL1err,conservL2err,'%'//trim(adjustl(drvFileName))   

    end select

    ! close files after writing
    close(reconFile)
    close(errFile)
    close(cErrFile)
    !close(errFile2)

    deallocate( dunPt0, dunPt, dunPtRef, &
                recPt0, recPt, recPtRef, &
                cellAvgRef, conservDunPt, conservDunPtRef, & 
                l1err, l2err, conservL1err, conservL2err )

    call deallocateMeshVars

end program afError
!-------------------------------------------------------------------------------
