!-------------------------------------------------------------------------------
!> @purpose 
!>  Functions for boundary conditions
!>
!> @author
!>  Timothy A. Eymann
!>
!> @history
!>  20 September 2016 - Integrate all advective systems (Maeng)
!>
module boundaryConditions

    use solverVars, only: inLength, FP

    integer, parameter :: internalID = 0,   & !< internal node/edge
                          periodicID = 1,   & !< periodic bc type
                          outflowID = 2,    & !< outflow
                          wallID = 3,       & !< outflow
                          specFuncID = 4,   & !< use specified function
                          initSolnID = 5      !< use initial solution function
                          !rotPeriodicID = 5,& !< rotating periodic bc type

    integer, allocatable :: patchType(:),   & !< bc associated with each id
                            patchID(:),     & !< array of patch id numbers
                            edgePatch(:),   & !< patch associated to edge
                            nodeBC(:),      & !< bc type associated with node
                            edgeBC(:),      & !< bc type associated with edge midpt
                            pNodePair(:,:), & !< periodic node pair
                            pEdgePair(:)      !< periodic edge pair

    character(inLength) :: bcFunction !< specified boundary function

contains
!-------------------------------------------------------------------------------
!> @purpose
!>  Update node and edge values using specified boundary conditions
!>
!> @author
!>  Timothy A. Eymann
!>
!> @history
!>  29 March 2012 - Initial creation
!>  13 December 2013 - Adaptation for 2D linear advection (Maeng)
!>  25 February 2015 - pointer for state variables (Maeng)
!>  6 October 2015 - pointer for updated flag (Maeng)
!>  31 January 2017 - assumed shape array for states 
!>
subroutine applyBC(nEqns,iNodeEdge,signal,q,dt,updated,iter)

    use solverVars, only: tSim

    implicit none

    ! Interface variables
    integer, intent(in) :: nEqns,       & !< number of equations
                           iNodeEdge      !< node or edge index

    real(FP), intent(in) :: signal(nEqns), & !< change in state at node/edge
                            dt               !< time step

    real(FP), intent(inout) :: q(:,:) !< state

    logical, intent(inout), optional :: updated(:) !< flag that node/edge updated

    integer, intent(in), optional :: iter     !< iteration number

    ! Local variables
    integer :: iEdge,   & !< edge index
               iNode      !< node index

    real(FP) :: t         !< simulation time

    ! calculate the simulation time
    t = tSim + dt

    if ( iNodeEdge > 0 ) then

        iNode = iNodeEdge

        select case ( nodeBC(iNode) )

            case ( periodicID )
                ! need updated 
                call applyPeriodicSignal(nEqns,iNodeEdge,signal,q,updated)
            !case ( rotPeriodicID )
            !call applyRotatingPeriodicSignal(nEqns,iNodeEdge,signal,q,updated)

            case ( outflowID )
                call outflowBC(nEqns,iNodeEdge,q,updated)

            case ( wallID )
                call wallBC(nEqns,iNodeEdge,q,updated)

            case ( specFuncID )
                call specifyBC(nEqns,iNodeEdge,q,t,updated)

            case ( initSolnID )
                call specifyBC(nEqns,iNodeEdge,q,t,updated)

            case default
                continue

        end select

    else

        iEdge = abs(iNodeEdge)

        select case ( edgeBC(iEdge) )

            case ( periodicID )
                call applyPeriodicSignal(nEqns,iNodeEdge,signal,q,updated)
            !case ( rotPeriodicID )
            !call applyRotatingPeriodicSignal(nEqns,iNodeEdge,signal,q,updated)

            case ( outflowID )
                call outflowBC(nEqns,iNodeEdge,q,updated)

            case ( wallID )
                call wallBC(nEqns,iNodeEdge,q,updated)

            case ( specFuncID )
                call specifyBC(nEqns,iNodeEdge,q,t,updated)

            case ( initSolnID )
                call specifyBC(nEqns,iNodeEdge,q,t,updated)

            case default
                continue

        end select

    end if

end subroutine applyBC
!-------------------------------------------------------------------------------
!> @purpose 
!>  Search opposite side of domain to find periodic neighbor cell.  Routine
!>  assumes a rectangular grid aligned with Cartesian axes.  Prime candidate
!>  for efficiency improvements
!>
!> @author
!>  Timothy A. Eymann
!>
!> @history
!>  27 March 2012 - Initial creation
!>  7 June 2012 - Add code for storing node neighbors
!>
subroutine setPeriodicNeighbor(perPatches)

    use meshUtil
    use solverVars, only: iLeft, iRight, eps
    implicit none

    ! Interface variables
    integer, intent(in) :: perPatches(2)  !< list of patches in periodic bound

    ! Local variables
    integer, parameter :: ix = 1,   & !< index for x direction
                          iy = 2      !< index for y direction

    real(FP), parameter :: coordTol = 1.0e-10_FP !< tolerance for coordinate matches

    integer :: iFace,       & !< face id
               jFace,       & !< face id
               iNode,       & !< node index
               gNode,       & !< global node index
               oppNode,     & !< opposite node global index
               iPatch,      & !< patch id
               matchDim       !< dimension to match

    real(FP) :: x(nDim,maxNodesPerFace) !< coordinates of face nodes


    ! flag node and edge bc
    do iFace = 1, nFaces

        if ( (faceCells(iRight,iFace) == -perPatches(1)) .or. &
             (faceCells(iRight,iFace) == -perPatches(2)) ) then

            edgePatch(iFace) = abs(faceCells(iRight,iFace))
            edgeBC(iFace) = periodicID
            do iNode = 1, maxNodesPerFace
                nodeBC(faceNodes(iNode,iFace)) = periodicID
            end do

        end if

    end do

    ! match up faces across domain
    select case ( nDim )

        case (1)
            ! assumes ordered numbering of cells
            faceCells(iRight,1) = -nCells
            faceCells(iRight,nFaces) = -1

            pNodePair(1,nNodes) = 1
            pNodePair(1,1) = nNodes

            pEdgePair(nFaces) = 1
            pEdgePair(1) = nFaces

        case (2)
            do iFace = 1, nFaces-1
                if ( (faceCells(iRight,iFace) == -perPatches(1)) .or. &
                     (faceCells(iRight,iFace) == -perPatches(2)) ) then

                    if ( faceCells(iRight,iFace) == -perPatches(1)) then
                        iPatch = perPatches(2)
                    else
                        iPatch = perPatches(1)
                    end if

                    x(:,:) = nodeCoord(:,faceNodes(:,iFace))

                    ! determine if edge is horizontal or vertical
                    ! assumes that all edges and nodes are aligned with cartesian coordinate
                    !if ( abs(x(ix,1)-x(ix,2)) <= 10.0*eps ) then
                    if ( abs(x(ix,1)-x(ix,2)) <= coordTol ) then
                        ! edge is vertical
                        matchDim = iy
                    else
                        ! edge is horizontal
                        matchDim = ix
                    end if

                    do jFace = iFace+1, nFaces
                        if ( faceCells(iRight,jFace) == -iPatch ) then

                            ! if edge coordinates match store neighbor info
                            !   ASSUMES EQUAL SPACING ALONG EDGES
                            if ( abs(edgeCoord(matchDim,iFace) - &
                                     edgeCoord(matchDim,jFace)) <= coordTol ) then

                                ! set neighbor cell
                                faceCells(iRight,iFace) = &
                                    -faceCells(iLeft,jFace)

                                faceCells(iRight,jFace) = &
                                    -faceCells(iLeft,iFace)

                                ! store periodic edges
                                pEdgePair(iFace) = jFace
                                pEdgePair(jFace) = iFace

                                ! store periodic nodes
                                do iNode = 1, 2
                                    gNode = faceNodes(iNode,iFace)
                                    oppNode = faceNodes(3-iNode,jFace)

                                    if ( pNodePair(1,gNode) == 0 ) then
                                        pNodePair(1,gNode) = oppNode
                                    elseif ( pNodePair(1,gNode) /= oppNode ) then
                                        pNodePair(2,gNode) = oppNode
                                    end if

                                    if ( pNodePair(1,oppNode) == 0 ) then
                                        pNodePair(1,oppNode) = gNode
                                    elseif ( pNodePair(1,oppNode) /= gNode ) then
                                        pNodePair(2,oppNode) = gNode
                                    end if

                                end do
                            end if
                        end if

                    end do


                end if
            end do

    end select

    !do iFace = 61,62
    !write(*,*) iFace, pEdgePair(iFace) !pNodePair(2,faceNodes(1,iFace)), pNodePair(2,faceNodes(2,iFace))
    !end do
    !do iNode = 1,8
    !if ( iNode = 3 ) then 
    !iNode = 3
        !write(*,*) iNode, pNodePair(1,iNode), pNodePair(2,iNode)
    !end if

end subroutine setPeriodicNeighbor
!-------------------------------------------------------------------------------
!> @purpose 
!>  Ensure periodic assignament worked
!>
!> @author
!>  Timothy A. Eymann
!>
!> @history
!>  27 October 2012 - Initial creation
!>
subroutine testPeriodicAssignment

    use solverVars, only: eps
    use meshUtil, only: nFaces, nNodes, edgeCoord, xMax, nDim
    implicit none

    ! Local variables
    integer, parameter :: ix = 1,   & !< x-coordinate
                          iy = 2      !< y-coordinate

    integer :: iFace,   & !< face index
               jFace,   & !< face index
               iNode,   & !< node index
               jNode,   & !< node count
               matchDim   !< matching dimension


    do iFace = 1, nFaces
        if ( edgeBC(iFace) == periodicID ) then
            if ( pEdgePair(iFace) == 0 ) then
                write(*,'(a,i0,a)') 'ERROR: edge ',iFace, &
                                    ' has no periodic match'

                write(*,*) 'edge coordinates: ',edgeCoord(:,iFace)
                write(*,*) 'possible candidates...'

                if ( (abs(edgeCoord(1,iFace))-xMax(1)) <= eps ) then
                    matchDim = iy
                else
                    matchDim = ix
                end if

                do jFace = 1, nFaces
                    if (( edgeBC(jFace) == periodicID ) .and. &
                       ( abs( edgeCoord(matchDim,iface) - &
                              edgeCoord(matchDim,jface)) <= 1.0e-6 ) ) then
                        write(*,'(i0,3e18.6e2)') jFace, edgeCoord(:,jFace), &
                                   abs(edgeCoord(matchDim,iFace) - &
                                       edgeCoord(matchDim,jFace))
                    end if

                end do
                stop
            end if
        end if
    end do
    jNode = 0
    do iNode = 1, nNodes
        if ( nodeBC(iNode) == periodicID ) then
            if ( pNodePair(1,iNode) == 0 ) then
                write(*,'(a,i0,a)') 'ERROR: node ',iNode, &
                                    ' has no periodic match'
                stop
            elseif ( nDim > 1 ) then
                if ( pNodePair(2,iNode) /= 0 ) then
                    jNode = jNode + 1
                end if
            end if
        end if
    end do
    if ( nDim > 1 ) then
        if ( jNode < 4 ) then
            write(*,*) 'WARNING: some corner nodes only have one match.'
            !stop
        end if
    end if

end subroutine testPeriodicAssignment
!-------------------------------------------------------------------------------
!> @purpose 
!>  Apply rotating flow signal to periodically-matched nodes and edges
!>  by manipulating velocity signals. Only work for systems with velocities.
!>
!> @history
!>  9 November 2015 - Initial creation (Maeng)
!>  31 January 2017 - assumed shape array for states 
!>
subroutine applyRotatingPeriodicSignal(nEqns,iNodeEdge,signal,q,updated)

    use meshUtil, only: nDim, nodeCoord, edgeCoord

    implicit none

    ! Interface variables
    integer, intent(in) :: nEqns,       & !< number of equations
                           iNodeEdge      !< node/edge index

    real(FP), intent(in) :: signal(nEqns) !< change in state at node/edge

    real(FP), intent(inout) :: q(:,:) !< state

    logical, intent(inout) :: updated(:) !< flag that node/edge updated

    ! Local variables
    integer :: jNode,   & !< node index
               kNode,   & !< node index
               pEdge,   & !< matching periodic edge
               pNode,   & !< matching periodic node
               pNode2     !< second matching periodic node

    logical :: cornerUpdated !< flag that signal propagated to corner

    cornerUpdated = .false.
        
    if ( iNodeEdge > 0 ) then ! apply signal to nodes
        cornerUpdated = .false.

        ! corner nodes have more than one periodic neigbor
        do jNode = 1,nDim
            pNode = pNodePair(jNode,iNodeEdge)
            
            if ( (pNode > 0) .and. (updated(iNodeEdge)) ) then

                ! change the sign of signals for boundaries for rotating flow 
                if ( nodeCoord(1,iNodeEdge) == nodeCoord(1,pNode) ) then
                    ! x-coord matching
                    q(1,pNode) = q(1,pNode) + signal(1)
                    q(2,pNode) = q(2,pNode) - signal(2)
                    q(3,pNode) = q(3,pNode) + signal(3)
                    q(4,pNode) = q(4,pNode) + signal(4)
                else
                    ! y-coord matching
                    q(1,pNode) = q(1,pNode) + signal(1)
                    q(2,pNode) = q(2,pNode) + signal(2)
                    q(3,pNode) = q(3,pNode) - signal(3)
                    q(4,pNode) = q(4,pNode) + signal(4)
                end if
                updated(pNode) = .true.
                do kNode = 1,nDim
                    pNode2 = pNodePair(kNode,pNode)
                    ! make sure that neighbors of neigbors only updated once
                    if ( ( pNode2 /= iNodeEdge ) .and. ( pNode2 > 0 ) .and.&
                         ( .not. cornerUpdated) ) then
                        q(1,pNode2) = q(1,pNode2) + signal(1)
                        q(2,pNode2) = q(2,pNode2) - signal(2)
                        q(3,pNode2) = q(3,pNode2) - signal(3)
                        q(4,pNode2) = q(4,pNode2) + signal(4)
                        cornerUpdated = .true.
                        updated(pNode2) = .true.
                    end if
                end do
            end if
        end do
    else ! apply signal to edges

        pEdge = pEdgePair(abs(iNodeEdge))
        if ( pEdge > 0 .and. updated(abs(iNodeEdge)) ) then

            ! change the sign of signals for boundaries for rotating flow 
            if ( edgeCoord(1,abs(iNodeEdge)) == edgeCoord(1,pEdge) ) then
                ! x-coord matching
                q(1,pEdge) = q(1,pEdge) + signal(1)
                q(2,pEdge) = q(2,pEdge) - signal(2)
                q(3,pEdge) = q(3,pEdge) + signal(3)
                q(4,pEdge) = q(4,pEdge) + signal(4)
            else
                ! y-coord matching
                q(1,pEdge) = q(1,pEdge) + signal(1)
                q(2,pEdge) = q(2,pEdge) + signal(2)
                q(3,pEdge) = q(3,pEdge) - signal(3)
                q(4,pEdge) = q(4,pEdge) + signal(4)
            end if
            updated(pEdge) = .true.
        end if

    end if

end subroutine applyRotatingPeriodicSignal
!-------------------------------------------------------------------------------
!> @purpose 
!>  Apply signal to periodically-matched nodes and edges
!>
!> @history
!>  17 May 2012 - Initial creation (Eymann)
!>  25 February 2015 - pointer for state variables (Maeng)
!>  6 October 2015 - pointer for updated flag (Maeng)
!>  6 October 2015 - updated flag for node/edge to eliminate duplicate update
!>  31 January 2017 - Assumed shape array for states
!>
subroutine applyPeriodicSignal(nEqns,iNodeEdge,signal,q,updated)

    use meshUtil, only: nDim

    implicit none

    ! Interface variables
    integer, intent(in) :: nEqns,       & !< number of equations
                           iNodeEdge      !< node/edge index

    real(FP), intent(in) :: signal(nEqns) !< change in state at node/edge

    real(FP), intent(inout) :: q(:,:) !< state

    logical, intent(inout), optional :: updated(:) !< flag that node/edge updated

    ! Local variables
    integer :: jNode,   & !< node index
               kNode,   & !< node index
               pEdge,   & !< matching periodic edge
               pNode,   & !< matching periodic node
               pNode2     !< second matching periodic node

    logical :: cornerUpdated = .false. !< flag that signal propagated to corner
        
    if ( iNodeEdge > 0 ) then ! apply signal to nodes
        cornerUpdated = .false.
        
        ! corner nodes have more than one periodic neigbor
        do jNode = 1,nDim
            pNode = pNodePair(jNode,iNodeEdge)
            
            if ( (pNode > 0) .and. (updated(iNodeEdge)) ) then
                q(:,pNode) = q(:,pNode) + signal
                updated(pNode) = .true.
                do kNode = 1,nDim
                    pNode2 = pNodePair(kNode,pNode)
                    
                    ! make sure that neighbors of neigbors only updated once
                    if ( ( pNode2 /= iNodeEdge ) .and. ( pNode2 > 0 ) .and.&
                         ( .not. cornerUpdated) ) then
                        q(:,pNode2) = q(:,pNode2) + signal
                        cornerUpdated = .true.
                        updated(pNode2) = .true.
                    end if
                end do
            end if
        end do

    else ! apply signal to edges

        pEdge = pEdgePair(abs(iNodeEdge))
        if ( pEdge > 0 .and. updated(abs(iNodeEdge)) ) then
            q(:,pEdge) = q(:,pEdge) + signal
            updated(pEdge) = .true.
        end if

    end if

end subroutine applyPeriodicSignal
!-------------------------------------------------------------------------------
!> @purpose 
!>  Update node and edge values using a specified function
!>
!> @author
!>  Timothy A. Eymann
!>
!> @history
!>  20 July 2012 - Initial creation
!>  13 December 2013 - Adaptation for 2D linear advection (Maeng)
!>  25 February 2015 - pointer for state variables (Maeng)
!>  6 October 2015 - pointer for updated flag (Maeng)
!>  31 January 2017 - Assumed shape array for states
!>
subroutine specifyBC(nEqns,iNodeEdge,q,t,updated)

    use analyticFunctions
    use meshUtil, only: nDim, nodeCoord, edgeCoord
    implicit none

    ! Interface variables
    integer, intent(in) :: nEqns,       & !< number of equations
                           iNodeEdge      !< node/edge index

    real(FP), intent(inout) :: q(:,:) !< state 

    real(FP), intent(in) :: t  !< simulation time at n+1

    logical, intent(inout), optional :: updated(:) !< flag that node/edge updated

    ! Local variables
    integer :: iNode,   & !< node index
               iEdge      !< edge index

    if ( iNodeEdge > 0 ) then ! apply signal to nodes

        iNode = iNodeEdge

        q(:,iNode) = evalFunction(nDim,nEqns,nodeCoord(:,iNode),t, &
                    bcFunction) 

        if ( present(updated) ) updated(iNode) = .true.

    else ! apply signal to edges

        iEdge = abs(iNodeEdge)

        q(:,iEdge) = evalFunction(nDim,nEqns,edgeCoord(:,iEdge),t, &
                    bcFunction) 

        if ( present(updated) ) updated(iEdge) = .true.

    end if 


end subroutine specifyBC
!-------------------------------------------------------------------------------
!> @purpose
!>  Outflow BC.
!>
!> @author
!>  Timothy A. Eymann
!>
!> @history
!>  20 July 2012 - Initial creation\n
!>  30 October 2012 - Reset to use cell averages
!>  13 December 2013 - Adaptation for 2D linear advection case (Maeng)
!>  6 October 2015 - pointer for updated flag (Maeng)
!>  31 January 2017 - Assumed shape array for states
!>
subroutine outflowBC(nEqns,iNodeEdge,q,updated)

    use analyticFunctions
    use meshUtil, only: nDim, nodeCoord, edgeCoord
    implicit none

    ! Interface variables
    integer, intent(in) :: nEqns,       & !< number of equations
                           iNodeEdge      !< node/edge index

    real(FP), intent(inout) :: q(:,:) !< state 

    logical, intent(inout), optional :: updated(:) !< flag that node/edge updated

    ! Local variables
    integer :: iNode,   & !< local node index
               iEdge      !< local edge index

    if ( iNodeEdge > 0 ) then ! apply signal to nodes

        iNode = iNodeEdge

        !q(:,iNode) = q(:,iNode)

        if ( present(updated) ) updated(iNode) = .true.

    else ! apply signal to edges

        iEdge = abs(iNodeEdge)

        !q(:,iEdge) = q(:,iEdge)

        if ( present(updated) ) updated(iEdge) = .true.

    end if


end subroutine outflowBC
!-------------------------------------------------------------------------------
!> @purpose
!>  Wall BC
!>
!> @author
!>  J. Brad Maeng
!>
!> @history
!>  5 January 2016 - Initial creation
!>
subroutine wallBC(nEqns,iNodeEdge,q,updated)

    use meshUtil, only: nDim, nodeCoord, edgeCoord
    use physics, only: DENSITY, UVEL, VVEL

    implicit none

    ! Interface variables
    integer, intent(in) :: nEqns,       & !< number of equations
                           iNodeEdge      !< node/edge index

    real(FP), intent(inout) :: q(:,:) !< state 

    logical, intent(inout), optional :: updated(:) !< flag that node/edge updated

    ! Local variables
    integer :: iNode,   & !< local node index
               iEdge      !< local edge index

    real(FP) :: curvature,      & !< radius of curvature of geometry
                normalVec(nDim)   !< normal vector 

    if ( iNodeEdge > 0 ) then ! apply signal to nodes

        iNode = iNodeEdge

        ! for Euler equations, normal components of pressure and velocity needs  
        q(:,iNode) = q(:,iNode)

        if ( present(updated) ) updated(iNode) = .true.

    else ! apply signal to edges

        iEdge = abs(iNodeEdge)

        q(:,iEdge) = q(:,iEdge)

        if ( present(updated) ) updated(iEdge) = .true.

    end if


end subroutine wallBC
!-------------------------------------------------------------------------------
end module boundaryConditions
!-------------------------------------------------------------------------------
