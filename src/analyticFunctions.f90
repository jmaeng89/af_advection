!!-------------------------------------------------------------------------------
!!>  Contains initialization functions
!>
!> @author
!>  Timothy A. Eymann
!>  
!> @history
!>  10 April 2015 - Combined evalFunctions for simplicity (Maeng)
!>  20 September 2016 - Integrate all advective systems (Maeng)
!>
module analyticFunctions

    use solverVars, only: FP
    implicit none

contains
!-------------------------------------------------------------------------------
!> @purpose 
!>  Select which function to evaluate (might be slow since case select is
!>  within loop)
!>
!> @author
!>  Timothy A. Eymann
!>
!> @history
!>  13 March 2012 - Initial Creation
!>  7 April 2015 - Combined Steady and Unsteady (Maeng)
!>  6 May 2015 - Began working on euler 2d (Maeng)
!>
function evalFunction(nDim,nEqns,x,t,funcName)

    use solverVars, only: initSolnType, inLength
    implicit none

    ! Interface variables
    character(inLength), intent(in), optional :: funcName !< name of function

    integer, intent(in) :: nDim,    & !< dimension of x-array
                           nEqns      !< number of equations in system

    real(FP), intent(in) :: x(nDim) !< function evaluation point

    real(FP), intent(in), optional :: t !< time

    ! Function variable
    real(FP) :: evalFunction(nEqns)

    ! Local variable
    character(inLength) :: func!< local variable for function name

    real(FP) :: tSol   !< solution evaluation time

    if ( .not. present(t) ) then
        tSol = 0.0_FP
    else
        tSol = t
    end if

    if ( present(funcName) ) then
        func = funcName
    else
        func = initSolnType
    end if

    select case ( trim(adjustl(func)) )

        ! ONE DIMENSIONAL PROBLEMS --------
        case ( 'gauss1d' )
            evalFunction = gaussian1d(nEqns,x(1),tSol)

        case ( 'sin1d' )
            evalFunction = sinusoid1d(nEqns,x(1),tSol)
        
        case ( 'burgG1d' )
            evalFunction = burgers_gauss1d(nEqns,x(1),tSol)

        case ( 'burgS1d' )
            evalFunction = burgers_sin1d(nEqns,x(1),tSol)

        case ( 'cshk1d' )
            evalFunction = burgers_shock1d(nEqns,x(1),tSol)
    
        case ( 'rare1d' )
            evalFunction = burgers_rarefaction1d(nEqns,x(1),tSol)
    
        case ( 'plessG1d' )
            evalFunction = burgers_gauss1d(nEqns,x(1),tSol)
            !evalFunction = pless_gauss1d(nEqns,x(1),Sol)

        case ( 'plessS1d' )
            evalFunction = burgers_sin1d(nEqns,x(1),tSol)
            !evalFunction = pless_sin1d(nEqns,x(1),tSol)

        ! TWO DIMENSIONAL PROBLEMS ---------
        ! linear advection cases
        case ( 'constVec' )
            evalFunction = constVec(nEqns,x(1),x(2))

        case ( 'gaussian' )
            evalFunction = gaussian(nEqns,x(1),x(2),tSol)

        case ( 'sinusoid' )
            evalFunction = sinusoid(nEqns,x(1),x(2),tSol)

        case ( 'circRotg' )
            evalFunction = circularRot_gauss(nEqns,x(1),x(2),tSol)

        case ( 'circRotc' )
            evalFunction = circularRot_cyl(nEqns,x(1),x(2),tSol)

        case ( 'square' )
            evalFunction = squareWave(nEqns,x(1),x(2),tSol)

        ! burgers' equation
        case ( 'burgersG' )
            evalFunction = burgers_gauss(nEqns,x(1),x(2),tSol)

        case ( 'burgersS' )
            evalFunction = burgers_sin(nEqns,x(1),x(2),tSol)

        ! pressureless Euler cases
        case ( 'plEuEx2') ! too trivial
            evalFunction = plEulerExact2d(nEqns,x(1),x(2),tSol)

        ! divergence tests
        case ( 'solidRot' )
            evalFunction = solidRotation(nEqns,x(1),x(2))

        case ( 'plEu2dt1' )
            evalFunction = plEuler2d_v1(nEqns,x(1),x(2))

        case ( 'plEu2dt2' )
            evalFunction = plEuler2d_v2(nEqns,x(1),x(2))

        ! dynamic tests
        case ( 'plessG' )
            evalFunction = pless_gauss(nEqns,x(1),x(2),tSol)

        case ( 'plessS' )
            evalFunction = pless_sin_2d(nEqns,x(1),x(2),tSol)

        case ( 'plessSC' )
            evalFunction = pless_sin_cos_2d(nEqns,x(1),x(2),tSol)

        case ( 'plessS2' )
            evalFunction = pless_sin_uncoupled(nEqns,x(1),x(2),tSol)

        case ( 'pvortex')
            evalFunction = plessVortex(nEqns,x(1),x(2),tSol)

        case default
            write(*,'(3a)') 'ERROR: "',trim(adjustl(func)), &
                            '" is not a valid function.'
            stop

    end select

end function evalFunction
!-------------------------------------------------------------------------------
!> @purpose 
!>  Gaussian pulse 
!>
!> @author
!>  J. Brad Maeng
!>
!> @history
!>  14 April 2017 - Initial Creation
!>
function gaussian1d(nEqns,x,t)

    implicit none

    ! Interface variables
    integer, intent(in) :: nEqns !< equation number

    real(FP), intent(in) :: x,  & !< x coord
                            t     !< time

    ! Function variable
    real(FP) :: gaussian1d(nEqns)

    ! Local variables
    integer :: iEq !< equation index

    real(FP) :: a = 1.0_FP,     & !< x-dir wave speed
                b = 1.0_FP,     & !< y-dir wave speed
                c = 10.0_FP        !< decay coefficient

    gaussian1d = 0.0_FP
    gaussian1d(1) = exp( -c*(x)**2.0_FP ) 
    gaussian1d(2) = a    

end function gaussian1d
!-------------------------------------------------------------------------------
!> @purpose 
!>  Sinusoidal wave 
!>
!> @author
!>  J. Brad Maeng
!>
!> @history
!>  14 April 2017 - Initial Creation
!>
function sinusoid1d(nEqns,x,t)

    use solverVars, only: pi
    implicit none

    ! Interface variables
    integer, intent(in) :: nEqns !< equation number

    real(FP), intent(in) :: x,  & !< x coord
                            t     !< time

    ! Function variable
    real(FP) :: sinusoid1d(nEqns)

    ! Local variables
    integer :: iEq !< equation index
     
    real(FP) :: a = 1.0_FP,     & !< x-dir wave speed
                b = 0.0_FP        !< y-dir wave speed

    sinusoid1d = 0.0_FP
    sinusoid1d(1) = sin( (2.0_FP*pi)/2.0_FP*(x+1) ) 
    sinusoid1d(2) = a    

end function sinusoid1d
!-------------------------------------------------------------------------------
!> @purpose 
!>  Burgers' equation initial condition
!>
!> @author
!>  J. Brad Maeng
!>
!> @history
!>  14 April 2017 - Initial creation
!>
function burgers_gauss1d(nEqns,x,t)

    use solverVars, only: pi
    implicit none

    ! Interface variables
    integer, intent(in) :: nEqns !< equation number

    real(FP), intent(in) :: x, & 
                            t    !< time

    ! Function variable
    real(FP) :: burgers_gauss1d(nEqns)

    ! Local variables
    real(FP) :: c = 10.0_FP         !< decay coefficient 

    burgers_gauss1d = 0.0_FP
    burgers_gauss1d(1) = 1.0_FP 
    burgers_gauss1d(2) = 0.1_FP + exp( -c*(x**2.0_FP) ) 

end function burgers_gauss1d
!-------------------------------------------------------------------------------
!> @purpose 
!>  Burgers' equation initial condition
!>
!> @author
!>  J. Brad Maeng
!>
!> @history
!>  14 April 2017 - Initial creation
!>
function burgers_sin1d(nEqns,x,t)

    use solverVars, only: pi
    implicit none

    ! Interface variables
    integer, intent(in) :: nEqns !< equation number

    real(FP), intent(in) :: x, & 
                            t    !< time

    ! Function variable
    real(FP) :: burgers_sin1d(nEqns)

    ! Local variables
    real(FP) :: c,      & !< decay coefficient 
                A,      & !< constant
                k,      & !< constant
                ts,     & !< constant
                xc        !< constant 

    A = 1.0_FP/3.0_FP
    burgers_sin1d = 0.0_FP
    burgers_sin1d(1) = 1.0_FP 
    burgers_sin1d(2) = ( 1.0_FP + A*sin(2.0_FP*pi/2.0_FP*(x+1.0_FP)) )

end function burgers_sin1d
!-------------------------------------------------------------------------------
!> @purpose 
!>  Burgers' shock formation initial condition
!>
!> @author
!>  J. Brad Maeng
!>
!> @history
!>  24 April 2017 - Initial creation
!>
function burgers_shock1d(nEqns,x,t)

    use solverVars, only: pi
    implicit none

    ! Interface variables
    integer, intent(in) :: nEqns !< equation number

    real(FP), intent(in) :: x, & 
                            t    !< time

    ! Function variable
    real(FP) :: burgers_shock1d(nEqns)

    ! Local variables
    real(FP) :: c,      & !< decay coefficient 
                A,      & !< constant
                k,      & !< constant
                ts,     & !< constant
                xc        !< constant 

    ts = 1.0_FP ! shock formation time
    A = 1.0_FP/(2.0_FP*pi*(ts/2.0_FP))
    burgers_shock1d = 0.0_FP
    burgers_shock1d(1) = 1.0_FP 
    burgers_shock1d(2) = A*sin(2.0_FP*pi*(x+1.0_FP)/2.0_FP) 

end function burgers_shock1d
!-------------------------------------------------------------------------------
!> @purpose 
!>  Burgers' rarefaction wave formation initial condition
!>
!> @author
!>  J. Brad Maeng
!>
!> @history
!>  24 April 2017 - Initial creation
!>
function burgers_rarefaction1d(nEqns,x,t)

    use solverVars, only: pi
    implicit none

    ! Interface variables
    integer, intent(in) :: nEqns !< equation number

    real(FP), intent(in) :: x, & 
                            t    !< time

    ! Function variable
    real(FP) :: burgers_rarefaction1d(nEqns)

    ! Local variables
    real(FP) :: c,      & !< decay coefficient 
                A,      & !< constant
                k,      & !< constant
                ts,     & !< constant
                xc        !< constant 

    A = 0.2_FP
    k = 20.0_FP
    burgers_rarefaction1d = 0.0_FP
    burgers_rarefaction1d(1) = 1.0_FP 
    burgers_rarefaction1d(2) = A*tanh(k*(x)) 

end function burgers_rarefaction1d
!-------------------------------------------------------------------------------
!> @purpose 
!>  Constant vector for testing systems
!>
!> @author
!>  Timothy A. Eymann
!>
!> @history
!>  25 October 2012 - Initial Creation
!>
function constVec(nEqns,x,y)

    implicit none

    ! Interface variables
    integer, intent(in) :: nEqns !< equation number

    real(FP), intent(in) :: x,  & !< x coord
                            y     !< y coord

    ! Function variable
    real(FP) :: constVec(nEqns)

    ! Local variables
    integer :: iEq !< equation index

    constVec(:) = 1.0_FP ! + dble(iEq)
    !constVec(1) = 1.0_FP ! + dble(iEq)
    !constVec(2) = x**2.0 + 2.*y**2.0 - x*y + y!0.5_FP*x*y - 1.0_FP*y**2.0 ! + dble(iEq)
    !constVec(3) = -0.5*x**2.0 + 3.*y**2.0 + 5.0*x*y + x! + dble(iEq)
    !constVec(4) = 1.0_FP ! + dble(iEq)

end function constVec
!-------------------------------------------------------------------------------
!> @purpose 
!>  Gaussian pulse 
!>
!> @author
!>  J. Brad Maeng
!>
!> @history
!>  10 August 2016 - Initial Creation
!>
function gaussian(nEqns,x,y,t)

    implicit none

    ! Interface variables
    integer, intent(in) :: nEqns !< equation number

    real(FP), intent(in) :: x,  & !< x coord
                            y,  & !< y coord
                            t     !< time

    ! Function variable
    real(FP) :: gaussian(nEqns)

    ! Local variables
    integer :: iEq !< equation index

    real(FP) :: a = 1.0_FP,     & !< x-dir wave speed
                b = 1.0_FP,     & !< y-dir wave speed
                c = 0.5_FP        !< decay coefficient

    gaussian(1) = exp( -c*(x)**2.0_FP - c*(y)**2.0_FP ) 
    gaussian(2) = a    
    gaussian(3) = b 

end function gaussian
!-------------------------------------------------------------------------------
!> @purpose 
!>  Sinusoidal wave !>
!> @author
!>  J. Brad Maeng
!>
!> @history
!>  10 August 2016 - Initial Creation
!>
function sinusoid(nEqns,x,y,t)

    use solverVars, only: pi
    implicit none

    ! Interface variables
    integer, intent(in) :: nEqns !< equation number

    real(FP), intent(in) :: x,  & !< x coord
                            y,  & !< y coord
                            t     !< time

    ! Function variable
    real(FP) :: sinusoid(nEqns)

    ! Local variables
    integer :: iEq !< equation index
     
    real(FP) :: a = 1.0_FP,     & !< x-dir wave speed
                b = 1.0_FP        !< y-dir wave speed

    sinusoid(1) = sin( (2.0_FP*pi)/5.0_FP*(x+y) ) 
    sinusoid(2) = a  
    sinusoid(3) = b

end function sinusoid
!-------------------------------------------------------------------------------
!> @purpose 
!>  Circular advection of blob
!>
!> @author
!>  J. Brad Maeng
!>
!> @history
!>  27 October 2016 - Initial Creation
!>
function circularRot_gauss(nEqns,x,y,t)

    use solverVars, only: pi
    implicit none

    ! Interface variables
    integer, intent(in) :: nEqns !< equation number

    real(FP), intent(in) :: x,  & !< x coord
                            y,  & !< y coord
                            t     !< time

    ! Function variable
    real(FP) :: circularRot_gauss(nEqns)

    ! Local variable
    real(FP) :: a,  & !< x-dir wave speed
                b,  & !< y-dir wave speed
                c,  & !< coefficient
                x0, & !< x-center 
                y0, & !< y-center
                r     !< radius
    x0 = -2.5_FP
    y0 = 0.0_FP
    r = sqrt((x-x0)**2.0_FP+(y-y0)**2.0_FP) ! centered at x0, y0 
    a = 2.0_FP*pi*y
    b = -2.0_FP*pi*x
    c = 0.8_FP 
    
    ! smooth interpolation
    if ( r <= (3.0_FP) ) then 
        circularRot_gauss(1) = exp(-c*r**2.0_FP)  ! smooth bump
        !circularRot(1) = 1.0_FP  !< cylinder
    else
        circularRot_gauss(1) = 0.0_FP
    end if
    circularRot_gauss(2) = a 
    circularRot_gauss(3) = b 

end function circularRot_gauss
!-------------------------------------------------------------------------------
!> @purpose 
!>  Circular advection of blob
!>
!> @author
!>  J. Brad Maeng
!>
!> @history
!>  27 October 2016 - Initial Creation
!>
function circularRot_cyl(nEqns,x,y,t)

    use solverVars, only: pi
    implicit none

    ! Interface variables
    integer, intent(in) :: nEqns !< equation number

    real(FP), intent(in) :: x,  & !< x coord
                            y,  & !< y coord
                            t     !< time

    ! Function variable
    real(FP) :: circularRot_cyl(nEqns)

    ! Local variable
    real(FP) :: a,  & !< x-dir wave speed
                b,  & !< y-dir wave speed
                c,  & !< coefficient
                x0, & !< x-center 
                y0, & !< y-center
                r     !< radius
    x0 = -2.5_FP
    y0 = 0.0_FP
    r = sqrt((x-x0)**2.0_FP+(y-y0)**2.0_FP) ! centered at x0, y0 
    a = 2.0_FP*pi*y
    b = -2.0_FP*pi*x
    c = 0.8_FP 
    
    ! smooth interpolation
    if ( r <= (1.0_FP) ) then 
        circularRot_cyl(1) = 1.0_FP  !< cylinder
    else
        circularRot_cyl(1) = 0.0_FP
    end if
    circularRot_cyl(2) = a 
    circularRot_cyl(3) = b 

end function circularRot_cyl
!-------------------------------------------------------------------------------
!> @purpose 
!>  Square wave profile
!>
!> @author
!>  J. Brad Maeng
!>
!> @history
!>  24 April 2016 - Initial Creation
!>
function squareWave(nEqns,x,y,t)

    use solverVars, only: pi
    implicit none

    ! Interface variables
    integer, intent(in) :: nEqns !< equation number

    real(FP), intent(in) :: x,  & !< x coord
                            y,  & !< y coord
                            t     !< time

    ! Function variable
    real(FP) :: squareWave(nEqns)

    ! Local variable
    real(FP) :: a,  & !< x-dir wave speed
                b,  & !< y-dir wave speed
                c,  & !< coefficient
                x0, & !< x-center 
                y0, & !< y-center
                r     !< radius
    x0 = -2.5_FP
    y0 = 0.0_FP
    r = sqrt((x-x0)**2.0_FP+(y-y0)**2.0_FP) ! centered at x0, y0 
    !a = 2.0_FP*pi*y
    !b = -2.0_FP*pi*x
    !c = 0.8_FP 
    a = 1.0_FP
    b = 1.0_FP
    
    !if ( r <= (3.0_FP) ) then 

    if ( abs(x) <= 2.0_FP .and. abs(y) <= 2.0_FP ) then 
        squareWave(1) = 1.0_FP 
    else
        squareWave(1) = 0.0_FP
    end if
    squareWave(2) = a 
    squareWave(3) = b 

end function squareWave
!-------------------------------------------------------------------------------
!> @purpose 
!>  Burgers' equation initial condition
!>
!> @author
!>  J. Brad Maeng
!>
!> @history
!>  9 February 2017 - Initial creation
!>
function burgers_gauss(nEqns,x,y,t)

    use solverVars, only: pi
    implicit none

    ! Interface variables
    integer, intent(in) :: nEqns !< equation number

    real(FP), intent(in) :: x, & 
                            y, & !< input coordinates
                            t    !< time

    ! Function variable
    real(FP) :: burgers_gauss(nEqns)

    ! Local variables
    real(FP) :: c         !< decay coefficient 

    c = 0.5_FP 

    burgers_gauss(1) = 1.0_FP 
    burgers_gauss(2) = 0.1_FP + exp( -c*(x**2.0_FP + y**2.0_FP) ) 
    burgers_gauss(3) = 0.1_FP + exp( -c*(x**2.0_FP + y**2.0_FP) ) 

end function burgers_gauss
!-------------------------------------------------------------------------------
!> @purpose 
!>  Burgers' equation initial condition
!>
!> @author
!>  J. Brad Maeng
!>
!> @history
!>  14 February 2017 - Initial creation
!>
function burgers_sin(nEqns,x,y,t)

    use solverVars, only: pi
    implicit none

    ! Interface variables
    integer, intent(in) :: nEqns !< equation number

    real(FP), intent(in) :: x, & 
                            y, & !< input coordinates
                            t    !< time

    ! Function variable
    real(FP) :: burgers_sin(nEqns)

    ! Local variables
    real(FP) :: c,      & !< decay coefficient 
                A,      & !< constant
                k,      & !< constant
                ts,     & !< constant
                xc        !< constant 

    A = 1.0_FP/3.0_FP
    burgers_sin(1) = 1.0_FP 
    burgers_sin(2) = ( 1.0_FP + A*sin(2.0_FP*pi/5.0_FP*(x+y)) )
    burgers_sin(3) = ( 1.0_FP + A*sin(2.0_FP*pi/5.0_FP*(x+y)) )

end function burgers_sin
!-------------------------------------------------------------------------------
!> @purpose 
!>  Initialization for solid body rotation in 2d
!>
!> @author
!>  J. Brad Maeng
!>
!> @history
!>  26 August 2015 - Initial Creation
!>
function solidRotation(nEqns,x,y)

    use solverVars, only: pi
    implicit none

    ! Interface variables
    integer, intent(in) :: nEqns      !< equation number

    real(FP), intent(in) :: x,  & !< x coord
                            y     !< y coord

    ! Function variable
    real(FP) :: solidRotation(nEqns)

    ! Local variables
    real(FP) :: xc, yc    !< center coord

    real(FP) :: r,      & !< radius 
                d,      & !< distance
                d2,     & !< distance
                k,      & !< decay coefficient
                kk,     & !< decay coefficient 2
                vMag,   & !< magnitude of velocity
                theta     !< angle

    xc = x!-0.0125_FP
    yc = y!-0.0125_FP

    d = 4.0_FP
    r = sqrt((xc)**2.0_FP + (yc)**2.0_FP)
    k = 0.5_FP
    kk = d

    solidRotation(1) = 1.0_FP

    ! with free bc
    solidRotation(2) = -k*(yc)
    solidRotation(3) =  k*(xc)

    !if ( r >= d ) then
    !    solidRotation(2) = 0.0_FP 
    !    solidRotation(3) = 0.0_FP
    !else
    !    solidRotation(2) = -k*(y)*tanh(kk*(r-d)**2.0_FP)
    !    solidRotation(3) =  k*(x)*tanh(kk*(r-d)**2.0_FP)
    !end if

end function solidRotation
!-------------------------------------------------------------------------------
!> @purpose 
!>  Pressureless euler systems exact solutions for two dimensional expansion case. 
!>
!> @author
!>  J. Brad Maeng
!>
!> @history
!>  17 February 2015 - Initial Creation
!>
function plEulerExact2d(nEqns,x,y,t)

    implicit none

    ! Interface variables
    integer, intent(in) :: nEqns      !< equation number

    real(FP), intent(in) :: x,  & !< x coord
                            y,  & !< y coord
                            t     !< simulation time

    ! Function variable
    real(FP) :: plEulerExact2d(nEqns)

    ! Local variables
    real(FP) :: xVal, yVal
    
    !xVal = x+6.0_FP
    xVal = x-6.0_FP
    yVal = y+6.0_FP

    plEulerExact2d(1) = (((xVal**2.0_FP+yVal**2.0_FP)/(5.0_FP+5.0_FP*t)**2.0_FP)**4.0_FP)/&
                           (5.0_FP+5.0_FP*t)**2.0_FP
    plEulerExact2d(2) = 5.0_FP/(5.0_FP+5.0_FP*t)*(xVal)
    plEulerExact2d(3) = 5.0_FP/(5.0_FP+5.0_FP*t)*(yVal)

end function plEulerExact2d
!-------------------------------------------------------------------------------
!> @purpose 
!>  Initialization for pressureless euler system with velocity divergence  
!>  first-order term = 0
!>  second-order term =/ 0
!>
!> @author
!>  J. Brad Maeng
!>
!> @history
!>  30 January 2015 - Initial Creation
!>
function plEuler2d_v1(nEqns,x,y)

    use solverVars, only: pi
    implicit none

    ! Interface variables
    integer, intent(in) :: nEqns      !< equation number

    real(FP), intent(in) :: x,       & !< x coord
                            y          !< y coord

    ! Function variable
    real(FP) :: plEuler2d_v1(nEqns)

    ! Local variable
    real(FP) :: A = 1.0_FP/3.0_FP,  &
                c = 0.5_FP      !< decay coefficient 

    !plEuler2d_v1(1) = 1.0_FP
    !!plEuler2d_v1(1) = exp(sin(2.0_FP*pi*(x+y)/5.0_FP))
    !plEuler2d_v1(2) = A*cos(pi*(y)/(5.0_FP)+2.0_FP) 
    !plEuler2d_v1(3) = A*sin(pi*(x)/(5.0_FP)+2.0_FP) 

    plEuler2d_v1(1) = 0.1_FP + exp( -c*(x**2.0_FP + y**2.0_FP) )
    plEuler2d_v1(2) = ( 1.0_FP + A*cos(2.0_FP*pi*(y)/(5.0_FP)) )
    plEuler2d_v1(3) = ( 1.0_FP + A*sin(2.0_FP*pi*(x)/(5.0_FP)) )

end function plEuler2d_v1
!-------------------------------------------------------------------------------
!> @purpose 
!>  Initialization for pressureless euler system with velocity divergence  
!>  first-order term =/ 0
!>  second-order term = 0
!>
!> @author
!>  J. Brad Maeng
!>
!> @history
!>  30 January 2015 - Initial Creation
!>
function plEuler2d_v2(nEqns,x,y)

    use solverVars, only: pi
    implicit none

    ! Interface variables
    integer, intent(in) :: nEqns      !< equation number

    real(FP), intent(in) :: x, y       !< x+y coord

    ! Function variable
    real(FP) :: plEuler2d_v2(nEqns)

    ! Local variable
    real(FP) :: A = 1.0_FP/3.0_FP,  &
                c = 0.5_FP
    
    plEuler2d_v2(1) = 0.1_FP + exp( -c*(x**2.0_FP + y**2.0_FP) )
    plEuler2d_v2(2) = ( 1.0_FP + A*cos(2.0_FP*pi*(x+y)/(5.0_FP)) )
    plEuler2d_v2(3) = ( 1.0_FP + A*sin(2.0_FP*pi*(x+y)/(5.0_FP)) )

    !plEuler2d_v2(2) = 1.0_FP !-1.0_FP/3.0_FP*(cos(pi*(x+y)/(5.0_FP))+2.0_FP)
    !plEuler2d_v2(3) = 1.0_FP ! 1.0_FP/3.0_FP*(sin(pi*(x+y)/(5.0_FP))+2.0_FP)

end function plEuler2d_v2
!-------------------------------------------------------------------------------
!> @purpose 
!>  Pressureless Euler with gaussian velocity initial condition
!>
!> @author
!>  J. Brad Maeng
!>
!> @history
!>  2 March 2017 - Initial creation
!>
function pless_gauss(nEqns,x,y,t)

    use solverVars, only: pi
    implicit none

    ! Interface variables
    integer, intent(in) :: nEqns !< equation number

    real(FP), intent(in) :: x, & 
                            y, & !< input coordinates
                            t    !< time

    ! Function variable
    real(FP) :: pless_gauss(nEqns)

    ! Local variables
    real(FP) :: c1 = 0.5_FP,     & !< decay coefficient 
                c2 = 0.45_FP        !< decay coefficient 

    pless_gauss(1) = 1.0_FP !exp( -c*(x**2.0_FP + y**2.0_FP) ) 
    pless_gauss(2) = 0.1_FP + exp( -c1*(x**2.0_FP + y**2.0_FP) ) 
    pless_gauss(3) = 0.1_FP + exp( -c1*(x**2.0_FP + y**2.0_FP) ) 

end function pless_gauss
!-------------------------------------------------------------------------------
!> @purpose 
!>  Pressureless Euler with sinusoidal velocity initial condition
!>
!> @author
!>  J. Brad Maeng
!>
!> @history
!>  2 March 2017 - Initial creation
!>
function pless_sin_2d(nEqns,x,y,t)

    use solverVars, only: pi
    implicit none

    ! Interface variables
    integer, intent(in) :: nEqns !< equation number

    real(FP), intent(in) :: x, & 
                            y, & !< input coordinates
                            t    !< time

    ! Function variable
    real(FP) :: pless_sin_2d(nEqns)

    ! Local variables
    real(FP) :: c,      & !< decay coefficient 
                A,      & !< constant
                k,      & !< constant
                ts,     & !< constant
                xc        !< constant 

    A = 1.0_FP/3.0_FP
    pless_sin_2d(1) = 1.0_FP 
    pless_sin_2d(2) = ( 1.0_FP + A*sin(2.0_FP*pi/5.0_FP*(x+y)) )
    pless_sin_2d(3) = ( 1.0_FP + A*sin(2.0_FP*pi/5.0_FP*(x+y)) )

end function pless_sin_2d
!-------------------------------------------------------------------------------
!> @purpose 
!>  Pressureless Euler with directional sinusoidal initial condition
!>
!> @author
!>  J. Brad Maeng
!>
!> @history
!>  28 April 2017 - Initial creation
!>
function pless_sin_uncoupled(nEqns,x,y,t)

    use solverVars, only: pi
    implicit none

    ! Interface variables
    integer, intent(in) :: nEqns !< equation number

    real(FP), intent(in) :: x, & 
                            y, & !< input coordinates
                            t    !< time

    ! Function variable
    real(FP) :: pless_sin_uncoupled(nEqns)

    ! Local variables
    real(FP) :: A = 1.0_FP/3.0_FP     !< decay coefficient 

    pless_sin_uncoupled(1) = 1.0_FP  
    pless_sin_uncoupled(2) = ( 1.0_FP + A*cos(2.0_FP*pi*(x)/(5.0_FP)) )
    pless_sin_uncoupled(3) = ( 1.0_FP + A*sin(2.0_FP*pi*(y)/(5.0_FP)) )

end function pless_sin_uncoupled
!-------------------------------------------------------------------------------
!> @purpose 
!>  Pressureless Euler with sinusoidal velocity initial condition
!>
!> @author
!>  J. Brad Maeng
!>
!> @history
!>  2 March 2017 - Initial creation
!>
function pless_sin_cos_2d(nEqns,x,y,t)

    use solverVars, only: pi
    implicit none

    ! Interface variables
    integer, intent(in) :: nEqns !< equation number

    real(FP), intent(in) :: x, & 
                            y, & !< input coordinates
                            t    !< time

    ! Function variable
    real(FP) :: pless_sin_cos_2d(nEqns)

    ! Local variables
    real(FP) :: c,      & !< decay coefficient 
                A,      & !< constant
                k,      & !< constant
                ts,     & !< constant
                xc        !< constant 

    A = 1.0_FP/3.0_FP
    pless_sin_cos_2d(1) = 1.0_FP 
    pless_sin_cos_2d(2) = ( 1.0_FP + A*sin(2.0_FP*pi/5.0_FP*(x+y)) )
    pless_sin_cos_2d(3) = ( 1.0_FP + A*cos(2.0_FP*pi/5.0_FP*(x+y)) )

end function pless_sin_cos_2d
!-------------------------------------------------------------------------------
!> @purpose 
!>  2D vortex
!>
!> @history
!>  3 November 2015 - Initial Creation
!>
function plessVortex(nEqns,x,y,t)

    use solverVars, only: pi
    implicit none

    ! Interface variables
    integer, intent(in) :: nEqns  !< equation number

    real(FP), intent(in) :: x,  & !< x coord
                            y,  & !< y coord
                            t

    ! Local variable 
    real(FP) :: r,      & !< radial position
                ep,     & !< vortex strength
                xc,     & !< vortex location
                yc,     & !< vortex location
                x0,     & !< vortex initial location
                y0,     & !< vortex initial location
                uInf,   & !< freestream velocity, x
                vInf      !< freestream velocity, y

    ! Function variable
    real(FP) :: plessVortex(nEqns)

    ep = 10.0_FP
    uInf = 0.0_FP
    vInf = 0.0_FP
    x0 = 0.0_FP
    y0 = 0.0_FP
    xc = x - x0 - uInf*t
    yc = y - y0 - vInf*t
    r = sqrt((xc)**2.0_FP + (yc)**2.0_FP)
    
    ! original condition
    plessVortex = 0.0_FP
    plessVortex(1) = 1.0_FP
    plessVortex(2) = uInf - ep*yc*exp(0.25_FP*(1.0_FP-r**2.0_FP))/(2.0_FP*pi) 
    plessVortex(3) = vInf + ep*xc*exp(0.25_FP*(1.0_FP-r**2.0_FP))/(2.0_FP*pi)

end function plessVortex
!-------------------------------------------------------------------------------
!> @purpose 
!>  2D Isentropic Euler stationary vortex
!>
!> @author
!>  Doreen Fan
!>
!> @history
!>  20 June 2015 - Initial Creation
!>
function steadyVortex(nEqns,x,y,t)

    use solverVars, only: pi
    implicit none

    ! Interface variables
    integer, intent(in) :: nEqns  !< equation number

    real(FP), intent(in) :: x,  & !< x coord
                            y,  & !< y coord
                            t

    ! Local variable 
    real(FP) :: r,      & !< radial position
                ep,     & !< vortex strength
                xc,     & !< vortex location
                yc,     & !< vortex location
                x0,     & !< vortex initial location
                y0,     & !< vortex initial location
                uInf,   & !< freestream velocity, x
                vInf      !< freestream velocity, y

    real(FP) :: gam = 1.4_FP

    ! Function variable
    real(FP) :: steadyVortex(nEqns)

    ep = 10.0_FP
    uInf = 0.0_FP
    vInf = 0.0_FP
    x0 = 0.0_FP
    y0 = 0.0_FP
    xc = x - x0 - uInf*t
    yc = y - y0 - vInf*t
    r = sqrt((xc)**2.0_FP + (yc)**2.0_FP)
    
    ! original condition
    !steadyVortex(1) = ( 1.0_FP -  &
    !            (((gam-1.0_FP)*(ep**2.0_FP)*exp(1.0_FP-r**2.0_FP))/  &
    !            (8.0_FP*gam*pi**2.0_FP)) )**(1.0_FP/(gam-1.0_FP))
    steadyVortex(1) = 1.0_FP
    steadyVortex(2) = uInf - ep*yc*exp(0.25_FP*(1.0_FP-r**2.0_FP))/(2.0_FP*pi) 
    steadyVortex(3) = vInf + ep*xc*exp(0.25_FP*(1.0_FP-r**2.0_FP))/(2.0_FP*pi)

end function steadyVortex
!-------------------------------------------------------------------------------
end module analyticFunctions
