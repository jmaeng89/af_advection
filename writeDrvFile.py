#-------------------------------------------------------------------------------
# @ purpose
#  Write driver files to run Active Flux Euler code
#
# @ history
#  29 March 2016 - Initial creation (Maeng)

# required libraries
import sys, os, numpy, subprocess

# base path for cases
basePath = 'cases/pless/svortex';

# driver file names
drvFileList = ['diag002', 'diag008', 'diag032', 'diag128', 'diag512']
#drvFileList = ['unst002', 'unst008', 'unst032', 'unst128', 'unst512']
#drvFileList = ['diagInv002', 'diagInv008', 'diagInv032', 'diagInv128', 'diagInv512']
#drvFileList = ['quad001', 'quad004', 'quad016', 'quad064', 'quad256']


# mesh file list. Unless otherwise listed, the same name as driver file is assumed.
meshFileList = drvFileList;
#meshFileList = ['unstL000', 'unstL001', 'unstL002', 'unstL003', 'unstL004']
#meshFileList = ['unst032', 'unst032', 'unst032', 'unst032', 'unst032']
#meshFileList = ['diag032', 'diag032', 'diag032', 'diag032', 'diag032']
#meshFileList = ['oned/oneD004', 'oned/oneD008', 'oned/oneD016', 'oned/oneD032', 'oned/oneD064']

# boundary condition file
bcCond = 'periodic.bc'; # periodic in all directions
#bcCond = 'outflow.bc'; # outflow in all directions

# governing equation
#gvrnEq = 'linadv'; # linear advection equation
#gvrnEq = 'burgers'; # burgers equation
gvrnEq = 'plesseuler'; # pressureless euler equation

# initial condition
## 2d initial condition
#initSol = 'constVec';

## linadv
#initSol = 'sinusoid';
#initSol = 'gaussian';
#initSol = 'circRotg';
#initSol = 'circRotc';
#initSol = 'square';

## burgers
#initSol = 'burgersG';
#initSol = 'burgersS';

## pless euler
#initSol = 'plessG';
#initSol = 'plessS';
#initSol = 'plessS2';
#initSol = 'plessSC';
#initSol = 'plEu2dt1';
#initSol = 'plEu2dt2';
#initSol = 'solidRot';
initSol = 'svortex';


# time related parameters
tFinal = 1.0;
cfl = 0.7;
outputFreq0 = 10; # solution output frequency for coarsest mesh

# driver file printing parameter
# approximate element area refinement factor between consecutive files
refinmtFactor = 2;

# check if directory exists, and create if it does not
if ( not os.path.isdir(basePath) ):
    os.makedirs(basePath);

# write driver files
i = 0; # counter
for drvFile in drvFileList:

    # define variables
    meshFile = meshFileList[i];
    factor = refinmtFactor**(i);
    outputFreq = outputFreq0*factor;
    #nIter = nIter0*factor;
    #dtIn = tFinal/nIter;

    fileName = basePath + '/' + drvFile + '.drv'
    print 'writing ', fileName
    fid = open(fileName, 'w');
    fid.write( '# AF solver input file \n' );
    fid.write( '# ---------------------- \n' );
    fid.write( '# FILE names (do NOT follow with tabs) \n' );
    fid.write( '# ---------------------- \n' );
    fid.write( basePath + '    # base path \n' );
    fid.write( drvFile + '.plt' + '    # centroid average file \n' );
    fid.write( drvFile + '.dat' + '    # reconstruction file \n' );
    fid.write( 'grids/' + meshFile + '.grd' + '     # mesh file \n' );
    fid.write( 'grids/' + bcCond + '     # boundary condition file \n' );
    fid.write( ' \n' );
    fid.write( '# ---------------------- \n' );
    fid.write( '# Solution parameters \n' );
    fid.write( '# ---------------------- \n' );
    fid.write( gvrnEq + '    # governing equation \n' );
    fid.write( initSol + '    # initial solution \n' );
    fid.write( ' \n' );
    fid.write( '# ---------------------- \n' );
    fid.write( '# Time-step parameters \n' );
    fid.write( '# ---------------------- \n' );
    tFinalTemp = '%.12f' % tFinal;
    fid.write( tFinalTemp + '    # final simulation time \n' );
    cflTemp = '%.4e' % cfl;
    fid.write(  cflTemp + '    # Courant number \n' );
    fid.write( ' \n' );
    fid.write( '# ---------------------- \n' );
    fid.write( '# Output parameters \n' );
    fid.write( '# ---------------------- \n' );
    outputFreqTemp = '%d' % outputFreq;
    fid.write( outputFreqTemp + '  # Frequency to output solution \n' );
    fid.write( ' \n' );
    fid.write( '# ------------------------------------------------------------ \n' );
    fid.write( '# Solution input file name. Input path + recon. data file name\n' );
    fid.write( '# If following line is empty, no file will be read ----------- \n' );
    fid.close()

    i = i + 1

